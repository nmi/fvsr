/**
 * @file      convolve.h
 * @brief     Convolution filters
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef CONVOLVE_H
#define CONVOLVE_H

#include <stdint.h>
#include <stdio.h>

#include "config.h"
#include "fvbuf.h"
#include "fvframe.h"

#if HAVE_OPENCL
#include "fvopencl.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif


struct FvBufConvolve2dArg {
    FvBuf    *out;
    FvBuf    *in;
    FvBuf    *kernel;
    int       transpose;
};

struct FvFrameConvolve2dArg {
    FvFrame  *out;
    FvFrame  *in;
    FvFrame  *kernel;
    int       transpose;
};


int FvBuf_convolve_2d_init(FvFilterNode *node, FvFilterNode *parent);
int FvBuf_convolve_2d_arg_set(FvFilterNode *node, FvBuf *out, FvBuf *in,
        FvBuf *kernel, int transpose);
int FvBuf_convolve_2d_run(FvFilterNode *node);

int FvFrame_convolve_2d_init(FvFilterNode *node, FvFilterNode *parent);
int FvFrame_convolve_2d_arg_set(FvFilterNode *node,
        FvFrame *out, FvFrame *in, FvFrame *kernel, int transpose);
int FvFrame_convolve_2d_run(FvFilterNode *node);

int FvBuf_gaussian_kernel_1d(FvFilterNode *parent, FvBuf *buf, float sigma);
int FvFrame_gaussian_kernel_1d(FvFilterNode *parent, FvFrame *frame,
        float sigma);


#define gaussian_kernel_size(sigma) ((int) lrintf(sigma*3) * 2 + 1)

#ifdef __cplusplus
}
#endif

#endif /* CONVOLVE_H */

