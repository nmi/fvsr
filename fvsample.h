/**
 * @file      fvsample.h
 * @brief     Samples and sample arrays
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVSAMPLE_H
#define FVSAMPLE_H

#include <stdint.h>
#include <stdio.h>

#include "config.h"

#ifdef __cplusplus
extern "C" {
#endif

enum FvSampleT {
    FVSAMPLE_VOID,
    FVSAMPLE_U8,
    FVSAMPLE_S8,
    FVSAMPLE_U16,
    FVSAMPLE_S16,
    FVSAMPLE_U32,
    FVSAMPLE_S32,
    FVSAMPLE_F,
    FVSAMPLE_D,
    FVSAMPLE_T_SIZE
};

enum FvComponentT {
    FVCOMPONENT_VOID,
    FVCOMPONENT_Y,
    FVCOMPONENT_CB,
    FVCOMPONENT_CR,
    FVCOMPONENT_R,
    FVCOMPONENT_G,
    FVCOMPONENT_B,
    FVCOMPONENT_A,
    FVCOMPONENT_T_SIZE
};

typedef struct FvSampleV {
    union {
        int8_t   s8;
        uint8_t  u8;
        int16_t  s16;
        uint16_t u16;
        int32_t  s32;
        uint32_t u32;
        float    f;
        double   d;
    } value;
    enum FvSampleT type;
} FvSampleV;

typedef struct FvColorV {
    FvSampleV component[FVCOMPONENT_T_SIZE];
} FvColorV;

extern const size_t fvsample_size[];
extern const size_t fvsample_shift[];
extern const char  *fvsample_name[];

#define FvSampleT_size(type)  fvsample_size[(type)]
#define FvSampleT_shift(type) fvsample_shift[(type)]
#define FvSampleT_name(type)  fvsample_name[(type)]


#if HAVE_OPENCL
#include "fvopencl.h"

extern const cl_channel_type fvsample_cl_channel_type[FVSAMPLE_T_SIZE];

#define FvSampleT_cl_channel_type(sample_type) fvsample_cl_channel_type[sample_type]
#endif



#define FvSampleV_s8(val)  {.value.s8  = (val), .type = FVSAMPLE_S8}
#define FvSampleV_u8(val)  {.value.u8  = (val), .type = FVSAMPLE_U8}
#define FvSampleV_s16(val) {.value.s16 = (val), .type = FVSAMPLE_S16}
#define FvSampleV_u16(val) {.value.u16 = (val), .type = FVSAMPLE_U16}
#define FvSampleV_s32(val) {.value.s16 = (val), .type = FVSAMPLE_S16}
#define FvSampleV_u32(val) {.value.u16 = (val), .type = FVSAMPLE_U16}
#define FvSampleV_f(val)   {.value.f   = (val), .type = FVSAMPLE_F}
#define FvSampleV_d(val)   {.value.d   = (val), .type = FVSAMPLE_D}

#define FvColorV_YCbCrRGBA(Y, Cb, Cr, R, G, B, A) (FvColorV) { \
    .component = { \
        [FVCOMPONENT_VOID] = FvSampleV_u8(0),  \
        [FVCOMPONENT_Y]    = FvSampleV_u8(Y),  \
        [FVCOMPONENT_CB]   = FvSampleV_u8(Cb), \
        [FVCOMPONENT_CR]   = FvSampleV_u8(Cr), \
        [FVCOMPONENT_R]    = FvSampleV_u8(R),  \
        [FVCOMPONENT_G]    = FvSampleV_u8(G),  \
        [FVCOMPONENT_B]    = FvSampleV_u8(B),  \
        [FVCOMPONENT_A]    = FvSampleV_u8(A)   \
    } \
}

extern const FvColorV fvcolor_black_fullrange_u8;

extern const FvColorV fvcolor_white_fullrange_u8;

/**
 * Pointer to a sample conversion function.
 * @param out writable output value
 * @param in  read-only input value
 */
typedef void (*fptr_fvsample_convert)(void *out, const void *in);

extern const fptr_fvsample_convert
fvsample_convert_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE];

int fvsample_convert(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t);

/**
 * Pointer to an array processing function.
 * @param out writable output array
 * @param in  read-only input array
 * @param n   number of elements to process
 */
typedef void (*fptr_fvsample_process_array_Ppn)(void *out, const void *in, size_t n);


extern const fptr_fvsample_process_array_Ppn
fvsample_convert_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE];

int fvsample_convert_array(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t, size_t n);


extern const fptr_fvsample_process_array_Ppn
fvsample_sub_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE];

int fvsample_sub_array(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t, size_t n);

extern const fptr_fvsample_process_array_Ppn
fvsample_mul_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE];

int fvsample_mul_array(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t, size_t n);

#define fv_sgn(a, z) ((a) > (z)) - ((a) < -(z))

extern const fptr_fvsample_process_array_Ppn
fvsample_sign_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE];

int fvsample_sign_array(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t, size_t n);


/**
 * Pointer to an array processing function.
 * @param out        writable output array
 * @param value      single constant
 * @param value_type type of the constant
 * @param n          number of output elements
 */
typedef void (*fptr_fvsample_process_array_Pctn)(void *out,
        const void *value, enum FvSampleT value_type, size_t n);

extern const fptr_fvsample_process_array_Pctn
fvsample_fill_array_table[FVSAMPLE_T_SIZE];

int fvsample_fill_array(void *out, enum FvSampleT out_type,
        const void *value, enum FvSampleT value_type, size_t n);


/**
 * Pointer to an array processing function.
 * @param out        writable output array
 * @param in         read-only input array
 * @param value      single constant
 * @param value_type type of the constant
 * @param n          number of elements to process
 */
typedef void (*fptr_fvsample_process_array_Ppctn)(void *out, const void *in,
        const void *value, enum FvSampleT value_type, size_t n);

extern const fptr_fvsample_process_array_Ppctn
fvsample_mac_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE];

int fvsample_mac_array(void *out, enum FvSampleT out_type,
        const void *in, enum FvSampleT in_type,
        const void *multiplier, enum FvSampleT multiplier_type, size_t n);

#ifdef __cplusplus
}
#endif

#endif /* FVSAMPLE_H */

