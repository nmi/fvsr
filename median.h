/**
 * @file      median.h
 * @brief     Functions for calculating median values
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef MEDIAN_H
#define MEDIAN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Comparison macros with strict type checking inspred by kernel.h */

#define swap(a, b) do { \
    typeof(a) swap_a_ = (a); \
    typeof(b) swap_b_ = (b); \
    (void) (&swap_a_ == &swap_b_); \
    (a) = (b); (b) = swap_a_; \
} while (0)

#define sort2(a, b) do { \
    typeof(a) sort2_a_ = (a); \
    typeof(b) sort2_b_ = (b); \
    (void) (&sort2_a_ == &sort2_b_); \
    if ((a) > (b)) \
        swap((a), (b)); \
} while (0)


#define min2(a, b) ({ \
    typeof(a) min2_a_ = (a); \
    typeof(b) min2_b_ = (b); \
    (void) (&min2_a_ == &min2_b_); \
    min2_a_ < min2_b_ ? min2_a_ : min2_b_; \
})

#define max2(a, b) ({ \
    typeof(a) max2_a_ = (a); \
    typeof(b) max2_b_ = (b); \
    (void) (&max2_a_ == &max2_b_); \
    max2_a_ > max2_b_ ? max2_a_ : max2_b_; \
})


#define DECLARE_MEDIAN3_BRANCH(T) T median3_branch_##T(T *p);
#define DECLARE_MEDIAN3_SWAP(T) T median3_swap_##T(T *p);
#define DECLARE_MEDIAN3_MINMAX(T) T median3_minmax_##T(T *p);

DECLARE_MEDIAN3_BRANCH(uint8_t)
DECLARE_MEDIAN3_SWAP(uint8_t)
DECLARE_MEDIAN3_MINMAX(uint8_t)


#define DECLARE_MEDIAN_HEAP(T,T2) \
        T median_heap_##T(T *p, size_t n, void **priv); \
        int median_heap_##T##_init(size_t n, void **priv); \
        void median_heap_##T##_free(void **priv); \

DECLARE_MEDIAN_HEAP(uint8_t, uint32_t)


#ifdef __cplusplus
}
#endif

#endif /* MEDIAN_H */

