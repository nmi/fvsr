/**
 * @file      reconst.c
 * @brief     Reconstruction functions for multiframe super-resolution
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>

#include "cement.h"
#include "convolve.h"
#include "fvbuf.h"
#include "fvframe.h"
#include "fvsample.h"
#include "multiframe.h"
#include "reconst.h"

#include "fvopencl.h"

static const char *cl_reconst_1_src =
#include "reconst_1.cl"
;

static const char *cl_reconst_4_src =
#include "reconst_4.cl"
;

int ReconstState_init(ReconstState *state,
        FvFrame *hres, FvFrame *psf_h, FvFrame *psf_v,
        uint32_t reg_radius, float reg_decay, float reg_factor,
        float desc_start, float desc_factor, FvClContext *clc,
        uint32_t num_channels) {
    size_t width  = FvBuf_width( &hres->plane[0]);
    size_t height = FvBuf_height(&hres->plane[0]);
    uint32_t v_subsampling = hres->v_subsampling;
    uint32_t h_subsampling = hres->h_subsampling;
    uint32_t padding_luma, padding_chroma;
    uint32_t subsampling = cm_min(v_subsampling, h_subsampling);

    if (clc != NULL) width = width * num_channels;

    state->reg_decay_luma = reg_decay;
    state->reg_decay_chroma = powf(reg_decay, (float) subsampling);

    state->psf_h = psf_h;
    state->psf_v = psf_v;


    state->reg_radius_luma = reg_radius; 
    if (subsampling > 1) {
        state->reg_radius_chroma = (reg_radius+1)/subsampling; 
    } else {
        state->reg_radius_chroma = reg_radius; 
    }
    padding_luma   = (uint32_t) cm_max(
            FvBuf_width(&psf_h->plane[0]) >> 1,
            FvBuf_width(&psf_v->plane[0]) >> 1);
    padding_chroma = (uint32_t) cm_max(
            FvBuf_width(&psf_h->plane[1]) >> 1,
            FvBuf_width(&psf_v->plane[1]) >> 1);
    padding_luma   = cm_max(padding_luma, state->reg_radius_luma);
    padding_chroma = cm_max(padding_chroma, state->reg_radius_chroma);

    state->reg_factor = reg_factor;
    state->desc_start = desc_start;
    state->desc_factor = desc_factor;
    state->desc = desc_start;


    if (FvFrame_init_padded(&state->hres_median, FVSAMPLE_F, width, height, 3,
            v_subsampling, h_subsampling, 0, 0, FVPAD_NONE) < 0) {
        cm_log_error("Failed to allocate hres_median (%zux%zu)",
                width, height);
        goto exit_error;
    }
    if (FvFrame_init_padded(&state->hres_norm, FVSAMPLE_F, width, height, 3,
            v_subsampling, h_subsampling, 0, 0, FVPAD_NONE) < 0) {
        cm_log_error("Failed to allocate hres_norm (%zux%zu)",
                width, height);
        goto exit_error;
    }
    if (FvFrame_init_padded(&state->hres_deblurred, FVSAMPLE_F, width, height, 3,
            v_subsampling, h_subsampling, padding_luma, padding_chroma,
            FVPAD_MIRROR) < 0) {
        cm_log_error("Failed to allocate hres_deblurred (%zux%zu)",
                width, height);
        goto exit_error;
    }
    if (FvFrame_init_padded(&state->backprojection,
            FVSAMPLE_F, width, height, 3,
            v_subsampling, h_subsampling, padding_luma, padding_chroma,
            FVPAD_MIRROR) < 0) {
        cm_log_error("Failed to allocate backprojection (%zux%zu)",
                width, height);
        goto exit_error;
    }
    if (FvFrame_init_padded(&state->tmp,
            FVSAMPLE_F, width, height, 3,
            v_subsampling, h_subsampling, padding_luma, padding_chroma,
            FVPAD_MIRROR) < 0) {
        cm_log_error("Failed to allocate tmp (%zux%zu)",
                width, height);
        goto exit_error;
    }
    if (FvFrame_init_padded(&state->regularization, FVSAMPLE_F,
            width, height, 3, v_subsampling, h_subsampling, 0, 0,
            FVPAD_NONE) < 0) {
        cm_log_error("Failed to allocate regularization (%zux%zu)",
                width, height);
        goto exit_error;
    }

    FvFilterGraph_init(&state->filtergraph, "reconstruction");
    FvFilterGraph_set_cl_context(&state->filtergraph, clc);

    FvFrame_convolve_2d_init(&state->convolve_h, &state->filtergraph.root);
    FvFrame_convolve_2d_arg_set(&state->convolve_h, &state->tmp,
            &state->backprojection, state->psf_h, 0);

    FvFrame_convolve_2d_init(&state->convolve_v, &state->filtergraph.root);
    FvFrame_convolve_2d_arg_set(&state->convolve_v, &state->backprojection,
            &state->tmp, state->psf_v, 1);

    state->clc = clc;
    if (clc != NULL) {
        size_t psf_h_size = FvBuf_width(&state->psf_h->plane[0]);
        size_t psf_v_size = FvBuf_width(&state->psf_v->plane[0]);
        size_t psf_h_radius = (uint32_t) ((psf_h_size+1) >> 1) - 1;
        size_t psf_v_radius = (uint32_t) ((psf_v_size+1) >> 1) - 1;
        char *options = (char *) malloc(256);
        if (options != NULL) {
            snprintf(options, 256, "-D PSF_H_SIZE=%zu -D PSF_V_SIZE=%zu "
                    "-D PSF_H_RADIUS=%zu -D PSF_V_RADIUS=%zu "
                    "-D REG_RADIUS=%u -D REG_DECAY=%fF -D REG_FACTOR=%fF",
                    psf_h_size, psf_v_size, psf_h_radius, psf_v_radius,
                    state->reg_radius_luma, state->reg_decay_luma,
                    state->reg_factor);
        }

        if (num_channels == 4) {
            state->clchannels = CL_RGBA;
            state->clprg = FvClContext_create_program(clc, cl_reconst_4_src, options);
        } else {
            if (num_channels != 1) {
                cm_log_warning("Unsupported number of channels: %d", num_channels);
            }
            state->clchannels = CL_R;
            state->clprg = FvClContext_create_program(clc, cl_reconst_1_src, options);
        }
        free(options);
        state->clreconst_fgb = FvClContext_create_kernel(state->clprg, "reconst_fgb");
        state->clreconst_reg = FvClContext_create_kernel(state->clprg, "reconst_reg");
        state->clwait = NULL;
    }
    return 0;

exit_error:
    ReconstState_free(state);
    return -1;
}

void ReconstState_free(ReconstState *state) {
    FvFilterGraph_free_recursive(&state->filtergraph);
    FvFrame_free(&state->tmp);
    FvFrame_free(&state->hres_median);
    FvFrame_free(&state->hres_norm);
    FvFrame_free(&state->hres_deblurred);
    FvFrame_free(&state->backprojection);
    FvFrame_free(&state->regularization);
    memset(state, 0, sizeof(ReconstState));
}

void ReconstState_start(ReconstState *state) {
    //FvFrame_convert(NULL, &state->hres_median, frame);
    //FvFrame_zero(NULL, &state->tmp);
    FvFrame_convert(NULL, &state->hres_deblurred, &state->hres_median);
    state->desc = -state->desc_start;
}

static void fast_gradient_backproject(ReconstState *state) {
    FvFrame_convert(NULL, &state->backprojection, &state->hres_deblurred);
    
    FvFrame_update_pad(NULL, &state->backprojection);
    FvFrame_convolve_2d_run(&state->convolve_h);
    FvFrame_update_pad(NULL, &state->tmp);
    FvFrame_convolve_2d_run(&state->convolve_v);
    
    FvFrame_sub(NULL, &state->backprojection, &state->hres_median);
    //FvFrame_mul(NULL, &state->backprojection, &state->hres_norm);
    FvFrame_sign(NULL, &state->backprojection, &state->backprojection);
    FvFrame_mul(NULL,  &state->backprojection, &state->hres_norm);

    
    FvFrame_update_pad(NULL, &state->backprojection);
    FvFrame_convolve_2d_run(&state->convolve_h);
    FvFrame_update_pad(NULL, &state->tmp);
    FvFrame_convolve_2d_run(&state->convolve_v);
}

#define BTV_ZERO (0.5F / 256.0F)
#define sgn(a, z) ((a) > (z)) - ((a) < -(z))

static void bilateral_gradient_reg(FvFrame *out, FvFrame *in,
        const uint32_t reg_radius_luma, const uint32_t reg_radius_chroma,
        const float reg_decay_luma, const float reg_decay_chroma) {
    uint32_t p;
    uint32_t num_planes = cm_min(out->num_planes, in->num_planes);
    for (p = 0; p < num_planes; p++) {
        int l;
        FvBuf *out_plane = &out->plane[p];
        FvBuf *in_plane  = &in->plane[p];
        FvBinMain *out_bin;
        FvBinMain *in_bin;
        float *out_mem;
        const float *in_mem;
        const ptrdiff_t out_float_pitch = (const ptrdiff_t)
                (FvBuf_row_pitch(out_plane) >> fvsample_shift[FVSAMPLE_F]);
        const ptrdiff_t  in_float_pitch = (const ptrdiff_t)
                (FvBuf_row_pitch( in_plane) >> fvsample_shift[FVSAMPLE_F]);
        const size_t width  = cm_min(FvBuf_width(out_plane),
                                     FvBuf_width(in_plane));
        const size_t height = cm_min(FvBuf_height(out_plane),
                                     FvBuf_height(in_plane));
        const int   reg_radius = (p == 0) ? (int) reg_radius_luma :
                                            (int) reg_radius_chroma;
        const float reg_decay  = (p == 0) ? reg_decay_luma :
                                            reg_decay_chroma;

        in_bin  = FvBuf_access_main( in_plane, FVBUF_ACCESS_READ);
        out_bin = FvBuf_access_main(out_plane, FVBUF_ACCESS_INIT);
        if (out_bin == NULL) continue;
        if (in_bin == NULL) continue;
        out_mem = (float *) (out_bin->mem + out_plane->offset);
        in_mem =  (float *) (in_bin->mem + in_plane->offset);

        for (l = -reg_radius; l <= reg_radius; l++) {
            int m;
            if (l < 1) m = 1;
            else m = 0;
            for (; m <= reg_radius; m++) {
                const float scale = powf(reg_decay, (float)(abs(l) + abs(m)));
                int i;
                const ptrdiff_t positive_shift =
                        (ptrdiff_t) m * in_float_pitch + (ptrdiff_t) l;
                const ptrdiff_t negative_shift =
                        (ptrdiff_t) (-m) * in_float_pitch +
                        (ptrdiff_t) (-l);
                for (i = 0; i < height; i++) {
                    ptrdiff_t out_row = (ptrdiff_t) i * out_float_pitch;
                    ptrdiff_t in_row =  (ptrdiff_t) i * in_float_pitch;
                    ptrdiff_t negative_shifted_row = in_row + negative_shift;
                    ptrdiff_t positive_shifted_row = in_row + positive_shift;
                    ptrdiff_t j;
                    for (j = 0; j < width; j++) {
                        const float val = in_mem[in_row + j];
                        const float val_nshift = in_mem[negative_shifted_row + j]; 
                        const float val_pshift = in_mem[positive_shifted_row + j]; 
                        const int s1 = sgn(val - val_pshift, BTV_ZERO);
                        const int s2 = sgn(val_nshift - val, BTV_ZERO);
                        out_mem[out_row + j] += scale * (float) (s1 - s2);
                    }
                }
            }
        }
    }
}

void ReconstState_step(ReconstState *state) {
    uint32_t p;
    uint32_t num_planes = state->hres_deblurred.num_planes;

    if (state->clc != NULL) {
        FvFrame swap;
        for (p = 0; p < num_planes; p++) {
            FvBinClImg *deblurred_bin = FvBuf_access_climg(
                    &state->hres_deblurred.plane[p], FVBUF_ACCESS_WRITE, state->clchannels, state->clc);
            cl_mem *deblurred_img = &deblurred_bin->mem;

            cl_mem *backproject_img = &FvBuf_access_climg(&state->backprojection.plane[p],
                    FVBUF_ACCESS_WRITE, state->clchannels, state->clc)->mem;
            cl_mem *tmp_img = &FvBuf_access_climg(&state->tmp.plane[p],
                    FVBUF_ACCESS_WRITE, state->clchannels, state->clc)->mem;
            cl_mem *psf_h_img = &FvBuf_access_clbuf(&state->psf_h->plane[p],
                        FVBUF_ACCESS_READ, state->clc)->mem;
            cl_mem *psf_v_img = &FvBuf_access_clbuf(&state->psf_v->plane[p],
                        FVBUF_ACCESS_READ, state->clc)->mem;
            cl_int ret;
            cl_event nextwait;

            clSetKernelArg(state->clreconst_fgb, 0, sizeof(cl_mem), backproject_img);
            clSetKernelArg(state->clreconst_fgb, 1, sizeof(cl_mem), deblurred_img);
            clSetKernelArg(state->clreconst_fgb, 2, sizeof(cl_mem),
                    &FvBuf_access_climg(&state->hres_median.plane[p],
                        FVBUF_ACCESS_READ, state->clchannels, state->clc)->mem);
            clSetKernelArg(state->clreconst_fgb, 3, sizeof(cl_mem),
                    &FvBuf_access_climg(&state->hres_norm.plane[p],
                        FVBUF_ACCESS_READ, state->clchannels, state->clc)->mem);
            clSetKernelArg(state->clreconst_fgb, 4, sizeof(cl_mem), psf_h_img);
            clSetKernelArg(state->clreconst_fgb, 5, sizeof(cl_mem), psf_v_img);

            if (state->clwait == NULL) {
                ret = clEnqueueNDRangeKernel(state->clc->commandq,
                        state->clreconst_fgb, 2, NULL, deblurred_bin->region,
                        NULL, 0, NULL, &state->clwait);
            } else {
                ret = clEnqueueNDRangeKernel(state->clc->commandq,
                        state->clreconst_fgb, 2, NULL, deblurred_bin->region,
                        NULL, 1, &state->clwait, &nextwait);
                state->clwait = nextwait;
            }
            if (ret != CL_SUCCESS) {
                cm_log_error("Failed to run OpenCL reconstruction: %s",
                        fvclErrorString(ret));
                return;
            }

            clSetKernelArg(state->clreconst_reg, 0, sizeof(cl_mem), tmp_img);
            clSetKernelArg(state->clreconst_reg, 1, sizeof(cl_mem), deblurred_img);
            clSetKernelArg(state->clreconst_reg, 2, sizeof(cl_mem), backproject_img);
            clSetKernelArg(state->clreconst_reg, 3, sizeof(cl_mem), psf_h_img);
            clSetKernelArg(state->clreconst_reg, 4, sizeof(cl_mem), psf_v_img);
            clSetKernelArg(state->clreconst_reg, 5, sizeof(cl_float), &state->desc);
            ret = clEnqueueNDRangeKernel(state->clc->commandq,
                    state->clreconst_reg, 2, NULL, deblurred_bin->region,
                    NULL, 1, &state->clwait, &nextwait);
            state->clwait = nextwait;
 
            if (ret != CL_SUCCESS) {
                cm_log_error("Failed to run OpenCL reconstruction: %s",
                        fvclErrorString(ret));
                return;
            }
        }

            /* 
            memcpy(&swap, &state->hres_deblurred, sizeof(FvFrame));
            memcpy(&state->hres_deblurred, &state->tmp, sizeof(FvFrame));
            memcpy(&state->tmp, &swap, sizeof(FvFrame));
            */
            swap = state->hres_deblurred;
            state->hres_deblurred = state->tmp;
            state->tmp = swap;
    } else {
        FvFrame_update_pad(NULL, &state->hres_deblurred);
        fast_gradient_backproject(state);
        FvFrame_zero(NULL, &state->regularization);

        bilateral_gradient_reg(&state->regularization,
                &state->hres_deblurred, state->reg_radius_luma,
                state->reg_radius_chroma, state->reg_decay_luma,
                state->reg_decay_chroma);


        for (p = 0; p < num_planes; p++) {
            FvBuf * bp_plane = &state->backprojection.plane[p];
            FvBuf * hr_plane = &state->hres_deblurred.plane[p];
            FvBuf *reg_plane = &state->regularization.plane[p];
            float *bp_row, *hr_row, *reg_row;
            size_t width  = FvBuf_width(hr_plane);
            size_t height = FvBuf_height(hr_plane);
            size_t i;
            FvBinMain * bp_bin = FvBuf_access_main( bp_plane, FVBUF_ACCESS_WRITE);
            FvBinMain * hr_bin = FvBuf_access_main( hr_plane, FVBUF_ACCESS_WRITE);
            FvBinMain *reg_bin = FvBuf_access_main(reg_plane, FVBUF_ACCESS_READ);
            size_t bp_pitch  = FvBuf_row_pitch_samples(bp_plane);
            size_t hr_pitch  = FvBuf_row_pitch_samples(hr_plane);
            size_t reg_pitch = FvBuf_row_pitch_samples(reg_plane);
            if ((bp_bin == NULL) || (reg_bin == NULL) || (hr_bin == NULL)) {
                continue;
            }
            bp_row  = (float *) ( bp_bin->mem +  bp_plane->offset);
            hr_row  = (float *) ( hr_bin->mem +  hr_plane->offset);
            reg_row = (float *) (reg_bin->mem + reg_plane->offset);
            for (i = 0; i < height; i++) {
                fvsample_mac_array(bp_row, FVSAMPLE_F, reg_row, FVSAMPLE_F,
                        &state->reg_factor, FVSAMPLE_F, width);
                fvsample_mac_array(hr_row, FVSAMPLE_F, bp_row, FVSAMPLE_F,
                        &state->desc, FVSAMPLE_F, width);
                bp_row  += bp_pitch;
                hr_row  += hr_pitch;
                reg_row += reg_pitch;
            }
        }
    }
    //fprintf(stderr, "reg: %f, desc: %f\n", state->reg_factor * 256, state->desc * 256);
    state->desc *= state->desc_factor;
}


