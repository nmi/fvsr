/**
 * @file      filter.c
 * @brief     Filtering functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <math.h>
#include <stdio.h>

#include "cement.h"
#include "convolve.h"
#include "filter.h"
#include "fvbuf.h"
#include "yuv4mpeg.h"
/*#include "multiframe.h"*/

#if 0
int FvFrame2d_resize_linear(FvFrame2d *out, const FvFrame2d *in,
                             FvBuf2d *tmp) {
    uint32_t num_planes = cm_min(out->num_planes, in->num_planes);
    uint32_t p;
    uint32_t tmpw = 0;
    uint32_t tmph = 0;
    float    phasex = 0.5F;
    float    phasey = 0.5F;
    if (out->plane[0].buf.sample_type != FV_TYPE_UINT8) {
        cm_log_error("Unsupported output frame sample type '%s', "
                "expected '%s'",
                fv_type_names[out->plane[0].buf.sample_type],
                fv_type_names[FV_TYPE_UINT8]);
        return -1;
    }
    if (in->plane[0].buf.sample_type != FV_TYPE_UINT8) {
        cm_log_error("Unsupported input frame sample type '%s', "
                "expected '%s'",
                fv_type_names[in->plane[0].buf.sample_type],
                fv_type_names[FV_TYPE_UINT8]);
        return -1;
    }
    for (p = 0; p < num_planes; p++) {
        uint32_t w = out->plane[p].width;
        uint32_t h = in->plane[p].height;
        if (w > tmpw) tmpw = w;
        if (h > tmph) tmph = h;
    }
    if ((tmp->width < maxw) || (tmp->height < maxh)) {
        FvBuf2d_free(tmp);
        if (FvBuf2d_init_with_padding(tmp, FV_CC_UNDEFINED,
                    FV_TYPE_UINT8, tmpw, tmph, 0, FV_PADDING_NONE) < 0) {
            cm_log_error("Failed to allocate temporary buffer");
            return -1;
        }
    }
    for (p = 0; p < num_planes; p++) {
        FvBuf2d    *inplane  = &in->plane[p];
        uint8_t    *inbuf    = inplane->buf.samples.u8;
        FvBuf2d    *outplane = &out->plane[p];
        uint8_t    *outbuf   = outplane->buf.samples.u8;
        uint8_t    *tmpbuf   = tmp->buf.samples.u8;
        uint32_t i;
        float ystep = (float) inplane->height / (float) outplane->height;
        float xstep = (float) inplane->width  / (float) outplane->width;
        size_t last = inplane->width - 1;

        if (xstep <= 0) {
            /* Horizontal interpolation */
            for (i = 0; i <= inplane->height; i++) {
                uint32_t inx   = 0;
                uint32_t tmpx  = 0;
                float    delta = 0.0F;
                uint32_t tmpwidth = outplane->width;
                while ((delta < phasex) && (tmpx < tmpwidth)) {
                    tmpbuf[tmpx] = inbuf[0]; 
                    delta += xstep;
                    tmpx++;
                }
                while ((inx < inplane->width - 1) && (tmpx < tmpwidth)) {
                    float i1 = inbuf[inx];
                    float i2 = inbuf[inx + 1];
                    float t = i1 + (i2 - i1) / delta;
                    tmpbuf[tmpx] = (uint8_t) lrintf(t); 
                    delta += xstep;
                    if (delta >= 1.0) {
                        delta -= 1.0;
                        inx++; 
                    }
                    tmpx++;
                }
                while ((tmpx < tmpwidth)) {
                    tmpbuf[tmpx] = inbuf[inx];
                    tmpx++;
                }
            }
        } else {
            cm_log_error("Horizontal downsampling not implemented");
        }
        /* Vertical interpolation */
        for (i = step; i <= h; i += step) {
            uint32_t j;
            size_t outrow = (size_t) i * (size_t) outplane->stride_bytes;
            for (j = 0; j < w; j++) {
                uint32_t k;
                float v1, v2, d;
                uint8_t *block = outbuf + (size_t) (i-step) *
                        (size_t) outplane->stride_bytes + (size_t) j;
                v1 = (float) *block;
                v2 = (float) outbuf[outrow + j];
                d = (v2 - v1) / (float) step;
                v1 += d;
                block += outplane->stride_bytes;
                for (k = 1; k < step; k++) {
                    if (*block == 0) {
                        *block = (uint8_t) lrintf(v1);
                    }
                    v1 += d; 
                    block += outplane->stride_bytes;
                }
            }
        }
    }
}
#endif

#if 0
void FvFrame2d_interpolate_median(FvFrame2d *frame, FvFrame2d *tmp) {
    void    *mheap;
    uint8_t *medianbuf;
    uint32_t num_planes = cm_min(frame->num_planes, (uint32_t) 3);
    uint32_t p;

    medianbuf = (uint8_t *) malloc((size_t) window->num_elements);
    if (medianbuf == NULL) {
        cm_log_error("Failed to allocate buffer for median calculation");
        return;
    }

    if (median_heap_uint8_t_init(window->num_elements, &mheap) < 0) {
        cm_log_error("Failed to initialize median heap");
        goto exit_error;
    }

    if (((FvBuf *) frame)->sample_type != FV_TYPE_UINT8) {
        cm_log_error("Unsupported output frame sample type '%s', "
                "expected '%s'",
                fv_type_names[((FvBuf *) frame)->sample_type],
                fv_type_names[FV_TYPE_UINT8]);
        goto exit_error;
    }
 
    if (tmp->num_planes > 0) {
        if ((tmp->num_planes < frame->num_planes) ||
            (tmp->plane[0].width < frame->plane[0].width) ||
            (tmp->plane[0].height < frame->plane[0].height) ||
            (tmp->v_subsampling != frame->v_subsampling) ||
            (tmp->h_subsampling != frame->h_subsampling) ||
            (((FvBuf *) tmp)->sample_type != FV_TYPE_UINT8)) {
            cm_log_info("Incompatible temporary frame, reallocating...");
            FvFrame2d_free(tmp);
        }
    }
    if (tmp->num_planes == 0) {
        if (FvFrame2d_init_with_padding(tmp,
                ((FvBuf *) frame)->sample_type,
                frame->plane[0].width, frame->plane[0].height,
                frame->num_planes, frame->v_subsampling,
                frame->h_subsampling, 0, 0, FV_PADDING_NONE) < 0) {
            cm_log_error("Failed to allocate a temporary frame");
            goto exit_error;
        }
    }

    for (p = 0; p < num_planes; p++) {
        FvBuf2d    *outplane = &frame->plane[p];
        FvBuf2d    *tmpplane = &tmp->plane[p];
        uint8_t    *outbuf   = outplane->buf.samples.u8;
        uint8_t    *tmpbuf   = tmpplane->buf.samples.u8;
        uint32_t w = outplane->width;
        uint32_t h = outplane->height;
        uint32_t i;

        for (i = 0; i < h; i++) {
            uint32_t j;
            size_t outrow = (size_t) i * (size_t) outplane->stride_bytes;
            size_t tmprow = (size_t) i * (size_t) tmpplane->stride_bytes;

            for (j = 0; j < w; j++) {
                if (outbuf[outrow + j] == 0) {
                    size_t k;
                    size_t wsize = 0;
                    median_block(
                    for (k = 0; k < window->num_elements; k++) {
                        int x, y;
                        Point2D_i32_f32 *point = (Point2D_i32_f32 *)
                            Array_element(window, (size_t) k);
                        x = (int) j + point->x;
                        y = (int) i + point->y;
                        if ((x >= 0) && (x < w) && (y >= 0) && (y < h)) {
                            uint8_t val = outbuf[(size_t) y *
                                    (size_t) outplane->stride_bytes + (size_t) x];
                            if (val > 0) {
                                medianbuf[wsize] = val;
                                wsize++;
                            }
                        }

                    }
                    tmpbuf[tmprow + j] = median_heap_uint8_t(medianbuf,
                            wsize, &mheap);
                } else {
                    tmpbuf[tmprow + j] = outbuf[outrow + j];
                }
            }
        }
        for (i = 0; i < h; i++) {
            memcpy(outbuf, tmpbuf, w);
            outbuf += (size_t) outplane->stride_bytes;
            tmpbuf += (size_t) tmpplane->stride_bytes;
        }

/* Spatial median filter over the final image */
/*
        for (i = 0; i < h; i++) {
            uint32_t j;
            size_t outrow = (size_t) i * (size_t) outplane->stride_bytes;

            for (j = 0; j < w; j++) {
                size_t area = window->num_elements/8;
                size_t k;
                size_t wsize = 0;

                for (k = 0; k < area; k++) {
                    int x, y;
                    Point2D_i32_f32 *point = (Point2D_i32_f32 *)
                        Array_element(window, (size_t) k);
                    x = (int) j + point->x;
                    y = (int) i + point->y;
                    if ((x >= 0) && (x < w) && (y >= 0) && (y < h)) {
                        uint8_t val = tmpbuf[(size_t) y *
                            (size_t) tmpplane->stride_bytes + (size_t) x];
                        medianbuf[wsize] = val;
                        wsize++;
                    }

                }
                outbuf[outrow + j] = median_heap_uint8_t(medianbuf,
                        wsize, &mheap);
            }
        }
*/
    }
exit_error:
    median_heap_uint8_t_free(&mheap);
    free(medianbuf);
}
#endif


