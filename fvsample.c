/**
 * @file      fvsample.c
 * @brief     Processing of samples and sample arrays
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <stdint.h>
#include <stdio.h>

#include "config.h"
#include "cement.h"
#include "fvsample.h"

#if HAVE_OPENCL
#include "fvopencl.h"
#endif


const size_t fvsample_size[FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_VOID] = 1,
    [FVSAMPLE_U8]   = 1,
    [FVSAMPLE_S8]   = 1,
    [FVSAMPLE_U16]  = 2,
    [FVSAMPLE_S16]  = 2,
    [FVSAMPLE_U32]  = 4,
    [FVSAMPLE_S32]  = 4,
    [FVSAMPLE_F]    = 4,
    [FVSAMPLE_D]    = 8
};

const size_t fvsample_shift[FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_VOID] = 0,
    [FVSAMPLE_U8]   = 0,
    [FVSAMPLE_S8]   = 0,
    [FVSAMPLE_U16]  = 1,
    [FVSAMPLE_S16]  = 1,
    [FVSAMPLE_U32]  = 2,
    [FVSAMPLE_S32]  = 2,
    [FVSAMPLE_F]    = 2,
    [FVSAMPLE_D]    = 3
};

const char *fvsample_name[FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_VOID] = "void",
    [FVSAMPLE_U8]   = "u8",
    [FVSAMPLE_S8]   = "s8",
    [FVSAMPLE_U16]  = "u16",
    [FVSAMPLE_S16]  = "s16",
    [FVSAMPLE_U32]  = "u32",
    [FVSAMPLE_S32]  = "s32",
    [FVSAMPLE_F]    = "f",
    [FVSAMPLE_D]    = "d"
};


const cl_channel_type fvsample_cl_channel_type[FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_VOID] = CL_UNSIGNED_INT8,
    [FVSAMPLE_U8]   = CL_UNSIGNED_INT8,
    [FVSAMPLE_S8]   = CL_SIGNED_INT8,
    [FVSAMPLE_U16]  = CL_UNSIGNED_INT16,
    [FVSAMPLE_S16]  = CL_SIGNED_INT16,
    [FVSAMPLE_U32]  = CL_UNSIGNED_INT32,
    [FVSAMPLE_S32]  = CL_SIGNED_INT32,
    [FVSAMPLE_F]    = CL_FLOAT,
    [FVSAMPLE_D]    = 0
};

const FvColorV fvcolor_black_fullrange_u8 =
    FvColorV_YCbCrRGBA(0, 128, 128, 0, 0, 0, 255);

const FvColorV fvcolor_white_fullrange_u8 =
    FvColorV_YCbCrRGBA(255, 128, 128, 255, 255, 255, 255);

#define U8_TO_F (1.0F/256.0F)
static void fvsample_u8_to_f(void *out, const void *in) {
    float         *out_p = (float *)        out;
    const uint8_t *in_p  = (const uint8_t *) in;
    *out_p = ((float) *in_p) * U8_TO_F;
}

static void fvsample_f_to_u8(void *out, const void *in) {
    uint8_t       *out_p = (uint8_t *)      out;
    const float   *in_p  = (const float *)   in;
    float val = 256.0F * cm_min(1.0F, cm_max(0.0F, *in_p));
    *out_p = (uint8_t) cm_min((uint32_t) 255, (uint32_t) val);
}

static void fvsample_u8_to_u8(void *out, const void *in) {
    uint8_t       *out_p = (uint8_t *)      out;
    const uint8_t *in_p  = (const uint8_t *) in;
    *out_p = *in_p;
}

static void fvsample_f_to_f(void *out, const void *in) {
    float       *out_p = (float *)      out;
    const float *in_p  = (const float *) in;
    *out_p = *in_p;
}

const fptr_fvsample_convert fvsample_convert_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_U8] = {
        [FVSAMPLE_U8] = fvsample_u8_to_u8,
        [FVSAMPLE_F]  = fvsample_f_to_u8,
    },
    [FVSAMPLE_F] = {
        [FVSAMPLE_U8] = fvsample_u8_to_f,
        [FVSAMPLE_F]  = fvsample_f_to_f,
    },
};

int fvsample_convert(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t) {
    fptr_fvsample_convert convert = fvsample_convert_table[out_t][in_t];
    if (convert == NULL) return -1;
    convert(out, in);
    return 0;
}


static void fvsample_convert_array_u8_to_u8(void *out, const void *in, size_t n) {
    uint8_t       *out_u8 =       (uint8_t *) out;
    const uint8_t *in_u8  = (const uint8_t *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_u8[i] = in_u8[i];
    } 
}

static void fvsample_convert_array_s8_to_u8(void *out, const void *in, size_t n) {
    uint8_t       *out_u8 = (     uint8_t *) out;
    const uint8_t *in_s8  = (const int8_t *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_u8[i] = (uint8_t) ((1 << 7) + (int) in_s8[i]);
    } 
}

static void fvsample_convert_array_s16_to_u8(void *out, const void *in, size_t n) {
    uint8_t       *out_u8 = (      uint8_t *) out;
    const int16_t *in_s16 = (const int16_t *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_u8[i] = (uint8_t) (((uint16_t) in_s16[i] +
                    ((uint16_t) 1 << 15)) >> 8);
    } 
}

static void fvsample_convert_array_u16_to_u8(void *out, const void *in, size_t n) {
    uint8_t        *out_u8 = (       uint8_t *) out;
    const uint16_t *in_u16 = (const uint16_t *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_u8[i] = (uint8_t) (in_u16[i] >> 8);
    } 
}

static void fvsample_convert_array_s32_to_u8(void *out, const void *in, size_t n) {
    uint8_t       *out_u8 = (      uint8_t *) out;
    const int32_t *in_s32 = (const int32_t *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_u8[i] = (uint8_t) (((uint32_t) in_s32[i] +
                    ((uint32_t) 1 << 31)) >> 24);
    } 
}

static void fvsample_convert_array_u32_to_u8(void *out, const void *in, size_t n) {
    uint8_t        *out_u8 = (       uint8_t *) out;
    const uint32_t *in_u32 = (const uint32_t *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_u8[i] = (uint8_t) (in_u32[i] >> 24);
    } 
}

static void fvsample_convert_array_f_to_u8(void *out, const void *in, size_t n) {
    uint8_t     *out_u8 = (    uint8_t *) out;
    const float *in_f   = (const float *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        float val = 256.0F * cm_min(1.0F, cm_max(0.0F, in_f[i]));
        out_u8[i] = (uint8_t) cm_min((uint32_t) 255, (uint32_t) val);
    }
}

static void fvsample_convert_array_d_to_u8(void *out, const void *in, size_t n) {
    uint8_t      *out_u8 = (     uint8_t *) out;
    const double *in_d   = (const double *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        double val = 256.0 * cm_min(1.0, cm_max(0.0, in_d[i]));
        out_u8[i] = (uint8_t) cm_min((uint32_t) 255, (uint32_t) val);
    }
}

static void fvsample_convert_array_u8_to_f(void *out, const void *in, size_t n) {
    float         *out_f = (        float *) out;
    const uint8_t *in_u8 = (const uint8_t *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_f[i] = ((float) in_u8[i]) / 256.0F;
    }
}

static void fvsample_convert_array_f_to_f(void *out, const void *in, size_t n) {
    float       *out_f = (      float *) out;
    const float *in_f  = (const float *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_f[i] = in_f[i];
    }
}

static void fvsample_convert_array_d_to_f(void *out, const void *in, size_t n) {
    float        *out_f = (       float *) out;
    const double *in_d  = (const double *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_f[i] = (float) in_d[i];
    }
}

static void fvsample_convert_array_u8_to_d(void *out, const void *in, size_t n) {
    double        *out_d = (       double *) out;
    const uint8_t *in_u8 = (const uint8_t *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_d[i] = ((double) in_u8[i]) / 256.0;
    }
}

static void fvsample_convert_array_f_to_d(void *out, const void *in, size_t n) {
    double      *out_d = (     double *) out;
    const float *in_f  = (const float *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_d[i] = (double) in_f[i];
    }
}

static void fvsample_convert_array_d_to_d(void *out, const void *in, size_t n) {
    double       *out_d = (      double *) out;
    const double *in_d  = (const double *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_d[i] = in_d[i];
    }
}

const fptr_fvsample_process_array_Ppn
fvsample_convert_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_U8] = {
        [FVSAMPLE_S8]  = fvsample_convert_array_s8_to_u8,
        [FVSAMPLE_U8]  = fvsample_convert_array_u8_to_u8,
        [FVSAMPLE_S16] = fvsample_convert_array_s16_to_u8,
        [FVSAMPLE_U16] = fvsample_convert_array_u16_to_u8,
        [FVSAMPLE_S32] = fvsample_convert_array_s32_to_u8,
        [FVSAMPLE_U32] = fvsample_convert_array_u32_to_u8,
        [FVSAMPLE_F]   = fvsample_convert_array_f_to_u8,
        [FVSAMPLE_D]   = fvsample_convert_array_d_to_u8,
    },
    [FVSAMPLE_F] = {
        [FVSAMPLE_U8]  = fvsample_convert_array_u8_to_f,
        [FVSAMPLE_F]   = fvsample_convert_array_f_to_f,
        [FVSAMPLE_D]   = fvsample_convert_array_d_to_f,
    },
    [FVSAMPLE_D] = {
        [FVSAMPLE_U8]  = fvsample_convert_array_u8_to_d,
        [FVSAMPLE_F]   = fvsample_convert_array_f_to_d,
        [FVSAMPLE_D]   = fvsample_convert_array_d_to_d,
    }
};

int fvsample_convert_array(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t, size_t n) {
    fptr_fvsample_process_array_Ppn convert =
            fvsample_convert_array_table[out_t][in_t];
    if (convert == NULL) return -1;
    convert(out, in, n);
    return 0;
}


static void fvsample_sub_array_f_f(void *out, const void *in, size_t n) {
    float       *out_f = (      float *) out;
    const float *in_f  = (const float *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_f[i] -= in_f[i];
    }
}

const fptr_fvsample_process_array_Ppn
fvsample_sub_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_F] = {
        [FVSAMPLE_F] = fvsample_sub_array_f_f,
    }
};

int fvsample_sub_array(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t, size_t n) {
    fptr_fvsample_process_array_Ppn sub =
            fvsample_sub_array_table[out_t][in_t];
    if (sub == NULL) return -1;
    sub(out, in, n);
    return 0;
}


static void fvsample_mul_array_f_f(void *out, const void *in, size_t n) {
    float       *out_f = (      float *) out;
    const float *in_f  = (const float *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_f[i] *= in_f[i];
    }
}

const fptr_fvsample_process_array_Ppn
fvsample_mul_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_F] = {
        [FVSAMPLE_F] = fvsample_mul_array_f_f,
    }
};

int fvsample_mul_array(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t, size_t n) {
    fptr_fvsample_process_array_Ppn mul =
            fvsample_mul_array_table[out_t][in_t];
    if (mul == NULL) return -1;
    mul(out, in, n);
    return 0;
}


#define SGN_ZERO_F (0.5F / 256.0F)

static void fvsample_sign_array_f_f(void *out, const void *in, size_t n) {
    float       *out_f = (      float *) out;
    const float *in_f  = (const float *) in;
    size_t i;
    for (i = 0; i < n; i++) {
        out_f[i] = (float) fv_sgn(in_f[i], SGN_ZERO_F);
    }
}

const fptr_fvsample_process_array_Ppn
fvsample_sign_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_F] = {
        [FVSAMPLE_F] = fvsample_sign_array_f_f,
    }
};

int fvsample_sign_array(void *out, enum FvSampleT out_t,
        const void *in, enum FvSampleT in_t, size_t n) {
    fptr_fvsample_process_array_Ppn sign =
            fvsample_sign_array_table[out_t][in_t];
    if (sign == NULL) return -1;
    sign(out, in, n);
    return 0;
}


static void fvsample_fill_array_u8(void *out, const void *value,
        enum FvSampleT value_type, size_t n) {
    uint8_t *out_u8 = (uint8_t *) out;
    size_t i;
    uint8_t v = 0;
    fvsample_convert(&v, FVSAMPLE_U8, value, value_type);
    for (i = 0; i < n; i++) {
        out_u8[i] = v;
    }
}

static void fvsample_fill_array_f(void *out, const void *value,
        enum FvSampleT value_type, size_t n) {
    float *out_f = (float *) out;
    size_t i;
    uint8_t v = 0;
    fvsample_convert(&v, FVSAMPLE_F, value, value_type);
    for (i = 0; i < n; i++) {
        out_f[i] = v;
    }
}

const fptr_fvsample_process_array_Pctn
fvsample_fill_array_table[FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_U8] = fvsample_fill_array_u8,
    [FVSAMPLE_F] = fvsample_fill_array_f
};

int fvsample_fill_array(void *out, enum FvSampleT out_type,
        const void *value, enum FvSampleT value_type, size_t n) {
    fptr_fvsample_process_array_Pctn fill =
            fvsample_fill_array_table[out_type];
    if (fill == NULL) return -1;
    fill(out, value, value_type, n);
    return 0;
}
 



static void fvsample_mac_array_u8_to_u8(void *out, const void *in,
        const void *multiplier, enum FvSampleT multiplier_type, size_t n) {
    uint8_t       *out_u8 = (      uint8_t *) out;
    const uint8_t *in_u8  = (const uint8_t *) in;
    size_t i;
    uint8_t m = 0;
    fvsample_convert(&m, FVSAMPLE_U8, multiplier, multiplier_type);
    for (i = 0; i < n; i++) {
        out_u8[i] = (uint8_t) cm_min((uint32_t) 255,
                (uint32_t) out_u8[i] +
                (((uint32_t) m * (uint32_t) in_u8[i]) >> 8));
    }
}

static void fvsample_mac_array_u8_to_f(void *out, const void *in,
        const void *multiplier, enum FvSampleT multiplier_type, size_t n) {
    float         *out_f = (        float *) out;
    const uint8_t *in_u8 = (const uint8_t *) in;
    size_t i;
    float m = 0.0F;
    fvsample_convert(&m, FVSAMPLE_F, multiplier, multiplier_type);
    for (i = 0; i < n; i++) {
        out_f[i] = cm_min(1.0F, out_f[i] + m * ((float) in_u8[i]) / 256.0F);
    }
}

static void fvsample_mac_array_f_to_u8(void *out, const void *in,
        const void *multiplier, enum FvSampleT multiplier_type, size_t n) {
    uint8_t     *out_u8 = (    uint8_t *) out;
    const float *in_f   = (const float *) in;
    size_t i;
    float m = 0.0F;
    fvsample_convert(&m, FVSAMPLE_F, multiplier, multiplier_type);
    for (i = 0; i < n; i++) {
        float val = 256.0F * cm_min(1.0F, cm_max(0.0F, m * in_f[i]));
        out_u8[i] = (uint8_t) cm_min((uint32_t) 255,
                (uint32_t) out_u8[i] + (uint32_t) val);
    }
}

static void fvsample_mac_array_f_to_f(void *out, const void *in,
        const void *multiplier, enum FvSampleT multiplier_type, size_t n) {
    float       *out_f = (      float *) out;
    const float *in_f  = (const float *) in;
    size_t i;
    float m = 0.0F;
    fvsample_convert(&m, FVSAMPLE_F, multiplier, multiplier_type);
    for (i = 0; i < n; i++) {
        out_f[i] = out_f[i] + m * in_f[i];
    }
}


const fptr_fvsample_process_array_Ppctn
fvsample_mac_array_table[FVSAMPLE_T_SIZE][FVSAMPLE_T_SIZE] = {
    [FVSAMPLE_U8] = {
        [FVSAMPLE_U8] = fvsample_mac_array_u8_to_u8,
        [FVSAMPLE_F]  = fvsample_mac_array_f_to_u8,
    },
    [FVSAMPLE_F] = {
        [FVSAMPLE_U8] = fvsample_mac_array_u8_to_f,
        [FVSAMPLE_F]  = fvsample_mac_array_f_to_f,
    },
};

int fvsample_mac_array(void *out, enum FvSampleT out_type,
        const void *in, enum FvSampleT in_type,
        const void *multiplier, enum FvSampleT multiplier_type, size_t n) {
    fptr_fvsample_process_array_Ppctn mac =
            fvsample_mac_array_table[out_type][in_type];
    if (mac == NULL) return -1;
    mac(out, in, multiplier, multiplier_type, n);
    return 0;
}
 

