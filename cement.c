/**
 * @file      cement.c
 * @brief     C Environment -- cross-platform C programming utilities
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright 2-clause BSD
 *
 * @section LICENSE
 *
 * Copyright (c) 2013, Niko Mikkilä
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer
 *    in the documentation and/or other materials provided with
 *    the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "config.h"

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#if HAVE_MALLOC_H
#include <malloc.h>
#endif

#include "cement.h"


/**********************************************************************/
/*               */
/* Error logging */
/*               */
/*****************/


static FILE *log_target = NULL;
static enum cm_log_level log_level = CM_LOG_LEVEL_INFO;
static const char * const log_level_strings[] = {
    "", "ERROR", "WARNING", "INFO", "DEBUG"};


/**
 * Returns current target stream for logging macros.
 * Default target is stderr and it can be changed with
 * cm_set_log_target.
 *
 * @return log target stream
 */
FILE *cm_get_log_target(void) {
    if (log_target != NULL) return log_target;
    else return CM_DEFAULT_LOG_TARGET;
}


/**
 * Sets target stream for CM_ERROR, CM_WARNING and CM_INFO logging macros.
 *
 * @param target FILE stream for logging
 */
void cm_set_log_target(FILE *target) {
    log_target = target;
}

/**
 * Returns logging level: CM_LOG_LEVEL_ERROR, CM_LOG_LEVEL_WARNING or
 * CM_LOG_LEVEL_INFO.
 *
 * @return current logging level.
 */
enum cm_log_level cm_get_log_level(void) {
    return log_level;
}


/**
 * Sets logging level. To only log error messages, use
 * CM_LOG_LEVEL_ERROR. For errors and warnings, use CM_LOG_LEVEL_WARNING.
 * For everything, use CM_LOG_LEVEL_INFO.
 *
 * @param level        logging level
 */
void cm_set_log_level(enum cm_log_level level) {
    log_level = level;
}


/**
 * Writes a message to the log. This function should be called through
 * the CM_INFO(), CM_ERROR() or CM_WARNING() preprocessor macros.
 *
 * @param level    message logging level: CM_LOG_LEVEL_ERROR or
 *                 CM_LOG_LEVEL_WARNING
 * @param file     source file name
 * @param function source function name
 * @param line     source line number
 * @param format   output formatting string to pass to fprintf()
 * @param ...      variable arguments for fprintf()
 */
void cm_log(enum cm_log_level msg_level, const char *file,
             const char *function, int line, const char *format, ...) {
    va_list ap;
    FILE *out = log_target ? log_target : CM_DEFAULT_LOG_TARGET;

    if (msg_level < CM_LOG_LEVEL_ERROR)
        msg_level = CM_LOG_LEVEL_ERROR;
    else if (msg_level > CM_LOG_LEVEL_DEBUG)
        msg_level = CM_LOG_LEVEL_DEBUG; 
    if (msg_level > log_level) return;
    if (out == NULL) return;

    if (msg_level == CM_LOG_LEVEL_INFO) {
        fprintf(out, "[%s]: ",
                log_level_strings[msg_level]);
    } else {
        if (file != NULL) {
            fprintf(out, "[%s in %s:%s():%d]: ",
                    log_level_strings[msg_level], file, function, line);
        } else {
            fprintf(out, "[%s in %s():%d]: ",
                    log_level_strings[msg_level], function, line);
        }
    }
    va_start(ap, format);
    vfprintf(out, format, ap);
    fputs("\n", out);
}


/**
 * Converts errno (errno.h) error numbers to string messages, which
 * is useful when printing formatted log messages. This implementation
 * is thread-safe in the same way as strerror_l: the returned string is
 * stored in a thread-local static buffer. If strerror_l is available,
 * it is used instead.
 *
 * @param errnum an errno error number
 *
 * @return an error string that corresponds to errnum or NULL if there
 *         is an error. The returned string must be freed by the caller
 *         with free().
 *
 * Example:
 * @code
 *     file = fopen(filename);
 *     if (!file) {
 *         cm_log_error("Unable to open file \"%s\" (%s)",
 *                       filename, cm_strerror(errno));
 *     }
 * @endcode
 */
#define STRERROR_MAX 256

#if HAVE_STRERROR_L 

/* Use strerror_l when available */

#include <locale.h>

char *cm_strerror(int errnum) {
    return strerror_l(errnum, LC_GLOBAL_LOCALE);
}

#elif defined(HAVE_STRERROR_R)   && HAVE_STRERROR_R
#if   defined(STRERROR_R_CHAR_P) && STRERROR_CHAR_P

/* GNU-specific strerror_r -- returns a pointer to the message */

#if !defined(HAVE_DECL_STRERROR_R) || !HAVE_DECL_STRERROR_R
char *strerror_r(int errnum, char *buf, size_t buflen);
#endif

char *cm_strerror(int errnum) {
    static tls char errstr[STRERROR_MAX];
    char *str = strerror_r(errnum, errstr, STRERROR_MAX);
    if (str != errstr) {
        fprintf(stderr, "H");
        strncpy(errstr, str, STRERROR_MAX);
        errstr[STRERROR_MAX-1] = '\0';
    }
    return errstr;
}

#else  /* defined(STRERROR_R_CHAR_P) && STRERROR_CHAR_P */

/* XSI-compliant strerror_r */

#if !defined(HAVE_DECL_STRERROR_R) || !HAVE_DECL_STRERROR_R
int strerror_r(int errnum, char *buf, size_t buflen);
#endif

char *cm_strerror(int errnum) {
    static tls char errstr[STRERROR_MAX];
    int ret = strerror_r(errnum, errstr, STRERROR_MAX);

    /* Handle negative return value from older GLIBC versions'
     * pseudo-standard strerror_r that returns the actual error
     * in errno. */
    if (ret < 0) ret = errno;

    if (ret == EINVAL) {
        snprintf(errstr, STRERROR_MAX, "errno %d", errnum);
    }
    /* if (ret == ERANGE): Not enough space, message has been cut.
     * This shouldn't really happen with a 256 character buffer, so
     * we'll just ignore it. */

    return errstr;
}

#endif /* defined(STRERROR_R_CHAR_P) && STRERROR_CHAR_P */

#else  /* defined(HAVE_STRERROR_R)   && HAVE_STRERROR_R */

#warning "strerror_r is not available. This is most likely a configuration problem. Make sure that HAVE_STRERROR_R is properly defined in config.h"

/* Provide a fallback function that returns a string with the numeric
 * error code */

char *cm_strerror(int errnum) {
    static tls char errstr[STRERROR_MAX];
    snprintf(errstr, STRERROR_MAX, "errno %d", errnum);
    return errstr;
}

#endif /* defined(HAVE_STRERROR_R)   && HAVE_STRERROR_R */



/**********************************************************************/
/*             */
/* File access */
/*             */
/***************/

/**
 * fopen wrapper with large file support for binary files.
 *
 * This function uses binary mode file access regardless of operating
 * system or global settings such as _fmode. Option "t" in mode is also
 * ignored. Use cm_fopen_text for text mode access.
 *
 * @param path name of the file to open
 * @param mode standard fopen mode string. If NULL is passed as mode,
 *             the function defaults to read-only access.
 *
 * @return FILE pointer, or NULL on failure with errno set to indicate
 *         the error
 */
FILE *cm_fopen_bin(const char *path, const char *mode) {
    FILE *ret;
    char *newmode;
    /* Add "b" to mode */ 
    if (mode == NULL) {
        newmode = (char *) malloc(3);
        if (newmode == NULL) {
            errno = ENOMEM;
            return NULL;
        }
        newmode[0] = 'r';
        newmode[1] = 'b';
        newmode[2] = '\0';
    } else {
        size_t i;
        size_t mode_len = strlen(mode);
        int b_found = 0;
        newmode = (char *) malloc(mode_len + 2);
        if (newmode == NULL) {
            errno = ENOMEM;
            return NULL;
        }
        for (i = 0; i < mode_len; i++) {
            if ((mode[i] == 't') || (mode[i] == 'b')) {
                newmode[i] = 'b'; 
                b_found = 1;
            } else newmode[i] = mode[i];
        }
        if (!b_found) newmode[i++] = 'b';
        newmode[i] = '\0';
    }
#if HAVE_FOPEN64
    ret = fopen64(path, newmode);
#else 
    ret = fopen(path, newmode);
#endif
    free(newmode);
    return ret;
}


/**
 * fopen wrapper with large file support for text files.
 *
 * This function uses text mode file access regardless of operating
 * system or global settings such as _fmode. Option "b" in mode is also
 * ignored. Use cm_fopen_bin for binary mode access. Note that text mode
 * and binary mode are equivalent on POSIX systems.
 *
 * @param path name of the file to open
 * @param mode standard fopen mode string. If NULL is passed as mode,
 *             the function defaults to read-only access.
 *
 * @return FILE pointer, or NULL on failure with errno set to indicate
 *         the error
 */
FILE *cm_fopen_text(const char *path, const char *mode) {
    FILE *ret;
    char *newmode;

    /* Add option "t" to mode if _fmode or _get_fmode() exist in stdlib.h.
     * (this is a Windows-specific modification to force text mode
     * access when _fmode is set to _O_BINARY). "t" is not
     * standard-compliant, so it cannot be used on all systems */
#if HAVE_GET_FMODE || HAVE_FMODE
    int use_t_option = 1; 
#else
    int use_t_option = 0;
#endif

    if (mode == NULL) {
        size_t i = 0;
        newmode = (char *) malloc(3);
        if (newmode == NULL) {
            errno = ENOMEM;
            return NULL;
        }
        newmode[i++] = 'r';
        if (use_t_option) newmode[i++] = 't';
        newmode[i] = '\0';
    } else {
        size_t i;
        size_t j;
        size_t mode_len = strlen(mode);
        int t_found = 0;
        newmode = (char *) malloc(mode_len + 2);
        if (newmode == NULL) {
            errno = ENOMEM;
            return NULL;
        }
        for (i = 0, j = 0; i < mode_len; i++) {
            if (((mode[i] == 't') || (mode[i] == 'b')) && use_t_option) {
                newmode[j++] = 't'; 
                t_found = 1;
            } else newmode[j++] = mode[i];
        }
        if (use_t_option && !t_found) newmode[j++] = 't';
        newmode[j] = '\0';
    }
#if HAVE_FOPEN64
    ret = fopen64(path, newmode);
#else 
    ret = fopen(path, newmode);
#endif
    free(newmode);
    return ret;
}



/**
 * fseek wrapper with large file support.
 *
 * This function uses fseeko64 (or _fseeki64 on MSVC) when available.
 *
 * @param stream stream to seek in
 * @param offset number of bytes to seek from whence
 * @param whence SEEK_SET, SEEK_CUR or SEEK_END for seeking from
 *               the start of the file, the current position, or
 *               the end of the file, respectively
 *
 * @return 0 when successful, otherwise -1 with errno set to indicate
 *         the error
 */
int cm_fseek(FILE *stream, int64_t offset, int whence) {
#if HAVE_FSEEKO64
    return fseeko64(stream, offset, whence);
#elif HAVE_FSEEKI64
    return _fseeki64(stream, offset, whence);
#elif HAVE_FSEEKO
/* Turn off warnings about an exhaustive test that are triggered
 * when off_t is 64-bit. */
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wlogical-op"
    if ((offset > (int64_t) OFF_T_MAX) || (offset < (int64_t) OFF_T_MIN)) {
        errno = EOVERFLOW;
        return -1;
    }
# pragma GCC diagnostic pop
    return fseeko(stream, (off_t) offset, whence);
#else
    if ((offset > (int64_t) LONG_MAX) || (offset < (int64_t) LONG_MIN)) {
        errno = EOVERFLOW;
        return -1;
    }
    return fseek(stream, (long) offset, whence);
#endif
}


/**
 * ftell wrapper with large file support.
 *
 * This function uses ftello64 (or _ftelli64 on MSVC) when available.
 *
 * @param stream stream whose position is requested
 *
 * @return file position, or -1 with errno set to indicate the error
 *         on failure
 */
int64_t cm_ftell(FILE *stream) {
#if HAVE_FTELLO64
    return ftello64(stream);
#elif HAVE_FTELLI64
    return _ftelli64(stream);
#elif HAVE_FTELLO
    return ftello(stream);
#else
    return ftell(stream);
#endif
}


#define GETDELIM_LINE_SIZE 128

/**
 * Delimited string input.
 *
 * This function implements POSIX.1-2008 getdelim() or calls the LIBC
 * implementation if available.
 *
 * Reads characters from a stream to a buffer until a delimiting character
 * is met. If address to an already allocated buffer is given in
 * *lineptr, the string is stored there. More space is allocated with
 * realloc() whenever the buffer gets full.. If *lineptr is NULL,
 * a new buffer is allocated. Buffer address is written to *lineptr and
 * buffer size is written to *n, unless there is an error other than
 * EOF. Note that the buffer is usually larger than the string, whose
 * length is returned directly by the function (except in the EOF case).
 *
 * Caller is responsible for freeing *lineptr. The interface
 * allows easy reuse of the same buffer when reading multiple delimited
 * strings.
 *
 * @param lineptr pointer reference for reading and writing a buffer
 *                address
 * @param n       reference to a size_t variable for storing the size of
 *                the lineptr memory area 
 * @param delim   delimiting character 
 * @param stream  stream to read
 *
 * @return when successful, the function returns number of characters
 *         read, including the delimiting character but not including
 *         the NUL character that ends the string. Returns -1 in case
 *         of an error, with errno set to indicate the cause. Reaching
 *         EOF before a delimiting character will also cause the function
 *         to return -1.
 */
ssize_t cm_getdelim(char **lineptr, size_t *n, int delim, FILE *stream) {
#if HAVE_GETDELIM
    return getdelim(lineptr, n, delim, stream);
#else
    ssize_t i = 0;
    char *newbuf;
    int c;
    if ((lineptr == NULL) || (n == NULL)) {
        errno = EINVAL;
        return -1;
    }
    if (*lineptr == NULL) {
        *lineptr = (char *) malloc(GETDELIM_LINE_SIZE);
        if (*lineptr == NULL) {
            errno = ENOMEM;
            return -1;
        }
        *n = GETDELIM_LINE_SIZE;
    } else if (*n < 1) {
        /* Make sure there is space for at least one character to
         * terminate the string. */
        newbuf = (char *) realloc(lineptr, GETDELIM_LINE_SIZE);
        if (newbuf == NULL) {
            errno = ENOMEM;
            return -1;
        }
        *n = GETDELIM_LINE_SIZE;
        *lineptr = newbuf;
    }

    while ((c = getc(stream)) != EOF) {
        (*lineptr)[i++] = (char) c;
        if (i >= *n) {
            size_t newsize = *n * 2;
            if (newsize < GETDELIM_LINE_SIZE) newsize = GETDELIM_LINE_SIZE;
            newbuf = (char *) realloc(lineptr, newsize);
            if (newbuf == NULL) {
                (*lineptr)[i-1] = '\0';
                errno = ENOMEM;
                return -1;
            }
            *n = newsize;
            *lineptr = newbuf;
        }
        if (c == delim) break;
    }
    (*lineptr)[i] = '\0';
    if (c == EOF) return -1;
    else return (i-1);
#endif
}


/**********************************************************************/
/*                              */
/* Command line option handling */
/*                              */
/********************************/


/**
 * Returns a pointer to the option struct that matches given id or short
 * option.
 *
 * @param options pointer to an array of option structs
 * @param optid short option character or option id
 *
 * @return pointer to matching option struct or NULL if no match is found
 */
const cm_option *cm_get_short_option(const cm_option *options, int optid) {
    int i = 0;
    if (options == NULL) return NULL;
    do {
        if (options[i].id == optid) return &options[i];
        ++i;
    } while (options[i].id >= 0);
    if (optid < 0) return &options[i];
    else return NULL;
}


/**
 * Returns a pointer to the option struct that matches given long option
 * string.
 *
 * @param options   pointer to an array of option structs
 * @param optstring long option string
 *
 * @return pointer to matching option struct or NULL if no match is found
 */
const cm_option *cm_get_long_option(const cm_option *options,
                                      const char *optstring) {
    int i = 0;
    do {
        if (((optstring == NULL) && (options[i].id < 0)) ||
                (!strcmp(optstring, options[i].long_option)))
            return &options[i];
        ++i;
    } while (options[i].id >= 0);
    if (optstring == NULL) return &options[i];
    else return NULL;
}


/**
 * Parses an option and its argument from the argv array.
 *
 * @param opt               option struct
 * @param argument          option argument string
 * @param target_parameters parameter structure passed through to the
 *                          argument parser callback function
 *
 * @return number of parsed arguments for the option. 0 if the option itself
 *         is an argument, -1 if there is an error (a required argument is
 *         missing, opt is NULL or there is no argument parsing function).
 */
int cm_parse_option(const cm_option *opt, const char *argument,
                 void *target_parameters) {
    const char *arg;

    if (opt == NULL) {
        return -1;
    }
    if (opt->arg_presence == CM_ARG_NONE) {
        arg = NULL;
    } else if (opt->arg_presence == CM_ARG_OPTIONAL) {
        if ((argument != NULL) && (argument[0] != '\0')) {
            if (argument[0] == '-') arg = NULL;
            else arg = argument;
        } else arg = NULL;
    } else {
        if ((argument == NULL) || (argument[0] == '\0')) {
            cm_log_error("Required argument missing for option "
                         "-%c, --%s", opt->id, opt->long_option);
            return -1;
        } else arg = argument;
    }
    if (opt->parser == NULL) {
        if (isalnum(opt->id) && (opt->long_option != NULL))
            cm_log_error("Missing argument parsing function for option "
                         "-%c, --%s", opt->id, opt->long_option);
        else if (opt->long_option != NULL)
            cm_log_error("Missing argument parsing function for option "
                         "--%s", opt->long_option);
        else cm_log_error("Missing parsing function for non-option arguments");
        return -1;
    } else {
        if ((opt->parser)(opt->id, arg, target_parameters) < 0) return -1;
    }

    if (opt->id < 0) return 0;
    else if (arg != NULL) return 1;
    else return 0;
}


/**
 * Parses described options and other arguments from the command line.
 *
 * @param argc              number of command line arguments
 * @param argv              array of pointers to argument strings
 * @param options           pointer to an array of option descriptions
 * @param target_parameters pointer to a parameter structure that will be
 *                          passed to the callback parser function.
 * @return 1 if successful, 0 otherwise
 */
int cm_parse_argv(int argc, const char * const argv[],
                   const cm_option *options, void *target_parameters) {
    int i;
    int pos = 0;
    for (i=1; i<argc; ++i) {
        int ret = 0;
        int hyphens = 0;
        const char *arg = argv[i];
        const char *nextarg;
        const cm_option *opt;

        if (i < argc-1) nextarg = argv[i+1];
        else nextarg = NULL;

        if (arg[0] == '\0') continue;
        if (arg[0] == '-') {
            if (arg[1] == '\0') {
                /* Plain "-" is passed through as a separate argument */
                opt = cm_get_short_option(options, -1);
                nextarg = arg;
            } else if (arg[1] == '-') {
                if (arg[2] == '\0') {
                    /* Short option "--" is also supported */
                    opt = cm_get_short_option(options, '-');
                    nextarg = arg;
                } else {
                    /* Long option */
                    hyphens = 2;
                    opt = cm_get_long_option(options, &arg[2]);
                }
            } else {
                int j = 1;
                hyphens = 1;
                /* Parse multiple concatenated short options */
                while (arg[j+1] != '\0') {
                    opt = cm_get_short_option(options, arg[j]);
                    if (opt && opt->arg_presence == CM_ARG_REQUIRED) {
                        cm_log_error(
                                "Required argument missing for option -%c: "
                                "argument \"%s\" is reserved for option -%c:",
                                arg[j],
                                (i < argc - 1) ? argv[i+1] : "",
                                arg[strlen(arg)-1]);
                        cm_show_error_position(stderr, pos+j, argc, argv);
                        return 0;
                    }
                    ret = cm_parse_option(opt, NULL, target_parameters);
                    if (ret < 0) {
                        if (opt == NULL) {
                            cm_log_error("Unknown option \"-%c\":", arg[j]);
                            cm_show_error_position(cm_get_log_target(),
                                                    pos+j, argc, argv);
                            return 0;
                        }
                        break;
                    }
                    ++j;
                }
                /* Only the last option can have an argument */
                opt = cm_get_short_option(options, arg[j]);
            }
        } else {
            /* This is a non-option argument */
            opt = cm_get_short_option(options, -1);
            nextarg = arg;
        }
        if (ret >= 0) {
            ret = cm_parse_option(opt, nextarg, target_parameters);
        }
        if (ret < 0) {
            if (opt == NULL) {
                cm_log_error("Unknown option \"%s\":", argv[i]);
            }
            cm_show_error_position(cm_get_log_target(), pos + hyphens,
                                    argc, argv);
            return 0;
        } else {
            pos += (int) strlen(argv[i]);
            if (ret) {
                pos += (int) strlen(argv[i+1]);
            }
            i += ret;
        }
    }
    return 1;
}


/**
 * Prints command line arguments until the given position is reached and
 * marks it with a '^' on the next line.
 *
 * @param out   output file or stream, for example stderr
 * @param pos   position of an incorrect argument
 * @param argc  number of arguments
 * @param argv  array of pointers to argument strings
 */
void cm_show_error_position(FILE *out, int pos, int argc,
                             const char * const argv[]) {
    int i;
    int linelen = 4;
    int arg0_len = (int) strlen(argv[0]);
    int argp = 0;
    if ((out == NULL) || (argc < 1)) return;
    fputs("    ", out);
    pos += arg0_len;
    for (i = 0; i < argc; ++i) {
        int j;
        int len = (int) strlen(argv[i]);
        for (j = 0; j < len; j++, linelen++, pos--) {
            fputc(argv[i][j], out);
            if (pos == 0) argp = linelen;
        }
        if (linelen >= 60) {
            if (pos < 0) {
                fputs("...", out);
                goto print_mark;
            }
            fputs(" \\\n    ", out);
            for (linelen = 0; linelen < arg0_len; linelen++)
                fputc(' ', out);
            linelen += 4;
        }
        if (i < argc - 1) {
            fputc(' ', out);
            if (pos > 0) linelen++;
        }
    }
print_mark:
    fputc('\n', out);
    if (argp > 0) {
        for (pos = 0; pos < argp; pos++) fputc(' ', out);
    }
    fputc('^', out);
    fputc('\n', out);
}


/**
 * Prints option descriptions neatly.
 *
 * @param out         output file or stream, for example stderr
 * @param options     pointer to an array of option structs
 * @param col1_width  width of the first column (option names), for example 32
 * @param col2_width  width of the second column (descriptions), for example 48
 */
void cm_print_options(FILE *out, const cm_option *options,
                       int col1_width, int col2_width) {
    int i = 0;
    const char *arg_brackets[] = {"  ", "[]", "<>"};
    int indent = 2;

    while (options[i].id >= 0) {
        int pos = 0;
        int cut = 0;
        int lastcut = 0;
        int col2w = col2_width;
        int arg_presence = options[i].arg_presence;
        const char *description = options[i].description;
        int dlen = (int) strlen(description);

        /* Print short option */
        if (isalnum(options[i].id)) {
            pos += fprintf(out, "  -%c", options[i].id);
        } else {
        }

        /* Print long option */
        if (options[i].long_option != NULL) {
            if (pos == 0) pos += fputs("     ", out);
            else pos += fputs(", ", out);
            pos += fprintf(out, "--%s", options[i].long_option);
        }
        if ((options[i].arg_type != NULL) && arg_presence) {
            pos += fprintf(out, " %c%s%c", arg_brackets[arg_presence][0],
                    options[i].arg_type, arg_brackets[arg_presence][1]);
        }
        if (pos >= col1_width) {
            fputc('\n', out);
            pos = -1;
        }
        for (; pos < col1_width-1; pos++) fputc(' ', out);

        /* Print description */
        while (lastcut < dlen) {
            int hardbreak = lastcut + col2w;
            cut = lastcut;
            if (hardbreak < dlen) {
                /* Try to cut at a space to fit the description to its column */
                for (pos = hardbreak; pos > cut; pos--) {
                    if (description[pos] == ' ') cut = pos;
                }
                if (cut == lastcut) cut = hardbreak;
            } else {
                cut = dlen;
            }

            for (pos = lastcut; pos < cut; pos++) {
                if (description[pos] == '\n') {
                    cut = pos;
                    break;
                }
                fputc(description[pos], out);
            }
            fputc('\n', out);

            lastcut = cut + 1;
            if (lastcut < dlen) {
                for (pos = 0; pos < col1_width + indent; pos++) fputc(' ', out);
            }
            col2w = col2_width - indent;
        }
        ++i;
    }
}


/**********************************************************************/
/*                           */
/* Aligned memory allocation */
/*                           */
/*****************************/


/**
 * Returns the closest power of two that is larger than (or equal to)
 * the given value or CM_MIN_ALIGNMENT.
 *
 * @param alignment an unsigned integer value
 *
 * @return closest larger or equal power of two
 */
static size_t cm_get_power_of_two_alignment(size_t alignment) {
    if (alignment < CM_MIN_ALIGNMENT) alignment = CM_MIN_ALIGNMENT;
    else if ((alignment & (alignment - 1)) != 0) {
        size_t a = CM_MIN_ALIGNMENT;
        while (a < alignment) a = a << 1;
        alignment = a;
    }
    return alignment;
}


/**
 * Aligned malloc. Allocated area must be freed with cm_free.
 *
 * @param alignment Returned area will be located at an address multiple of
 *        this value in bytes. Must be a power of two.
 * @param size amount of memory to allocate
 *
 * @return pointer to the allocated area, or NULL if allocation fails.
 */
void *cm_malloc_aligned(size_t size, size_t alignment) {
    void *ptr = NULL;

    /* Make sure that alignment is a power of 2 */
    alignment = cm_get_power_of_two_alignment(alignment);

#if defined(HAVE_POSIX_MEMALIGN)
    {
        int ret;
        ret = posix_memalign(&ptr, alignment, size);
        if (ret == EINVAL) {
            cm_log_error("Invalid alignment. This should not happen; "
                         "apparently there is a problem in function "
                         "cm_get_power_of_two_alignment.");
        }
    }
#elif defined(HAVE_MALLOC_H)
    ptr = memalign(alignment, size);
#else
    {
        void *p = malloc(size + alignment + sizeof(void **));
        if (p) {
            ptr = p + (alignment - 1) + sizeof(void **);
            ptr -= (intptr_t) ptr & (alignment - 1);
            *((void **) (ptr - sizeof(void **))) = p;
        }
    }
#endif
    if (!ptr) cm_log_error("Memory allocation of %zd bytes failed", size);
    return ptr;
}

/**
 * Aligned realloc.
 *
 * NOTE: Unlike the library realloc, this function frees the original memory
 *       area if allocation fails. Use cm_realloc_safe if you need to keep
 *       the data block accessible (but possibly misaligned) in case of
 *       failure.
 *
 * @param ptr pointer to previously allocated memory. If NULL, this function
 *        works like cm_alloc_aligned.
 * @param alignment
 * @param size new size for the area
 *
 * @return pointer to a reallocated area, or NULL if allocation fails.
 */
void *cm_realloc_aligned(void *ptr, size_t size, size_t alignment) {
    if (cm_realloc_safe(&ptr, size, alignment)) {
        cm_free(ptr);
        return NULL;
    } else return ptr;
}


/**
 * Aligned realloc with a posix_memalign-style interface. Calls the
 * C library realloc and moves the memory area back to an aligned position
 * if necessary.
 *
 * If realigning the memory block fails, it will remain unaligned. In this
 * case the function returns 1. If realloc fails, function will return
 * ENOMEM.
 *
 * @param pptr pointer to a pointer to previously allocated memory.
 *             If *ptr is NULL, this function works like cm_alloc_aligned.
 * @param alignment
 * @param size new size for the area
 *
 * @return 0 if successful, 1 if the area was reallocated but remains
 *         unaligned, or ENOMEM if reallocation failed completely.
 */
int cm_realloc_safe(void **pptr, size_t size, size_t alignment) {
    void *ptr = *pptr;
    void *newptr;

    if (ptr == NULL) {
        *pptr = cm_malloc_aligned(size, alignment);
        if (*pptr == NULL) return ENOMEM;
        else return 0;
    }

    /* Make sure that alignment is a power of 2 */
    alignment = cm_get_power_of_two_alignment(alignment);

#if defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MALLOC_H)

    newptr = realloc(ptr, size);
    if (newptr == NULL) {
        /* Free the area on failure, to keep things consistent with the
           other case below. */
        return ENOMEM;
    } else if ((uintptr_t) newptr & (alignment - 1)) {
        /* realloc moved the data to an unaligned position. Realign. */
  #if defined(HAVE_POSIX_MEMALIGN)
        {
            int ret;
            ret = posix_memalign(&ptr, alignment, size);
            if (ret == EINVAL) {
                cm_log_error("Invalid alignment. This should not happen. "
                        "Problem is in function cm_get_power_of_two_alignment.");
            }
        }
  #else
        ptr = memalign(alignment, size);
  #endif
        if (ptr == NULL) {
            /* Failed to allocate new area. Return pointer to the unaligned
               data. */
            *pptr = newptr;
            return 1;
        }
        memcpy(ptr, newptr, size);
        free(newptr);
        *pptr = ptr;
    } else {
        *pptr = newptr;
    }
    return 0;

#else

    {
        void *p = *(((void **) ptr) - 1);
        size_t pdiff = ptr - p;
        void *newptr = realloc(p, size + CM_MAX(pdiff, alignment +
                                                 sizeof(void **)));
        if (newptr == NULL) {
            return ENOMEM;
        } else if (((intptr_t) newptr + pdiff) & (alignment - 1)) {
            /* Realign the data block */
            p = newptr + (alignment - 1) + sizeof(void **);
            p -= (intptr_t) p & (alignment - 1);
            memmove(p, newptr + pdiff, size);
            *((void **) (p - sizeof(void **))) = newptr;
            *pptr = p;
            return 0;
        }
    }
#endif
}


/**
 * Simple dynamic array update routine. Should be called before every
 * addition of an element to the array.
 *
 * Allocated array must be freed with cm_free.
 *
 * @param pptr         pointer to a pointer to previously allocated memory
 *                     If *pptr is NULL, a new memory area is allocated
 *                     according to given *cap_elements and num_elements
 *                     values.
 * @param cap_elements pointer to a capacity value: total number of
 *                     elements of size element_size that fit within
 *                     the memory area pointed by *pptr
 * @param num_elements number of elements currently stored to the array
 * @param element_size size of stored elements
 * @param alignment    memory alignment in bytes
 *
 * @return 0 if successful, 1 if the array was reallocated but remains
 *         unaligned, or ENOMEM if reallocation failed completely.
 */
int cm_dynamic_array_update_aligned(void **pptr, size_t *cap_elements,
        size_t num_elements, size_t element_size, size_t alignment) {
    int update = 0;
    if (pptr == NULL) return -1;
    if (*cap_elements == 0) {
        *cap_elements = 1;
        update = 1;
    }
    if (element_size == 0) element_size = 1;

    if (num_elements >= *cap_elements) {
        *cap_elements = num_elements << 1;
        update = 1;
    } else if (*pptr == NULL) {
        update = 1;
    }

    if (update) {
        return cm_realloc_safe(pptr, *cap_elements * element_size, alignment);
    } else {
        return 0;
    }
} 


/**
 * Frees memory allocated by cm_malloc.
 *
 * @param ptr pointer to a memory area allocated by cm_malloc
 */
void cm_free(void *ptr) {
    if (ptr) {
#if defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MALLOC_H)
        free(ptr);
#else
        free(*(((void **) ptr) - 1));
#endif
    }
}


#define NANOSECOND 1000000000L

/**
 * Returns a monotonic timestamp in nanoseconds.
 *
 * @return a monotonic timestamp in nanoseconds.
 */
int64_t cm_timestamp_nano(void) {
    struct timespec tp;
    int err = -1;
#ifdef CLOCK_MONOTONIC_RAW
    err = clock_gettime(CLOCK_MONOTONIC_RAW, &tp);
#endif 
    if (err < 0) clock_gettime(CLOCK_MONOTONIC, &tp);
    return ((int64_t) tp.tv_sec) * NANOSECOND + ((int64_t) tp.tv_nsec);
}

