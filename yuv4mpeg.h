/**
 * @file      yuv4mpeg.h
 * @brief     YUV4MPEG demuxer/muxer API
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright 2-clause BSD
 *
 * @section LICENSE
 *
 * Copyright (c) 2013, Niko Mikkilä
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer
 *    in the documentation and/or other materials provided with
 *    the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef YUV4MPEG_H
#define YUV4MPEG_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "config.h"

#if HAVE_Y4M_FVFRAME
#include "fvframe.h"
#endif /*HAVE_Y4M_FVFRAME */

#ifdef __cplusplus
extern "C" {
#endif

typedef struct y4m_fraction {
    int num;
    int den;
} y4m_fraction;

enum y4m_sampling {
    Y4M_SAMPLING_420_JPEG    = 0,
    Y4M_SAMPLING_420_MPEG2   = 1,
    Y4M_SAMPLING_420_PALDV   = 2,
    Y4M_SAMPLING_411         = 3,
    Y4M_SAMPLING_422         = 4,
    Y4M_SAMPLING_444         = 5,
    Y4M_SAMPLING_444_ALPHA   = 6,
    Y4M_SAMPLING_LUMA        = 7,
    Y4M_SAMPLING_LAST
};

enum y4m_interlacing {
    Y4M_INTERLACING_UNKNOWN  = 0,
    Y4M_INTERLACING_NONE     = 1,
    Y4M_INTERLACING_TFF      = 2,
    Y4M_INTERLACING_BFF      = 3,
    Y4M_INTERLACING_MIXED    = 4,
    Y4M_INTERLACING_LAST
};

enum y4m_frame_presentation {
    Y4M_FRAME_PRESENTATION_1P         = 0,
    Y4M_FRAME_PRESENTATION_2P         = 1,
    Y4M_FRAME_PRESENTATION_3P         = 2,
    Y4M_FRAME_PRESENTATION_TFF        = 3,
    Y4M_FRAME_PRESENTATION_TFF_REPEAT = 4,
    Y4M_FRAME_PRESENTATION_BFF        = 5,
    Y4M_FRAME_PRESENTATION_BFF_REPEAT = 6,
    Y4M_FRAME_PRESENTATION_LAST
};

enum y4m_frame_temporal_sampling {
    Y4M_FRAME_TEMPORAL_SAMPLING_PROGRESSIVE = 0,
    Y4M_FRAME_TEMPORAL_SAMPLING_INTERLACED  = 1,
    Y4M_FRAME_TEMPORAL_SAMPLING_LAST
};

enum y4m_frame_chroma_sampling {
    Y4M_FRAME_CHROMA_SAMPLING_PROGRESSIVE = 0,
    Y4M_FRAME_CHROMA_SAMPLING_INTERLACED  = 1,
    Y4M_FRAME_CHROMA_SAMPLING_UNKNOWN     = 2,
    Y4M_FRAME_CHROMA_SAMPLING_LAST
};

typedef struct y4m_index_rec {
    int64_t header_pos;
    int64_t frame_pos;
} y4m_index_rec;

typedef struct y4m {
    FILE                *stream;
    uint32_t             width;
    uint32_t             height;
    enum y4m_sampling    chroma_sampling;
    enum y4m_interlacing interlacing;
    y4m_fraction         rate;
    y4m_fraction         aspect;
    char                *metadata;

    y4m_index_rec       *index;
    size_t               index_capacity;
    size_t               index_frames;
    int64_t              index_last_frame_pos;

    size_t               frame_size;
    size_t               plane_size[4];
    size_t               plane_offset[4];
    uint32_t             plane_width[4];
    uint32_t             plane_height[4];

#if HAVE_Y4M_FVFRAME
    FvFrame              tmpframe;
#endif /*HAVE_Y4M_FVFRAME */
} y4m;


typedef struct y4m_frame_info {
    char                            *metadata;
    enum y4m_frame_presentation      presentation;
    enum y4m_frame_temporal_sampling temporal_sampling;
    enum y4m_frame_chroma_sampling   chroma_sampling;
} y4m_frame_info;


int y4m_from_stream(y4m *v, FILE *stream, int build_index);

void y4m_free(y4m *v);

void y4m_print_info(y4m *v, FILE *out);

int y4m_get_indexed_frame(y4m *v, uint8_t *buf, size_t bufsize, size_t n);

int y4m_get_frame(y4m *v, uint8_t *buf, size_t bufsize, int add_to_index);

int y4m_get_fvframe(y4m *v, FvFrame *out, int add_to_index);


int y4m_create(y4m *v, FILE *out, uint32_t width, uint32_t height,
               enum y4m_sampling chroma_sampling,
               enum y4m_interlacing interlacing,
               y4m_fraction rate, y4m_fraction aspect, char *metadata);
 
int y4m_put_frame(y4m *v, uint8_t *buf, size_t bufsize, int add_to_index);

#if HAVE_Y4M_FVFRAME
int y4m_put_fvframe(y4m *v, FvFrame *frame, int add_to_index);
#endif /*HAVE_Y4M_FVFRAME */


#ifdef __cplusplus
}
#endif

#endif /* YUV4MPEG_H */

