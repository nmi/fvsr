/**
 * @file      multiframe.c
 * @brief     Stacked frame container functions for super-resolution
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */


#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "cement.h"
#include "fvbuf.h"
#include "median.h"
#include "multiframe.h"
#include "optflow.h"

#define MULTIFRAME_COPY_BLOCKS 0

int Multiframe_init(Multiframe *mf, uint32_t stacksize,
        uint32_t luma_scale, size_t luma_width, size_t luma_height,
        uint32_t chroma_scale, size_t chroma_width, size_t chroma_height) {
    if (stacksize > 65534) {
        cm_log_warning("Pixel stack size %u exceeds maximum (65535)", stacksize);
        stacksize = 65534;
    }
    /* Reserve space for a 16-bit counter for each superpixel */
    stacksize += 2;

    /* Calculate a modulo 32 frame stack size */
    //mf->stacksize = range + 31 - ((range-1) & 31);

    mf->plane[0].width    = luma_width  * luma_scale;
    mf->plane[0].height   = luma_height * luma_scale;
    mf->plane[0].datasize = (size_t) (stacksize * mf->plane[0].width *
                                                  mf->plane[0].height);

    mf->plane[1].width    = chroma_width  * chroma_scale;
    mf->plane[1].height   = chroma_height * chroma_scale;
    mf->plane[1].datasize = (size_t) (stacksize * mf->plane[1].width *
                                                  mf->plane[1].height);
 
    mf->plane[2].width    = chroma_width  * chroma_scale;
    mf->plane[2].height   = chroma_height * chroma_scale;
    mf->plane[2].datasize = (size_t) (stacksize * mf->plane[2].width *
                                                  mf->plane[2].height);
 
    mf->frame_size = (size_t) (mf->plane[0].width * mf->plane[0].height +
                               mf->plane[1].width * mf->plane[1].height +
                               mf->plane[2].width * mf->plane[2].height);

    mf->unscaled_frame_size = (size_t) (luma_width   * luma_height + 2 *
                                        chroma_width * chroma_height);

    mf->datasize = mf->plane[0].datasize + mf->plane[1].datasize +
                   mf->plane[2].datasize;

    mf->data = (uint8_t *) cm_malloc(mf->datasize);
 
    if (mf->data == NULL) {
        cm_log_error("Failed to allocate memory for a multiframe of "
                     "%u x (%ux%u x %zux%zu + 2 x %ux%u x %zux%zu) bytes "
                     "(%zu MiB)",
                     stacksize, luma_scale, luma_scale,
                     luma_width, luma_height,
                     chroma_scale, chroma_scale,
                     chroma_width, chroma_height,
                     mf->datasize / (size_t) (1024 * 1024));
        memset(mf, 0, sizeof(Multiframe));
        return -1;
    }
    memset(mf->data, 0, mf->datasize);

    mf->plane[0].data            = mf->data;
    mf->plane[0].unscaled_width  = luma_width;
    mf->plane[0].unscaled_height = luma_height;
    mf->plane[0].scale           = luma_scale;
    mf->plane[0].stacksize       = stacksize;
    mf->plane[0].pitch           = luma_width * luma_scale * stacksize;
    mf->plane[0].x_factor        = 1.0F;
    mf->plane[0].y_factor        = 1.0F;

    mf->plane[1].data            = mf->data + mf->plane[0].datasize;
    mf->plane[1].unscaled_width  = chroma_width;
    mf->plane[1].unscaled_height = chroma_height;
    mf->plane[1].scale           = chroma_scale;
    mf->plane[1].stacksize       = stacksize;
    mf->plane[1].pitch           = chroma_width * chroma_scale *
                                   stacksize;
    mf->plane[1].x_factor        = (float) chroma_width /
                                   (float) luma_width;
    mf->plane[1].y_factor        = (float) chroma_height /
                                   (float) luma_height;

    mf->plane[2].data            = mf->plane[1].data +
                                   mf->plane[1].datasize;
    mf->plane[2].unscaled_width  = chroma_width;
    mf->plane[2].unscaled_height = chroma_height;
    mf->plane[2].scale           = chroma_scale;
    mf->plane[2].stacksize       = stacksize;
    mf->plane[2].pitch           = chroma_width * chroma_scale *
                                   stacksize;
    mf->plane[2].x_factor        = (float) chroma_width /
                                   (float) luma_width;
    mf->plane[2].y_factor        = (float) chroma_height /
                                   (float) luma_height;

    return 0;
}

void Multiframe_free(Multiframe *mf) {
    free(mf->data);
    memset(mf, 0 , sizeof(Multiframe));
}


void Multiframe_clear(Multiframe *mf) {
    /*size_t nelements = mf->datasize / (size_t) mf->stacksize;
    size_t element = 0;
    size_t p = 0;
    for (; element < nelements; element++, p+=mf->stacksize) {
        mf->data[p] = 0;
    }*/
    memset(mf->data, 0, mf->datasize);
}

void Multiframe_add_origin(Multiframe *mf, FvFrame *of) {
    int pnum;
    int max_planes = cm_min(3, (int) of->num_planes);
    for (pnum = 0; pnum < max_planes; pnum++) {
        Multiplane *mplane = &mf->plane[pnum];
        uint8_t    *mrow   = mplane->data;
        FvBuf      *oplane = &of->plane[pnum];
        FvBinMain  *obin   = FvBuf_access_main(oplane, FVBUF_ACCESS_READ);
        uint8_t    *orow   = (uint8_t *) (obin->mem + oplane->offset);

        size_t i;
        size_t width  = cm_min(FvBuf_width(oplane),  mplane->width);
        size_t height = cm_min(FvBuf_height(oplane), mplane->height);
        size_t xskip  = (mplane->width  / FvBuf_width(oplane)) *
                        mplane->stacksize;
        size_t yskip  = (mplane->height / FvBuf_height(oplane));
        
        for (i = 0; i < height; i++) {
            uint32_t j;
            size_t mp = 0;
            for (j = 0; j < width; j++) {
                int n = (mrow[mp] << 8) + mrow[mp+1];
                if (n < mplane->stacksize-1) {
                    n = n + 1;
                    mrow[mp]   = (uint8_t) (n >> 8);
                    mrow[mp+1] = (uint8_t) (n & 0xFF);
                    mrow[mp+(size_t)n+1] = orow[j];
                } else {
                    cm_log_warning("Pixel stack full when adding reference frame");
                }
                mp += xskip;
            }
            /* Skip over remaining high resolution lines that match this
             * low resolution row */
            mrow += (size_t) yskip * (size_t) mplane->pitch;
            orow += FvBuf_row_pitch(oplane);
        }
    }
}

void Multiframe_add_mc(Multiframe *mf, FvFrame *of,
        FvFrame *frame, const OpticalFlow *flow,
        uint32_t mv_average_radius, uint32_t validation_radius,
        uint32_t validation_threshold) {
    int pnum;
    int max_planes = cm_min(3, (int) frame->num_planes);

    const float *flowdata = flow->data;
/*    if ((of->plane[0].width != flow->width) ||
        (of->plane[0].height != flow->height)) {
        cm_log_error("Optical flow size mismatch (frame: %ux%u, flow: %ux%u)",
                of->plane[0].width, of->plane[0].height,
                flow->width, flow->height);
        return;
    }
*/
    for (pnum = 0; pnum < max_planes; pnum++) {
        Multiplane *mplane = &mf->plane[pnum];
        FvBuf      *rplane = &frame->plane[pnum];
        FvBinMain  *rbin   = FvBuf_access_main(rplane, FVBUF_ACCESS_READ);
        uint8_t    *rmem;
        uint32_t i;
        float xmult, ymult;
        float vx = 0.0F, vy = 0.0F;
        if (rbin == NULL) continue;
        rmem = (uint8_t *) rbin->mem + rplane->offset;
        if (mv_average_radius >= flow->height) {
            size_t p, n;
            size_t wi, wj;
            float wx_sum = 0.0F, wy_sum = 0.0F;
            p = (size_t) (2 * (flow->height/4) * flow->width);
            n = 0;
            for (wi = (size_t) flow->height/4;
                    wi < (size_t) flow->height*3/4; wi++) {
                for (wj = (size_t) flow->width/4;
                        wj < (size_t) flow->width*3/4; wj++) {
                    wx_sum += flowdata[p + wj * 2];
                    wy_sum += flowdata[p + wj * 2 + 1];
                    n++;
                }
                p += (size_t) flow->width * 2;
            }
            vx = wx_sum/((float) n);
            vy = wy_sum/((float) n);
        }
        xmult = (float) FvBuf_width( rplane) / (float) flow->width;
        ymult = (float) FvBuf_height(rplane) / (float) flow->height;
        for (i = 0; i < flow->height; i++) {
            uint32_t j;
            size_t flowpos = 2 * ((size_t) i * (size_t) flow->width);
            size_t rrow = (size_t) (ymult * (float) i) *
                          FvBuf_row_pitch(rplane);
            for (j = 0; j < flow->width; j++) {
                int x, y, n;
                size_t p;
                if (OpticalFlow_validate_vector(flow, of, frame, j, i,
                        (uint32_t) 1, validation_radius,
                        validation_threshold) < 0) {
                    continue;
                }

                if (mv_average_radius <= 0) {
                    vx = flowdata[flowpos + (size_t) (2*j)];
                    vy = flowdata[flowpos + (size_t) (2*j+1)];
                } else if (mv_average_radius < flow->height) {
                    uint32_t wi, wj;
                    float wx_sum = 0.0F, wy_sum = 0.0F;
                    uint32_t startrow = (uint32_t) cm_max(0, (int) i - (int) mv_average_radius);
                    uint32_t endrow = cm_min(i + mv_average_radius, flow->height);
                    uint32_t startcol = (uint32_t) cm_max(0, (int) j - (int) mv_average_radius);
                    uint32_t endcol = cm_min(j + mv_average_radius, flow->width);
                    p = 2 * startrow * flow->width;
                    n = 0;
                    for (wi = startrow; wi < endrow; wi++) {
                        for (wj = startcol; wj < endcol; wj++) {
                            wx_sum += flowdata[p + wj * 2];
                            wy_sum += flowdata[p + wj * 2 + 1];
                            n++;
                        }
                        p += flow->width * 2;
                    }
                    vx = wx_sum/((float) n);
                    vy = wy_sum/((float) n);
                }

                x = (int) lrintf((((float) j) + vx) * mplane->x_factor *
                                 (float) mplane->scale);
                y = (int) lrintf((((float) i) + vy) * mplane->y_factor *
                                 (float) mplane->scale);
                if ((x < 0) || (x >= mplane->width) ||
                        (y < 0) || (y >= mplane->height)) continue;
                p = (size_t) y * mplane->pitch +
                    (size_t) x * (size_t) mplane->stacksize;

                n = (mplane->data[p] << 8) + mplane->data[p+1];
                if (n < mplane->stacksize - 1) {
                    size_t rx = (size_t) (xmult * (float) j);
                    n++;
                    mplane->data[p]   = (uint8_t) (n >> 8);
                    mplane->data[p+1] = (uint8_t) (n & 0xFF);
                    mplane->data[p+(size_t)n+1] = rmem[rrow + rx];
                } else {
                    cm_log_warning("Pixel stack full at (x:%d, y:%d)", x, y);
                }

#if MULTIFRAME_COPY_BLOCKS
                if (pnum >= 0) {
                    int k;
                    //int block_x_radius = lrintf(cm_max(0.0F, xmult - 1.0F));
                    //int block_y_radius = lrintf(cm_max(0.0F, ymult - 1.0F));
                    int xskip = (int) (mplane->width / rplane->width);
                    int yskip = (int) (mplane->height / rplane->height);
                    int block_x_radius = 1;
                    int block_y_radius = 1;
                    int rr = (int) rrow -
                             block_y_radius * (int) FvBuf_row_pitch(rplane);
                    int rx = (int) (xmult * (float) j);
                    int ry = (int) (ymult * (float) i);
                    int mr = (int) p - block_y_radius * yskip * (int) mplane->pitch;
                    for (k = -block_y_radius; k <= block_y_radius; k++) {
                        int l;
                        if ((y + k * yskip >= mplane->height) ||
                                (ry + k >= rplane->height)) break;
                        if ((y + k * yskip >= 0) && (rr >= 0)) {
                            for (l = -block_x_radius; l <= block_x_radius; l++) {
                                int mpos;
                                if ((x + l * xskip < 0) || (x + l * xskip >= mplane->width))
                                    continue;
                                if ((rx + l < 0) || (rx + l >= rplane->width))
                                    continue;
                                mpos = mr + l * xskip * (int) mplane->stacksize;
                                n = (mplane->data[mpos] << 8) + mplane->data[mpos+1];
                                if (n < mplane->stacksize - 1) {
                                    n = n + 1;
                                    mplane->data[mpos]   = (uint8_t) (n >> 8);
                                    mplane->data[mpos+1] = (uint8_t) (n & 0xFF);
                                    mplane->data[mpos+n+1] =
                                        rplane->buf.samples.u8[rr+rx+l];
                                }
                            }
                        }
                        rr += (int) FvBuf_row_pitch(rplane);
                        mr += (int) mplane->pitch * yskip;
                    }
                }
#endif /* MULTIFRAME_COPY_BLOCKS */
            }
        }
    }
} 

int Multiframe_norm(FvFrame *norm, Multiframe *mf) {
    int p;
    int max_planes = cm_min(3, (int) norm->num_planes);

    FvFrame_zero(NULL, norm);
    for (p = 0; p < max_planes; p++) {
        FvBuf     *norm_plane = &norm->plane[p];
        FvBinMain *norm_bin;
        uint8_t   *norm_row;
        uint8_t   *mf_row;

        size_t i;
        size_t width  = cm_min(FvBuf_width(norm_plane),  mf->plane[p].width);
        size_t height = cm_min(FvBuf_height(norm_plane), mf->plane[p].height);
        norm_bin = FvBuf_access_main(norm_plane, FVBUF_ACCESS_READ);
        if (norm_bin == NULL) continue;
        norm_row = norm_bin->mem + norm_plane->offset;
        mf_row = mf->plane[p].data;
        for (i = 0; i < height; i++) {
            size_t j;
            size_t mfpos = 0;

            for (j = 0; j < width; j++) {
                ((float *) norm_row)[j] = sqrtf((float)
                        (mf_row[mfpos] << 8) + mf_row[mfpos+1]);
                mfpos += (size_t) mf->plane[p].stacksize;
            }
            norm_row += FvBuf_row_pitch(norm_plane);
            mf_row += (size_t) mf->plane[p].pitch;
        }
    }
    return 0;
}


/*
static uint8_t average(uint8_t n, uint8_t *values) {
    uint32_t sum = 0;
    uint32_t i;
    for (i = 0; i < n; i++) sum += values[i];
    return (uint8_t) lrintf((float) sum / (float) n);
}
*/

int Multiframe_median(FvFrame *out, Multiframe *mf) {
    void  *mheap = NULL;
    size_t i;
    int p;
    int max_planes = cm_min(3, (int) out->num_planes);
    if (out->plane[0].sample_type != FVSAMPLE_U8) {
        cm_log_error("Unsupported output frame sample type '%s', "
                "expected '%s'",
                fvsample_name[out->plane[0].sample_type],
                fvsample_name[FVSAMPLE_U8]);
        return -1;
    }
    if (median_heap_uint8_t_init((size_t) (mf->plane[0].stacksize-1),
                                 &mheap) < 0) {
        return -1;
    }

    for (p = 0; p < max_planes; p++) {
        FvBuf      *outplane = &out->plane[p];
        FvBinMain  *outbin;
        uint8_t    *outmem;
        Multiplane *mplane  = &mf->plane[p];
        size_t mfrow = 0;
        size_t outrow = 0;
        size_t w = cm_min(FvBuf_width(outplane),  mplane->width);
        size_t h = cm_min(FvBuf_height(outplane), mplane->height);

        outbin = FvBuf_access_main(outplane, FVBUF_ACCESS_INIT);
        if (outbin == NULL) continue;
        outmem = (uint8_t *) (outbin->mem + outplane->offset);

        for (i = 0; i < h; i++) {
            size_t j;
            for (j = 0; j < w; j++) {
                uint8_t *data = mplane->data + mfrow +
                                (size_t) j * (size_t) mplane->stacksize;
                size_t n = (size_t) (data[0] << 8) + (size_t) data[1];
                if (n > 0) {
                    uint8_t v = median_heap_uint8_t(
                            data + 2, n, &mheap);
                    //uint8_t v = average(plane->data[sp],
                    //                    plane->data + sp + 1);
                    outmem[outrow + (size_t) j] = (uint8_t) ((v == 0) ? 1 : v);
                } else {
                    //outbuf[outrow + (size_t) j] = 0;
                }
            }
            mfrow  += mplane->pitch;
            outrow += FvBuf_row_pitch(outplane);
        }
    }
    median_heap_uint8_t_free(&mheap);
    return 0;
}


void Multiframe_median_linint(FvFrame *out, Multiframe *mf) {
    uint32_t num_planes = cm_min(out->num_planes, (uint32_t) 3);
    uint32_t p;
    Multiframe_median(out, mf);
    if (out->plane[0].sample_type != FVSAMPLE_U8) {
        cm_log_error("Unsupported output frame sample type '%s', "
                "expected '%s'",
                fvsample_name[out->plane[0].sample_type],
                fvsample_name[FVSAMPLE_U8]);
        return;
    }
    for (p = 0; p < num_planes; p++) {
        FvBuf      *outplane = &out->plane[p];
        FvBinMain  *outbin;
        uint8_t    *outmem;
        size_t i;
        size_t step = mf->plane[p].scale;
        size_t w = FvBuf_width(outplane);
        size_t h = FvBuf_height(outplane);

        outbin = FvBuf_access_main(outplane, FVBUF_ACCESS_INIT);
        if (outbin == NULL) continue;
        outmem = (uint8_t *) (outbin->mem + outplane->offset);

        /* Horizontal interpolation */
        for (i = 0; i <= h; i += step) {
            size_t j;
            size_t outrow = i * FvBuf_row_pitch(outplane);
            for (j = step; j <= w; j += step) {
                uint32_t k;
                float v1, v2, d;
                uint8_t *block = outmem + outrow + j - step;
                v1 = (float) block[0];
                v2 = (float) block[step];
                d = (v2 - v1) / ((float) step);
                v1 += d;
                for (k = 1; k < step; k++) {
                    if (block[k] == 0) {
                        block[k] = (uint8_t) lrintf(v1);
                    }
                    v1 += d; 
                }
            }
        }
        /* Vertical interpolation */
        for (i = step; i <= h; i += step) {
            size_t j;
            size_t outrow = i * FvBuf_row_pitch(outplane);
            for (j = 0; j < w; j++) {
                uint32_t k;
                float v1, v2, d;
                uint8_t *block = outmem +
                        (i - step) * FvBuf_row_pitch(outplane) + j;
                v1 = (float) *block;
                v2 = (float) outmem[outrow + j];
                d = (v2 - v1) / (float) step;
                v1 += d;
                block += FvBuf_row_pitch(outplane);
                for (k = 1; k < step; k++) {
                    if (*block == 0) {
                        *block = (uint8_t) lrintf(v1);
                    }
                    v1 += d; 
                    block += FvBuf_row_pitch(outplane);
                }
            }
        }
    }
}


