/**
 * @file      fvbuf.h
 * @brief     Multidimensional arrays for video
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVBUF_H
#define FVBUF_H

#include <stdint.h>
#include <stdio.h>

#include "config.h"
#include "fvfilter.h"
#include "fvsample.h"

#if HAVE_OPENCL
#include "fvopencl.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define FVBUF_ALIGNMENT 32


#define FVPAD_TOP    0
#define FVPAD_BOTTOM 1
#define FVPAD_LEFT   2
#define FVPAD_RIGHT  3

enum FvPadT {
    FVPAD_NONE,
    FVPAD_ZERO,
    FVPAD_BLACK,
    FVPAD_MIRROR,
    FVPAD_REPEAT,
    FVPAD_FIXED,
    FVPAD_T_SIZE
};


/* Maximum number of buffer dimensions must be at least 3 for
 * OpenCL compatibility */
#define FVBUF_MAX_DIMENSION 3

#define FVBUF_ROW_PITCH 0
#define FVBUF_SLICE_PITCH 1

#define FVBUF_ACCESS_READ  1
#define FVBUF_ACCESS_WRITE 2
#define FVBUF_ACCESS_INIT  4


/* Maximum number of storage bins */
#define FVBUF_MAX_BIN 4

int FvBuf_get_num_bins(void);

enum FvBinT {
    FVBIN_MAIN  = 0,
    FVBIN_CLBUF = 1,
    FVBIN_CLIMG = 2,
};

typedef struct FvBuf {
    size_t            size;
    size_t            offset;
    size_t            region[FVBUF_MAX_DIMENSION];
    void             *bin[FVBUF_MAX_BIN];

    uint32_t          bin_status;
    uint32_t          dimension; 
    enum FvComponentT component_type;
    enum FvSampleT    sample_type;

    enum FvPadT       pad_type; 
    int               pad_status;
    size_t            pad[FVBUF_MAX_DIMENSION][2];

    size_t            pitch[FVBUF_MAX_DIMENSION-1];
} FvBuf;

typedef struct FvBinMain {
    /*FvBuf          *buf;*/
    char             *mem;
    size_t            size;
    int               refcount;
} FvBinMain;

typedef struct FvBinCl {
    cl_mem            mem;
    FvClContext      *clc;
} FvBinCl;

typedef struct FvBinClImg {
    cl_mem            mem;
    FvClContext      *clc;
    cl_image_format   format;
#ifdef CL_VERSION_1_2
    cl_image_desc     desc;
#endif
    size_t            origin[FVBUF_MAX_DIMENSION];
    size_t            region[FVBUF_MAX_DIMENSION];
    size_t            pixel_shift;
} FvBinClImg;



/**
 * Returns the smallest integer that is:
 * (a) evenly divisible by the given divisor (value % divisor == 0)
 * (b) larger than or equal to value
 */
#define fv_next_divisible(value, divisor) ({ \
    typeof(value) value_ = (value); \
    typeof(value) divisor_ = (divisor); \
    (value_ == 0) ? value_ : (value_ + (divisor_ - 1) - ((value_ - 1) % divisor_)); \
})

/**
 * Returns the integer closest to zero that is:
 * (a) evenly divisible by the given divisor (value % divisor == 0)
 * (b) for a positive value: larger than or equal to value
 *     for a negative value: smaller than or equal to value
 */
#define fv_next_divisible_Z(value, divisor) ({ \
    typeof(value) value_ = (value); \
    typeof(value) divisor_ = (divisor); \
    if (divisor_ < 0) { \
        value_ = -value_; \
        divisor_ = -divisor_; \
    } \
    (value_ > 0) ? (value_ + (divisor_ - 1) - (( value_ - 1) % divisor_)) : \
        ((value_ == 0) ? value_ : \
                (-(-value_ + (divisor_ - 1) - ((-value_ - 1) % divisor_))) \
        ); \
})



/*
typedef void (*fv_fptr_Ppn)(void *out, const void *in, size_t n);
*/

/*
int  FvBuf_init(FvBuf *buf, FvSample_t sample_type, size_t num_samples);
void FvBuf_free(FvBuf *buf);
void FvBuf_zero(FvBuf *buf);
int  FvBuf_convert(FvBuf *out, FvBuf *in);
*/


int FvBuf_init_1d(FvBuf *buf, enum FvComponentT component_type,
        enum FvSampleT sample_type, size_t num_samples);
int FvBuf_init_1d_padded(FvBuf *buf, enum FvComponentT component_type,
        enum FvSampleT sample_type, size_t num_samples,
        size_t min_padding, enum FvPadT pad_type);
int FvBuf_init_2d(FvBuf *buf, enum FvComponentT component_type,
        enum FvSampleT sample_type, size_t width, size_t height);
int FvBuf_init_2d_padded(FvBuf *buf, enum FvComponentT component_type,
        enum FvSampleT sample_type, size_t width, size_t height,
        size_t min_pad, enum FvPadT pad_type);
int FvBuf_init_duplicate(FvBuf *dst, FvBuf *src, int copy_content);
void FvBuf_free(FvBuf *buf);
//int FvBuf2d_access(FvBuf2d *buf2d, enum FvBuf2dAlt alt);
FvBinMain  *FvBuf_access_main(FvBuf *buf, uint32_t flags);
FvBinClImg *FvBuf_access_climg(FvBuf *buf, uint32_t flags,
        cl_channel_order channel_order, FvClContext *cx);
FvBinCl    *FvBuf_access_clbuf(FvBuf *buf, uint32_t flags, FvClContext *cx);
int FvBuf_num_bins(void);

#define FvBuf_width(buf) ((buf)->region[0])
#define FvBuf_height(buf) ((buf)->region[1])
#define FvBuf_row_pitch(buf) ((buf)->pitch[FVBUF_ROW_PITCH])
#define FvBuf_row_pitch_samples(buf) \
    ((buf)->pitch[FVBUF_ROW_PITCH] >> fvsample_shift[(buf)->sample_type])
#define FvBuf_sample_size(buf) (fvsample_size[(buf)->sample_type])


int FvBuf_resize_2d(FvBuf *buf, uint32_t width, uint32_t height,
        int reallocate_if_smaller);

int FvBuf_zero(FvFilterNode *node, FvBuf *buf);

void FvBuf_fill(FvFilterNode *node, FvBuf *buf, const FvSampleV *val);
//int FvBuf2d_oi(FvBuf2d *out, const FvBuf2d *in,
//        const fv_fptr_Ppn **op_table);

int FvBuf_convert(FvFilterNode *node, FvBuf *out, FvBuf *in);
int FvBuf_sub(FvFilterNode *node, FvBuf *out, FvBuf *in);
int FvBuf_mul(FvFilterNode *node, FvBuf *out, FvBuf *in);
int FvBuf_sign(FvFilterNode *node, FvBuf *out, FvBuf *in);
int FvBuf_update_pad_2d(FvFilterNode *node, FvBuf *buf);
int FvBuf_check_pad_1d(const FvBuf *buf, size_t pad_x);
int FvBuf_check_pad_2d(const FvBuf *buf, size_t pad_x, size_t pad_y);


#ifdef __cplusplus
}
#endif

#endif /* FVBUF_H */

