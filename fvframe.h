/**
 * @file      fvframe.h
 * @brief     Generic frame buffers
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVFRAME_H
#define FVFRAME_H

#include <stdint.h>
#include <stdio.h>

#include "config.h"
#include "fvfilter.h"
#include "fvsample.h"
#include "fvbuf.h"

#if HAVE_OPENCL
#include "fvopencl.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern const size_t fvframe_component_index[FVCOMPONENT_T_SIZE];

#define FVFRAME_MAX_PLANE 4


typedef struct FvFrame {
    FvBuf     plane[FVFRAME_MAX_PLANE];
    uint32_t  num_planes;
    uint32_t  v_subsampling;
    uint32_t  h_subsampling;
} FvFrame;

int FvFrame_init_padded(FvFrame *frame,
        enum FvSampleT sample_type, size_t width, size_t height,
        uint32_t num_planes, uint32_t vertical_subsampling,
        uint32_t horizontal_subsampling, size_t luma_pad,
        size_t chroma_pad, enum FvPadT pad_type);
int FvFrame_init_duplicate(FvFrame *dst, FvFrame *src, int copy_content);
void FvFrame_view(FvFrame *view, FvFrame *src,
        size_t tl_x, size_t tl_y, size_t br_x, size_t br_y);
void FvFrame_free(FvFrame *frame);

void FvFrame_zero(FvFilterNode *node, FvFrame *frame);
int  FvFrame_convert(FvFilterNode *node, FvFrame *out, FvFrame *in);
int  FvFrame_sub(FvFilterNode *node, FvFrame *out, FvFrame *in);
int  FvFrame_mul(FvFilterNode *node, FvFrame *out, FvFrame *in);
int  FvFrame_sign(FvFilterNode *node, FvFrame *out, FvFrame *in);

void FvFrame_update_pad(FvFilterNode *node, FvFrame *frame);


//void FvFrame_filter_init(FvFilterNode *node, FvFilternNode *parent)

#ifdef __cplusplus
}
#endif

#endif /* FVFRAME_H */

