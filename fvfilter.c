/**
 * @file      fvfilter.c
 * @brief     Filter interface
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "cement.h"
#include "fvopencl.h"
#include "fvfilter.h"


static const char *FvFilterNode_default_name = "fvfilter";
static const char *FvFilterNode_root_name = "root";

static int FvFilterNode_process_sequence(FvFilterNode *node) {
    FvFilterNode *child = node->firstchild;
    while (child != NULL) {
        int ret = child->process(child);
        if (ret < 0) return ret;
        child = child->next;
    }
    return FV_SUCCESS;
}

void FvFilterGraph_init(FvFilterGraph *graph, const char *name) {
    memset(graph, 0, sizeof(FvFilterGraph));
    graph->name = name; 
    graph->id_counter = 1;
    graph->root.name = strdup(FvFilterNode_root_name);
    graph->root.graph = graph;
    graph->root.process = FvFilterNode_process_sequence;
    graph->root.profile.timings_limit = FVFILTER_DEFAULT_TIMINGS;
}

void FvFilterGraph_set_cl_context(FvFilterGraph *graph, FvClContext *clc) {
    graph->clcontext = clc;
}

void FvFilterGraph_free(FvFilterGraph *graph) {
    FvFilterNode_free(&graph->root);
}

void FvFilterGraph_free_recursive(FvFilterGraph *graph) {
    FvFilterNode_free_recursive(&graph->root);
}

static int FvFilterNode_set_name(FvFilterNode *node, const char *name) {
    size_t i;
    size_t n = 0;
    size_t log10 = 0;
    size_t len = strlen(name);

    if (node->parent != NULL) {
        FvFilterNode *sibling = node->parent->firstchild;
        while (sibling != NULL) {
            if ((sibling != node) && (strncmp(sibling->name, name, len) == 0)) {
                n++;
            }
            sibling = sibling->next;
        }
    }
    i = n;
    while (i > 0) {
        i = i / 10;
        log10++;
    }
    len = len + log10 + 1;
    node->name = (char *) malloc(len);
    if (node->name == NULL) {
        node->name = strdup(FvFilterNode_default_name);
        if (node->name == NULL) return FV_OUT_OF_MEMORY;
    } else {
        snprintf(node->name, len, "%s%zu", name, n);
    }
    return FV_SUCCESS;
}


int FvFilterNode_init(FvFilterNode *node, FvFilterNode *parent,
        const char *name, /*FvFilterPtr *functions,*/
        size_t arg_size, void *arg, size_t priv_size, void *priv,
        FvFilterFunction priv_free, FvFilterFunction process,
        FvFilterFunction arg_check) {
    if (parent == NULL) return FV_INVALID_ARGUMENT;
    memset(node, 0, sizeof(FvFilterNode));
    if (arg_size > 0) {
        node->context.arg = calloc(arg_size, 1);
        if (node->context.arg == NULL) {
            cm_log_error("Failed to allocate memory for filter arguments");
            return FV_OUT_OF_MEMORY;
        }
        if (arg != NULL) {
            memcpy(node->context.arg, arg, arg_size);
        }
        node->context.arg_size = arg_size;
    }
    if (priv_size > 0) {
        node->context.priv = calloc(priv_size, 1);
        if (node->context.priv == NULL) {
            cm_log_error("Failed to allocate private context");
            if (node->context.arg != NULL) free(node->context.arg);
            return FV_OUT_OF_MEMORY;
        }
        if (priv != NULL) {
            memcpy(node->context.priv, priv, priv_size);
        }
        node->context.priv_size = priv_size;
    }
    /*
    if (functions != NULL) {
        size_t n;
        for (n = 0; ftable[n] != NULL; n++);
        node->profile.functions = malloc(n * sizeof(FvFilterTiming));
        if (node->profile.functions == NULL) {
            cm_log_error("Failed to allocate filter function table");
            if (node->context.arg  != NULL) free(node->context.arg);
            if (node->context.priv != NULL) free(node->context.priv);
            return FV_OUT_OF_MEMORY;
        }
        node->profile.functions_num = n;
    }
    */
    node->process = process;
    node->arg_check = arg_check;


    FvFilterNode_add_child(parent, node);
    node->id = parent->graph->id_counter++;
    FvFilterNode_set_name(node, name);
    node->graph = parent->graph;
    node->parent = parent;
    node->context.priv_free = priv_free;
    node->profile.timings_limit = parent->profile.timings_limit;

    /*if (arg_check(node) < 0)*/
    node->flags |= FVFILTER_FLAG_CHECK_ARG;

    node->flags |= FVFILTER_FLAG_INITIALIZED;
    return FV_SUCCESS;
}


void FvFilterNode_free(FvFilterNode *node) {
    FvFilterContext *fc = &node->context;
    if (fc->arg != NULL) {
        free(fc->arg);
        fc->arg_size = 0;
        fc->arg = NULL;
    }
    if (fc->priv != NULL) {
        if (fc->priv_free != NULL) fc->priv_free(node);
        free(fc->priv);
        fc->priv_size = 0;
        fc->priv = NULL;
    }
    if (node->profile.functions != NULL) {
        free(node->profile.functions);
        node->profile.functions_num = 0;
        node->profile.functions = NULL;
    }
    if (node->name != NULL) {
        free(node->name);
        node->name = NULL;
    }
}

void FvFilterNode_free_recursive(FvFilterNode *node) {
    FvFilterNode *child = node->firstchild;
    while (child != NULL) {
        FvFilterNode_free_recursive(child);
        child = child->next;
    }
    FvFilterNode_free(node);
}

int FvFilterNode_add_child(FvFilterNode *parent, FvFilterNode *child) {
    /*if ((parent == NULL) || (child == NULL)) return FV_INVALID_ARGUMENT;*/
    if (parent->firstchild == NULL) {
        parent->firstchild = child;
    } else {
        FvFilterNode *lastchild = parent->firstchild;
        while (lastchild->next != NULL) lastchild = lastchild->next;
        lastchild->next = child;
    }
    return FV_SUCCESS;
}


int FvFilter_run(FvFilterNode *node,
        /* const char *name, */
        /*FvFilterFptr *functions,*/
        size_t arg_size, void *arg, size_t priv_size,
        FvFilterFunction priv_free, FvFilterFunction process,
        FvFilterFunction arg_check) {
    FvFilterContext *fc;
    int ret;

    if (node == NULL) return FV_INVALID_ARGUMENT;
    fc = &node->context;
    if ((fc->priv_size < priv_size) && (fc->priv != NULL)) {
        free(fc->priv);
        fc->priv = NULL;
        fc->priv_size = 0;
    }
    if (fc->priv == NULL) {
        fc->priv = calloc(1, priv_size);
        if (fc->priv == NULL) {
            fc->priv_size = 0;
            return FV_OUT_OF_MEMORY;
        }
        fc->priv_size = priv_size;
        node->flags |= FVFILTER_FLAG_CHECK_ARG;
    } else if (!(node->flags & FVFILTER_FLAG_INITIALIZED)) {
        memset(fc->priv, 0, priv_size);
        node->flags |= FVFILTER_FLAG_CHECK_ARG;
    }

    if ((fc->arg_size < arg_size) && (fc->arg != NULL)) {
        free(fc->arg);
        fc->arg = NULL;
        fc->arg_size = 0;
    }
    if (fc->arg == NULL) {
        fc->arg = calloc(1, arg_size);
        if (fc->arg == NULL) {
            fc->arg_size = 0;
            return FV_OUT_OF_MEMORY;
        }
        node->flags |= FVFILTER_FLAG_CHECK_ARG;
        fc->arg_size = arg_size;
    }
    if (arg != NULL) {
        node->flags |= FVFILTER_FLAG_CHECK_ARG;
        memcpy(fc->arg, arg, arg_size);

    } else if (!(node->flags & FVFILTER_FLAG_INITIALIZED)) {
        node->flags |= FVFILTER_FLAG_CHECK_ARG;
        memset(fc->arg, 0, arg_size);
    }

    if (priv_free != NULL) node->context.priv_free = priv_free;
    node->arg_check = arg_check;
    node->process = process;

    if (node->flags & FVFILTER_FLAG_INITIALIZED) {
        if (node->flags & FVFILTER_FLAG_CHECK_ARG) {
            ret = arg_check(node);
            if (ret == FVFILTER_CHANGED_ARG) {
                size_t i;
                if (node->flags | FVFILTER_FLAG_PROFILE) {
                    cm_log_warning("Filter arguments changed during "
                            "profiling of element %s(%zu). "
                            "Starting from beginning.",
                            node->name, node->id);
                }
                for (i = 0; i < node->profile.functions_num; i++) {
                    node->profile.functions[i].timing_pass = 0;
                }
            } else if (ret < 0) {
                cm_log_error("Invalid argument");
                return ret;
            }
            node->flags &= ~((uint32_t) FVFILTER_FLAG_CHECK_ARG);
        }
        ret = process(node);
    } else {
        /* node->name = name; */
        ret = arg_check(node);
        if (ret < 0) {
            cm_log_error("Invalid argument");
            return ret;
        }
        node->flags &= ~((uint32_t) FVFILTER_FLAG_CHECK_ARG);
        ret = process(node);
        if ((node->context.priv != NULL) && (priv_free != NULL)) {
            priv_free(node);
        }
    }

    return ret;
}


int FvFilter_call(FvFilterNode *node, FvFilterFunction *functions) {
    struct FvFilterProfile *profile;
    size_t i;
    int ret = FV_SUCCESS;
    if ((node == NULL) || (functions == NULL)) {
        cm_log_error("Invalid argument");
        return FV_INVALID_ARGUMENT;
    }
    profile = &node->profile;
    if (node->flags & FVFILTER_FLAG_PROFILE) {
        size_t n = 0;
        if (profile->functions == NULL) {
            while (functions[n] != NULL) n++;
        } else {
            while (functions[n] != NULL) {
                if (n < profile->functions_num) {
                    if (functions[n] != profile->functions[n].function) {
                       profile->functions_num = 0;
                    }
                }
                n++;
            }
        }
        if ((profile->functions_num != n) && (profile->functions != NULL)) {
            cm_log_warning("Filter function table changed during "
                           "profiling of element %s(%zu). Starting from beginning.",
                           node->name, node->id);
            free(profile->functions);
            profile->functions = NULL;
        }
        if (profile->functions == NULL) {
            char    *data;
            int64_t *timings;
            data = (char *) malloc(n * sizeof(FvFilterTiming) +
                    n * profile->timings_limit * sizeof(int64_t));
            if (data == NULL) {
                cm_log_warning("Failed to allocate memory for profiling data");
                goto noprofile;
            }
            timings = (int64_t *) (data + n * sizeof(FvFilterTiming));
            profile->functions = (FvFilterTiming *) data;
            profile->functions_num = n;
            for (i = 0; i < n; i++) {
                FvFilterTiming *ft = &profile->functions[i];
                ft->function = functions[i];
                ft->timing = &timings[i * profile->timings_limit];
                ft->timing_pass = 0;
            }
            /*profile->elapsed = 0; */
            /*profile->select = 0;*/
        }
        for (i = 0; i < n; i++) {
            int64_t time1, time2;
            size_t pass = profile->functions[i].timing_pass;
            if (pass >= profile->timings_limit) {
                continue;
            }
            time1 = cm_timestamp_nano();
            ret = profile->functions[i].function(node);
            if (ret < 0) continue;
            time2 = cm_timestamp_nano();
            time2 = time2 - time1;
            if (time2 <= 0) time2 = 1;
            profile->functions[i].timing[pass] = time2;
            profile->functions[i].timing_pass++;
            if (ret == FVFILTER_CHANGED_ARG) {
                cm_log_warning("Filter arguments changed during "
                        "profiling of element %s(%zu). Starting from beginning.",
                        node->name, node->id);
                for (i = 0; i < profile->functions_num; i++) {
                    profile->functions[i].timing_pass = 0;
                }
            }
            return ret;
        }
        node->flags |= FVFILTER_FLAG_PROFILE_READY;
    }
noprofile:
    i = 0;
    while (functions[i] != NULL) {
        if (i == profile->selected) {
            ret = functions[i](node);
            if (ret >= 0) {
                if (ret == FVFILTER_CHANGED_ARG) {
                    node->flags |= FVFILTER_FLAG_CHECK_ARG;
                }
                return ret;
            }
            cm_log_warning("Selected function terminated with error %d",
                    ret);
            break;
        }
        i++; 
    }
    i = 0;
    while (functions[i] != NULL) {
        ret = functions[i](node);
        if (ret >= 0) {
            cm_log_warning("Function selection overridden (called %zu instead of %zu)",
                    i, profile->selected);
            return ret;
        }
        i++; 
    }
    return ret;
}

