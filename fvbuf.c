/**
 * @file      fvbuf.c
 * @brief     Generic frame buffers
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "config.h"
#include "cement.h"
#include "fvsample.h"
#include "fvbuf.h"
#include "fvfilter.h"

#if HAVE_OPENCL
#include "fvopencl.h"
#endif

/*
typedef int (*FvBuf2d_alt_access)(FvBuf2d *buf2d);
static FvBuf2d_alt_access fvbuf2d_alt_access[FVBUF2D_MAX_ALTERNATIVES] = {
    [FVBUF2D_ALT_MAIN] = FvBuf2d_alt_access_main,
    [FVBUF2D_ALT_CL_BUFFER] = FvBuf2d_alt_access_cl_buffer2d,
    [FVBUF2D_ALT_CL_IMAGE2D] = FvBuf2d_alt_access_cl_image2d
};
*/
static int FvBuf_num_bins__ = 3;

int FvBuf_num_bins(void) {
    return FvBuf_num_bins__;
}

typedef int (*fptr_FvBin_copy)(FvBuf *buf);

static int FvBin_copy_main_to_clbuf(FvBuf *buf) {
    FvBinMain *src = (FvBinMain *) buf->bin[FVBIN_MAIN];
    FvBinCl   *dst = (FvBinCl   *) buf->bin[FVBIN_CLBUF];
    cl_int ret;
    if ((src == NULL) || (src->mem == NULL)) {
        cm_log_error("Source bin (main) is not available");
        return -1;
    }
    if (dst == NULL) {
        cm_log_error("Destination bin (OpenCL buffer) is not available");
        return -1;
    }
    ret = clEnqueueWriteBuffer(dst->clc->commandq, dst->mem, CL_TRUE,
            0, src->size, src->mem, 0, NULL, NULL); 
    if (ret == CL_SUCCESS) {
        return 0;
    } else {
        cm_log_error("Failed to copy from main memory to OpenCL buffer: %s",
                fvclErrorString(ret));
        return -1;
    }
}

static int FvBin_copy_main_to_climg(FvBuf *buf) {
    FvBinMain  *src = (FvBinMain *) buf->bin[FVBIN_MAIN];
    FvBinClImg *dst = (FvBinClImg   *) buf->bin[FVBIN_CLIMG];
    cl_int ret;
    if ((src == NULL) || (src->mem == NULL)) {
        cm_log_error("Source bin (main) is not available");
        return -1;
    }
    if (dst == NULL) {
        cm_log_error("Destination bin (OpenCL image) is not available");
        return -1;
    }
    ret = clEnqueueWriteImage(dst->clc->commandq, dst->mem, CL_TRUE,
            dst->origin, dst->region, buf->pitch[FVBUF_ROW_PITCH],
            buf->pitch[FVBUF_SLICE_PITCH],
            &src->mem[buf->offset], 0, NULL, NULL); 
    if (ret == CL_SUCCESS) {
        return 0;
    } else {
        cm_log_error("Failed to copy from main memory to OpenCL image: %s",
                fvclErrorString(ret));
        return -1;
    }
}

static int FvBin_copy_clbuf_to_main(FvBuf *buf) {
    FvBinCl   *src = (FvBinCl   *) buf->bin[FVBIN_CLBUF];
    FvBinMain *dst = (FvBinMain *) buf->bin[FVBIN_MAIN];
    cl_int ret;
    if (src == NULL) {
        cm_log_error("Source bin (OpenCL buffer) is not available");
        return -1;
    }
    if ((dst == NULL) || (dst->mem == NULL)) {
        cm_log_error("Destination bin (main) is not available");
        return -1;
    }
    ret = clEnqueueReadBuffer(src->clc->commandq, src->mem, CL_TRUE,
            0, dst->size, dst->mem, 0, NULL, NULL); 
    if (ret == CL_SUCCESS) {
        return 0;
    } else {
        cm_log_error("Failed to copy from main memory to OpenCL buffer: %s",
                fvclErrorString(ret));
        return -1;
    }
}

static int FvBin_copy_clbuf_to_climg(FvBuf *buf) {
    size_t src_region[3];
    FvBinCl    *src = (FvBinCl *) buf->bin[FVBIN_CLBUF];
    FvBinClImg *dst = (FvBinClImg *) buf->bin[FVBIN_CLIMG];
    cl_event done;
    cl_int ret;
    if (src == NULL) {
        cm_log_error("Source bin (OpenCL buffer) is not available");
        return -1;
    }
    if (dst == NULL) {
        cm_log_error("Destination bin (OpenCL image) is not available");
        return -1;
    }
    src_region[0] = buf->pitch[FVBUF_ROW_PITCH] >> dst->pixel_shift;
    if (buf->region[2] <= 1) {
        src_region[1] = buf->region[1];
        src_region[2] = 1;
    } else {
        src_region[1] = buf->region[1] + buf->pad[1][0] + buf->pad[1][1];
        src_region[2] = buf->region[2];
    }
    ret = clEnqueueCopyBufferToImage(src->clc->commandq, src->mem,
            dst->mem, buf->offset, dst->origin, src_region,
            0, NULL, &done); 
    if (ret == CL_SUCCESS) {
        /* Block until done */
        clWaitForEvents(1, &done);
        return 0;
    } else {
        cm_log_error("Failed to copy OpenCL buffer to OpenCL image: %s",
                fvclErrorString(ret));
        return -1;
    }
}


static int FvBin_copy_climg_to_main(FvBuf *buf) {
    FvBinClImg *src = (FvBinClImg *) buf->bin[FVBIN_CLIMG];
    FvBinMain  *dst = (FvBinMain  *) buf->bin[FVBIN_MAIN];
    cl_int ret;

    if (src == NULL) {
        cm_log_error("Source bin (OpenCL image) is not available");
        return -1;
    }
    if ((dst == NULL) || (dst->mem == NULL)) {
        cm_log_error("Destination bin (main) is not available");
        return -1;
    }
    ret = clEnqueueReadImage(src->clc->commandq, src->mem, CL_TRUE,
            src->origin, src->region, buf->pitch[FVBUF_ROW_PITCH],
            buf->pitch[FVBUF_SLICE_PITCH],
            &dst->mem[buf->offset], 0, NULL, NULL); 
    if (ret == CL_SUCCESS) {
        return 0;
    } else {
        cm_log_error("Failed to copy OpenCL image to main memory: %s",
                fvclErrorString(ret));
        return -1;
    }
}

static int FvBin_copy_climg_to_clbuf(FvBuf *buf) {
    size_t dst_region[3];
    FvBinClImg *src = (FvBinClImg *) buf->bin[FVBIN_CLIMG];
    FvBinCl    *dst = (FvBinCl    *) buf->bin[FVBIN_CLBUF];
    cl_event done;
    cl_int ret;
    if (src == NULL) {
        cm_log_error("Source bin (OpenCL buffer) is not available");
        return -1;
    }
    if (dst == NULL) {
        cm_log_error("Destination bin (OpenCL image) is not available");
        return -1;
    }
    dst_region[0] = buf->pitch[FVBUF_ROW_PITCH] >> src->pixel_shift;
    if (buf->region[2] <= 1) {
        dst_region[1] = buf->region[1];
        dst_region[2] = 1;
    } else {
        dst_region[1] = buf->region[1] + buf->pad[1][0] + buf->pad[1][1];
        dst_region[2] = buf->region[2];
    }
    ret = clEnqueueCopyImageToBuffer(src->clc->commandq, src->mem,
            dst->mem, src->origin, dst_region, buf->offset,
            0, NULL, &done); 
    if (ret == CL_SUCCESS) {
        /* Block until done */
        clWaitForEvents(1, &done);
        return 0;
    } else {
        cm_log_error("Failed to copy OpenCL image to OpenCL buffer: %s",
                fvclErrorString(ret));
        return -1;
    }
}

static fptr_FvBin_copy t_FvBin_copy[FVBUF_MAX_BIN][FVBUF_MAX_BIN] = {
    [FVBIN_MAIN][FVBIN_MAIN]   = NULL,
    [FVBIN_MAIN][FVBIN_CLBUF]  = FvBin_copy_main_to_clbuf,
    [FVBIN_MAIN][FVBIN_CLIMG]  = FvBin_copy_main_to_climg,
    [FVBIN_CLBUF][FVBIN_MAIN]  = FvBin_copy_clbuf_to_main,
    [FVBIN_CLBUF][FVBIN_CLBUF] = NULL,
    [FVBIN_CLBUF][FVBIN_CLIMG] = FvBin_copy_clbuf_to_climg,
    [FVBIN_CLIMG][FVBIN_MAIN]  = FvBin_copy_climg_to_main,
    [FVBIN_CLIMG][FVBIN_CLBUF] = FvBin_copy_climg_to_clbuf,
    [FVBIN_CLIMG][FVBIN_CLIMG] = NULL
};

static void FvBuf_free_bin_main(FvBuf *buf) {
    FvBinMain *bin = (FvBinMain *) buf->bin[FVBIN_MAIN];
    if (bin == NULL) return;
    bin->refcount--;
    if (bin->refcount <= 0) {
        if (bin->mem != NULL) cm_free(bin->mem);
        free(bin);
    }
    buf->bin[FVBIN_MAIN] = NULL;
}

static void FvBuf_free_bin_clbuf(FvBuf *buf) {
    FvBinCl *bin = (FvBinCl *) buf->bin[FVBIN_CLBUF];
    if (bin == NULL) return;
    clReleaseMemObject(bin->mem);
    free(bin);
    buf->bin[FVBIN_CLBUF] = NULL;
}

static void FvBuf_free_bin_climg(FvBuf *buf) {
    FvBinClImg *bin = (FvBinClImg *) buf->bin[FVBIN_CLIMG];
    if (bin == NULL) return;
    clReleaseMemObject(bin->mem);
    free(bin);
    buf->bin[FVBIN_CLIMG] = NULL;
}

typedef void (*fptr_FvBuf_free_bin)(FvBuf *buf);

static fptr_FvBuf_free_bin t_FvBuf_free_bin[FVBUF_MAX_BIN] = {
    [FVBIN_MAIN]  = FvBuf_free_bin_main,
    [FVBIN_CLBUF] = FvBuf_free_bin_clbuf,
    [FVBIN_CLIMG] = FvBuf_free_bin_climg,
};

static int FvBuf_bin_status(FvBuf *buf, enum FvBinT n) {
    return (buf->bin_status >> n) & 1;
}

static void FvBuf_bin_up(FvBuf *buf, enum FvBinT n) {
    buf->bin_status |= (uint32_t) (1 << n);
}

static void FvBuf_bin_up_exclusive(FvBuf *buf, enum FvBinT n) {
    buf->bin_status = (uint32_t) (1 << n);
    buf->pad_status = 0;
}


static int FvBuf_update_bin(FvBuf *buf, enum FvBinT n) {
    if (buf->bin_status & (uint32_t) (1 << n)) {
        return 0;
    } else {
        int i;
        for (i = 0; i < FvBuf_num_bins(); i++) {
            if ((i != n) && FvBuf_bin_status(buf, (enum FvBinT) i)) {
                fptr_FvBin_copy copy = t_FvBin_copy[i][n];
                if ((copy != NULL) && (copy(buf) >= 0)) {
                    FvBuf_bin_up(buf, n);
                    return 0;
                }
            }
        }
    }
    return -1;
}

FvBinMain *FvBuf_access_main(FvBuf *buf, uint32_t flags) {
    FvBinMain *bin = (FvBinMain *) buf->bin[FVBIN_MAIN];
    if (bin == NULL) {
        bin = (FvBinMain *) calloc(1, sizeof(FvBinMain));
        if (bin == NULL) {
            cm_log_error("Failed to allocate main memory bin");
            return NULL;
        }
        bin->refcount = 1;
        buf->bin[FVBIN_MAIN] = bin;
    }
    if (bin->mem == NULL) {
        bin->size = 0;
        bin->mem = (char *) cm_malloc_aligned(buf->size, FVBUF_ALIGNMENT);
        if (bin->mem == NULL) {
            cm_log_error("Failed to allocate a buffer of %zu bytes",
                    buf->size);
            /* Remove the bin */
            free(bin);
            buf->bin[FVBIN_MAIN] = NULL;
            return NULL;
        }
        bin->size = buf->size;
    }
    /* Update bin contents if necessary */
    if (flags & FVBUF_ACCESS_INIT) {
        FvBuf_bin_up_exclusive(buf, FVBIN_MAIN);
    } else {
        if (FvBuf_update_bin(buf, FVBIN_MAIN) < 0) {
            cm_log_error("Failed to update main memory buffer");
            return NULL;
        }
        if (flags | FVBUF_ACCESS_WRITE) {
            FvBuf_bin_up_exclusive(buf, FVBIN_MAIN);
        }
    }
    return bin;
}

FvBinClImg *FvBuf_access_climg(FvBuf *buf, uint32_t flags, cl_channel_order channel_order, FvClContext *clc) {
    FvBinClImg *bin = (FvBinClImg *) buf->bin[FVBIN_CLIMG];
    cl_int ret;
    int i;
    //cm_log_info("Accessing OpenCL image");
    if (bin == NULL) {
        bin = (FvBinClImg *) calloc(1, sizeof(FvBinClImg));
        if (bin == NULL) {
            cm_log_error("Failed to allocate OpenCL image bin");
            return NULL;
        }
        buf->bin[FVBIN_CLIMG] = bin;

        bin->format.image_channel_order = channel_order;
        switch (channel_order) {
            case CL_R:
            case CL_A:
            case CL_INTENSITY:
            case CL_LUMINANCE:
                bin->pixel_shift = 0;
                break;
            case CL_RG:
            case CL_RA:
                bin->pixel_shift = 1;
                break;
            case CL_RGB:
            case CL_RGBA:
            case CL_ARGB:
            case CL_BGRA:
                bin->pixel_shift = 2;
                break;
            default:
                cm_log_warning("Unknown channel order: %d", channel_order);
                bin->pixel_shift = 0;
        }
        bin->format.image_channel_data_type =
                FvSampleT_cl_channel_type(buf->sample_type);
        bin->clc = clc;
        for (i = 0; i < FVBUF_MAX_DIMENSION; i++) {
            bin->origin[i] = 0;
            bin->region[i] = buf->region[i];
        }
        bin->region[0] = buf->region[0] >> bin->pixel_shift;
#ifdef CL_VERSION_1_2
        switch (buf->dimension) {
            case 1:
                bin->desc.image_type = CL_MEM_OBJECT_IMAGE1D;
                break;
            case 2:
            default:
                bin->desc.image_type = CL_MEM_OBJECT_IMAGE2D;
                break;
            case 3:
                bin->desc.image_type = CL_MEM_OBJECT_IMAGE3D;
                break;
        }
        //bin->desc.image_width = buf->region[0] >> 2;
        bin->desc.image_width = buf->region[0] >> bin->pixel_shift;
        if (buf->dimension > 1) bin->desc.image_height = buf->region[1];
        if (buf->dimension > 2) bin->desc.image_depth  = buf->region[2];
        bin->desc.image_array_size = 0;
        bin->desc.image_row_pitch = 0;
        bin->desc.image_slice_pitch = 0;
        bin->desc.num_mip_levels = 0;
        bin->desc.num_samples = 0;
        bin->desc.buffer = NULL;
        bin->mem = clCreateImage(clc->context, CL_MEM_READ_WRITE,
                &bin->format, &bin->desc, NULL, &ret);
#else
        switch (buf->dimension) {
            case 1:
                bin->mem = clCreateImage2D(clc->context, CL_MEM_READ_WRITE,
                        &bin->format, bin->region[0], 1, 0, NULL, &ret);
                break;
            case 2:
            default:
                bin->mem = clCreateImage2D(clc->context, CL_MEM_READ_WRITE,
                        &bin->format, bin->region[0], bin->region[1],
                        0, NULL, &ret);
                break;
            case 3:
                bin->mem = clCreateImage3D(clc->context, CL_MEM_READ_WRITE,
                        &bin->format, bin->region[0], bin->region[1],
                        bin->region[2], 0, 0, NULL, &ret);
                break;
        }
#endif 
        if (ret != CL_SUCCESS) {
            cm_log_error("Failed to create OpenCL image object: %s",
                    fvclErrorString(ret));
            /* Remove the bin */
            free(bin);
            buf->bin[FVBIN_CLIMG] = NULL;
            return NULL;
        }
        buf->bin[FVBIN_CLIMG] = bin;
    }

    /* Update bin contents if necessary */
    if (flags & FVBUF_ACCESS_INIT) {
        FvBuf_bin_up_exclusive(buf, FVBIN_CLIMG);
    } else {
        if (FvBuf_update_bin(buf, FVBIN_CLIMG) < 0) {
            if (FvBuf_update_bin(buf, FVBIN_MAIN) < 0) {
                cm_log_error("Failed to update main memory bin");
                return NULL;
            } else {
                if (FvBuf_update_bin(buf, FVBIN_CLIMG) < 0) {
                    cm_log_error("Failed to update OpenCL image bin through "
                            "main memory");
                    return NULL;
                }
            }
        }
        if (flags | FVBUF_ACCESS_WRITE) {
            FvBuf_bin_up_exclusive(buf, FVBIN_CLIMG);
        }
    }
    return bin;
}


FvBinCl *FvBuf_access_clbuf(FvBuf *buf, uint32_t flags, FvClContext *clc) {
    FvBinCl *bin = (FvBinCl *) buf->bin[FVBIN_CLBUF];
    cl_int ret;
    //cm_log_info("Accessing OpenCL buffer");
    if (bin == NULL) {
        bin = (FvBinCl *) calloc(1, sizeof(FvBinCl));
        if (bin == NULL) {
            cm_log_error("Failed to allocate OpenCL buffer bin");
            return NULL;
        }
        buf->bin[FVBIN_CLBUF] = bin;
        bin->clc = clc;
        bin->mem = clCreateBuffer(clc->context, CL_MEM_READ_WRITE,
                buf->size, NULL, &ret);
        if (ret != CL_SUCCESS) {
            cm_log_error("Failed to create OpenCL buffer object: %s",
                    fvclErrorString(ret));
            /* Remove the bin */
            free(bin);
            buf->bin[FVBIN_CLBUF] = NULL;
            return NULL;
        }
        buf->bin[FVBIN_CLBUF] = bin;
    }

    /* Update bin contents if necessary */
    if (flags & FVBUF_ACCESS_INIT) {
        FvBuf_bin_up_exclusive(buf, FVBIN_CLBUF);
    } else {
        if (FvBuf_update_bin(buf, FVBIN_CLBUF) < 0) {
            if (FvBuf_update_bin(buf, FVBIN_MAIN) < 0) {
                cm_log_error("Failed to update main memory bin");
                return NULL;
            } else {
                if (FvBuf_update_bin(buf, FVBIN_CLBUF) < 0) {
                    cm_log_error("Failed to update OpenCL Image bin through "
                            "main memory");
                    return NULL;
                }
            }
        }
        if (flags | FVBUF_ACCESS_WRITE) {
            FvBuf_bin_up_exclusive(buf, FVBIN_CLBUF);
        }
    }
    return bin;
}


int FvBuf_init_1d(FvBuf *buf, enum FvComponentT component_type,
        enum FvSampleT sample_type, size_t num_samples) {
    return FvBuf_init_1d_padded(buf, component_type, sample_type,
               num_samples, 0, FVPAD_NONE);
}


int FvBuf_init_1d_padded(FvBuf *buf, enum FvComponentT component_type,
        enum FvSampleT sample_type, size_t num_samples, size_t min_pad,
        enum FvPadT pad_type) {
    size_t sample_size;
    int i;

    memset(buf, 0, sizeof(FvBuf));
    buf->bin_status     = 0xFFFFFFFF;
    buf->dimension      = 1;
    buf->component_type = component_type;
    buf->sample_type    = sample_type;

    sample_size         = fvsample_size[sample_type];

    /* Align both the left pad and the size of the buffer to
     * FVBUF_ALIGNMENT */
    if (FVBUF_ALIGNMENT % sample_size != 0) {
        cm_log_error("Buffer alignment is not divisible by sample size: "
                     "%d %% %zu != 0. Check that both are powers of 2.",
                     FVBUF_ALIGNMENT, sample_size);
        return -1;
    }
    buf->offset = fv_next_divisible(min_pad * sample_size, FVBUF_ALIGNMENT);
    buf->size   = fv_next_divisible((num_samples + min_pad) * sample_size +
            buf->offset, FVBUF_ALIGNMENT);
    buf->region[0] = num_samples;
    for (i = 1; i < FVBUF_MAX_DIMENSION; i++) {
        buf->region[i] = 1;
    }

    buf->pad[0][0]  = buf->offset / sample_size;
    buf->pad[0][1]  = (buf->size - num_samples * sample_size -
                          buf->offset) / sample_size;
    buf->pad_type   = pad_type;
    buf->pad_status = 0;
    return 0;    
}

int FvBuf_init_2d(FvBuf *buf, enum FvComponentT component_type,
        enum FvSampleT sample_type, size_t width, size_t height) {
    return FvBuf_init_2d_padded(buf, component_type, sample_type,
                width, height, 0, FVPAD_NONE);
}
 
int FvBuf_init_2d_padded(FvBuf *buf, enum FvComponentT component_type,
        enum FvSampleT sample_type, size_t width, size_t height,
        size_t min_pad, enum FvPadT pad_type) {
    size_t sample_size, left_pad_bytes;
    int i;
   
    memset(buf, 0, sizeof(FvBuf));
    buf->bin_status     = 0xFFFFFFFF;
    buf->dimension      = 2;
    buf->component_type = component_type;
    buf->sample_type    = sample_type;

    sample_size         = fvsample_size[sample_type];

    /* Align both the left pad and the row pitch to
     * FVBUF_ALIGNMENT */
    if (FVBUF_ALIGNMENT % sample_size != 0) {
        cm_log_error("Buffer alignment is not divisible by sample size: "
                     "%d %% %zu != 0. Check that both are powers of 2.",
                     FVBUF_ALIGNMENT, sample_size);
        return -1;
    }
    left_pad_bytes = fv_next_divisible(min_pad * sample_size, FVBUF_ALIGNMENT);
    buf->pitch[0]  = fv_next_divisible((width + min_pad) * sample_size +
                                       left_pad_bytes, FVBUF_ALIGNMENT);
    /*
    for (i = 1; i < FVBUF_MAX_DIMENSION-1; i++) {
        buf->pitch[i] = 0;
    }
    */

    buf->pad[0][0]  = left_pad_bytes / sample_size;
    buf->pad[0][1]  = (buf->pitch[0] - width * sample_size - left_pad_bytes)
                      / sample_size;
    buf->pad[1][0]  = min_pad;
    buf->pad[1][1]  = min_pad;
    buf->pad_type   = pad_type;
    buf->pad_status = 0;

    buf->size       = buf->pitch[0] * (height + buf->pad[1][0] +
                                                buf->pad[1][1]);
    buf->offset     = buf->pitch[0] * buf->pad[1][0] + left_pad_bytes;
    buf->region[0]  = width;
    buf->region[1]  = height;
    for (i = 2; i < FVBUF_MAX_DIMENSION; i++) {
        buf->region[i] = 1;
    }
    return 0;    
}


void FvBuf_free(FvBuf *buf) {
    int i;
    int bins = FvBuf_num_bins();
    for (i = 0; i < bins; i++) {
        t_FvBuf_free_bin[i](buf); 
    }
    memset(buf, 0, sizeof(FvBuf));
}

/*
int FvBuf_convert_main_c(FvBuf *out, FvBuf *in) {
    fptr_fvarray_Ppn operation =
            fv_array_convert_table[out->sample_type][in->sample_type];
    if (operation == NULL) {
        cm_log_error("No array conversion function from type %s to %s "
                "available",
                fv_type_names[in->sample_type],
                fv_type_names[out->sample_type]); 
        return -1;
    }
    operation(out->samples, in->samples,
            cm_min(in->num_samples, out->num_samples));
    return 0;
}

int FvBuf_convert_main_c(FvBuf *out, FvBuf *in) {
*/

int FvBuf_init_duplicate(FvBuf *dst, FvBuf *src, int copy_content) {
    int i;

    memset(dst, 0, sizeof(FvBuf));

    dst->size   = src->size;
    dst->offset = src->offset;
    for (i = 0; i < FVBUF_MAX_DIMENSION; i++) {
        dst->region[i] = src->region[i];
    }

    dst->bin_status     = 0xFFFFFFFF;
    dst->dimension      = src->dimension;
    dst->component_type = src->component_type;
    dst->sample_type    = src->sample_type;

    dst->pad_type   = src->pad_type;
    dst->pad_status = src->pad_status;
    for (i = 0; i < FVBUF_MAX_DIMENSION; i++) {
        dst->pad[i][0] = src->pad[i][0];
        dst->pad[i][1] = src->pad[i][1];
    }

    for (i = 0; i < FVBUF_MAX_DIMENSION-1; i++) {
        dst->pitch[i] = src->pitch[i];
    }

    if (copy_content) {
        FvBinMain *srcbin;
        FvBinMain *dstbin;
        srcbin = FvBuf_access_main(src, FVBUF_ACCESS_READ);
        if (srcbin == NULL) return FV_OUT_OF_MEMORY;
        dstbin = FvBuf_access_main(dst, FVBUF_ACCESS_INIT);
        if (dstbin == NULL) return FV_OUT_OF_MEMORY;
        memcpy(dstbin->mem, srcbin->mem, srcbin->size);
    } 
    return 0;
}

#if 0
int FvBuf2d_resize(FvBuf2d *buf2d, uint32_t width, uint32_t height,
                   int reallocate_if_smaller) {
    uint32_t min_padding, padding_bytes, stride_bytes;
    size_t   size;
    int i;
    FvBuf2d_access_main(buf2d, 1);
    for (i = 0; i < FvBuf2d_num_alternatives; i++) {
        if (i == FVBUF2D_ALT_MAIN) continue;
        FvBuf2d_alt_free[i](buf2d);
    }
    min_padding = buf2d->padding[FV_PADDING_TOP];
    padding_bytes = fv_next_divisible(min_padding * buf2d->buf.sample_size,
            FVBUF_ALIGNMENT);
    stride_bytes  = fv_next_divisible(padding_bytes + (width + min_padding) *
            buf2d->buf.sample_size, FVBUF_ALIGNMENT);
    size = ((size_t) stride_bytes) * ((size_t) (height + 2 * min_padding));
    if ((size > buf2d->buf.mem_size) ||
       ((size < buf2d->buf.mem_size) && (reallocate_if_smaller))) {
        buf2d->buf.mem = cm_realloc_aligned(buf2d->buf.mem, size,
                FVBUF_ALIGNMENT);
        if (buf2d->buf.mem == NULL) {
            buf2d->buf.mem_size = 0;
            buf2d->buf.samples.u8 = NULL;
            return -1;
        }
        buf2d->buf.mem_size = size;
    }
    buf2d->buf.samples.u8 = ((uint8_t *) buf2d->buf.mem) +
            min_padding * stride_bytes + padding_bytes;
    buf2d->width = width;
    buf2d->height = height;
    buf2d->stride_bytes = stride_bytes;
    buf2d->stride = stride_bytes / buf2d->buf.sample_size;
    buf2d->padding[FV_PADDING_LEFT]  = padding_bytes / buf2d->buf.sample_size;
    buf2d->padding[FV_PADDING_RIGHT] = (stride_bytes -
            width * buf2d->buf.sample_size - padding_bytes) /
            buf2d->buf.sample_size;
    return 0;
}

int FvBuf_fill_2d(FvBuf *buf, const FvSampleV *sample) {
    uint8_t *p;
    uint32_t i;
    FvValue  v; 
    FvBinMain bin = FvBuf_access_main(buf, FVBUF_ACCESS_INIT);
    if (bin == NULL) return -1;
    p = buf->buf.samples.u8;
    v.type = buf2d->buf.sample_type;
    fv_value_convert(&v.value, v.type, &val->value, val->type);
    for (i = 0; i < buf2d->height; i++) {
        fv_vector_broadcast_value(p, &v, buf2d->width);

        fv_sample_fill_array(p, buf->sample_type,
                    sample->value, sample->type,
                    buf2d->width);
        p += buf2d->stride_bytes;
    }
    buf2d->padding_status = 0;
}
#endif


int FvBuf_check_pad_1d(const FvBuf *buf, size_t pad_x) {
    int cmp = 1;
    cmp *= (buf->pad[0][0] >= pad_x);
    cmp *= (buf->pad[0][1] >= pad_x);
    return cmp - 1;
}


int FvBuf_check_pad_2d(const FvBuf *buf, size_t pad_x, size_t pad_y) {
    int cmp = 1;
    cmp *= (buf->pad[0][0] >= pad_x);
    cmp *= (buf->pad[0][1] >= pad_x);
    cmp *= (buf->pad[1][0] >= pad_y);
    cmp *= (buf->pad[1][1] >= pad_y);
    return cmp - 1;
}

static void mirror_2d(uint8_t *dst, uint8_t *src,
        int cols, int rows, int pitch, int sample_size, 
        int dst_step_x, int dst_step_y, int src_step_x, int src_step_y) {
    int i;
    switch (sample_size) {
        case 1:
            for (i = 0; i < rows; i++) {
                uint8_t *src_8 = src;
                uint8_t *dst_8 = dst;
                int j;
                for (j = 0; j < cols; j++) {
                    *dst_8 = *src_8;
                    src_8 += src_step_x;
                    dst_8 += dst_step_x;
                }
                src += pitch * src_step_y;
                dst += pitch * dst_step_y;
            }
            break;
        case 2:
            for (i = 0; i < rows; i++) {
                uint16_t *src_16 = (uint16_t *) src;
                uint16_t *dst_16 = (uint16_t *) dst;
                int j;
                for (j = 0; j < cols; j++) {
                    *dst_16 = *src_16;
                    src_16 += src_step_x;
                    dst_16 += dst_step_x;
                }
                src += pitch * src_step_y;
                dst += pitch * dst_step_y;
            }
            break;
        case 4:
            for (i = 0; i < rows; i++) {
                uint32_t *src_32 = (uint32_t *) src;
                uint32_t *dst_32 = (uint32_t *) dst;
                int j;
                for (j = 0; j < cols; j++) {
                    *dst_32 = *src_32;
                    src_32 += src_step_x;
                    dst_32 += dst_step_x;
                }
                src += pitch * src_step_y;
                dst += pitch * dst_step_y;
            }
            break;
        case 8:
            for (i = 0; i < rows; i++) {
                uint64_t *src_64 = (uint64_t *) src;
                uint64_t *dst_64 = (uint64_t *) dst;
                int j;
                for (j = 0; j < cols; j++) {
                    *dst_64 = *src_64;
                    src_64 += src_step_x;
                    dst_64 += dst_step_x;
                }
                src += pitch * src_step_y;
                dst += pitch * dst_step_y;
            }
            break;
        default:
            for (i = 0; i < rows; i++) {
                uint8_t *src_8 = (uint8_t *) src;
                uint8_t *dst_8 = (uint8_t *) dst;
                int j;
                for (j = 0; j < cols; j++) {
                    size_t k;
                    for (k = 0; k < sample_size; k++) {
                        dst_8[k] = src_8[k];
                    }
                    src_8 += src_step_x * sample_size;
                    dst_8 += dst_step_x * sample_size;
                }
                src += pitch * src_step_y;
                dst += pitch * dst_step_y;
            }
            break;
    }
}


static int FvBuf_pad_2d_mirror(FvBuf *buf, int step) {
    FvBinMain *bin = (FvBinMain *) FvBuf_access_main(buf, 0);
    size_t sample_size = fvsample_size[buf->sample_type];
    int pitch = (int) buf->pitch[0];
    int lines;
    int w = (int) buf->region[0];
    int h = (int) buf->region[1];
    size_t bottomleft;
    size_t topleft = buf->offset;
    size_t topright = buf->offset + buf->region[0] * sample_size;

    if (bin == NULL) return FV_INVALID_ARGUMENT;

    step = cm_max(0, step);

    /* Left */
    lines = (int) buf->pad[0][0];
    if (step > 0) {
        int lines_inside = (w + step - 1) / step;
        int lines_outside = lines - lines_inside;
        if (lines_outside > 0) {
            /* Left pad is larger than buffer width. Repeat the rightmost
             * column to fill remaining pad area. */
            mirror_2d(bin->mem + topleft  - sample_size * (size_t) lines,
                      bin->mem + topright - sample_size, lines_outside, h,
                      pitch, (int) sample_size, 1, 1, 0, 1);
            lines = lines_inside;
        }
    }
    mirror_2d(bin->mem + topleft - sample_size, bin->mem + topleft,
              lines, h, pitch, (int) sample_size, -1, 1, step, 1);

    /* Right */
    lines = (int) buf->pad[0][1];
    if (step > 0) {
        int lines_inside = (w + step - 1) / step;
        int lines_outside = lines - lines_inside;
        if (lines_outside > 0) {
            lines = lines_inside;
            mirror_2d(bin->mem + topright + sample_size * (size_t) lines,
                      bin->mem + topleft, lines_outside, h,
                      pitch, (int) sample_size, 1, 1, 0, 1);
        }
    }
    mirror_2d(bin->mem + topright, bin->mem + topright - sample_size,
              lines, h, pitch, (int) sample_size, 1, 1, -step, 1);


    topleft = buf->offset - buf->pad[0][0] * sample_size;
    bottomleft = topleft + (size_t) h * (size_t) pitch;
    w = pitch / (int) sample_size;

    /* Top */
    lines = (int) buf->pad[1][0];
    if (step > 0) {
        int lines_inside = (h + step - 1) / step;
        int lines_outside = lines - lines_inside;
        if (lines_outside > 0) {
            mirror_2d(bin->mem, bin->mem + bottomleft - (size_t) pitch,
                      w, lines_outside, pitch, (int) sample_size, 1, 1, 1, 0);
            lines = lines_inside;
        }
    }
    mirror_2d(bin->mem + topleft - (size_t) pitch, bin->mem + topleft,
              w, lines, pitch, (int) sample_size, 1, -1, 1, step);

    /* Bottom */
    lines = (int) buf->pad[1][1];
    if (step > 0) {
        int lines_inside = (h + step - 1) / step;
        int lines_outside = lines - lines_inside;
        if (lines_outside > 0) {
            lines = lines_inside;
            mirror_2d(bin->mem + bottomleft + (size_t) pitch * (size_t) lines,
                      bin->mem + topleft, w, lines_outside,
                      pitch, (int) sample_size, 1, 1, 1, 0);
        }
    }
    mirror_2d(bin->mem + bottomleft, bin->mem + bottomleft - (size_t) pitch,
              w, lines, pitch, (int) sample_size, 1, 1, 1, -step);

    return FV_SUCCESS;
}

int FvBuf_update_pad_2d(FvFilterNode *node, FvBuf *buf) {
    FvBinMain *bin;
    size_t   i, n, offset_end, w, h, pitch, sample_size;
    size_t   row_bytes, bottom_bytes, between_bytes;
    uint8_t *dst;
    const FvSampleV *fill_sample =
            &fvcolor_black_fullrange_u8.component[buf->component_type];
    int ret = FV_SUCCESS;

    (void) node; /* unused */

    if (buf->pad_type == FVPAD_NONE) {
        buf->pad_status = 1;
        return 0;
    }
    bin = (FvBinMain *) FvBuf_access_main(buf, 0);
    if (bin == NULL) {
        return -1;
    }
    /* Discard all bins except main and climg. OpenCL images are padded
     * internally, so they don't need to be updated. */
    buf->bin_status &= (1 << FVBIN_MAIN) + (1 << FVBIN_CLIMG);

    w     = buf->region[0];
    h     = buf->region[1];            
    pitch = buf->pitch[0];
    sample_size   = fvsample_size[buf->sample_type];
    row_bytes     = w * (size_t) sample_size;
    offset_end    = buf->offset + row_bytes + h * pitch;
    bottom_bytes  = bin->size - offset_end;
    between_bytes = pitch - row_bytes; 
 
    switch (buf->pad_type) {
        case FVPAD_NONE:
            break;
        case FVPAD_ZERO:
            /* Top */
            memset(bin->mem, 0, buf->offset);
            /* Sides */
            dst = bin->mem + buf->offset + row_bytes;
            for (i = 0; i < h-1; i++) {
                memset(dst, 0, between_bytes);
                dst += pitch;
            }
            /* Bottom */
            memset(bin->mem + offset_end, 0, bottom_bytes);
            break;
        case FVPAD_BLACK:
            /* Top */
            fvsample_fill_array(bin->mem, buf->sample_type,
                    &fill_sample->value.u8, fill_sample->type,
                    buf->offset / sample_size);
            /* Sides */
            dst = bin->mem + buf->offset + row_bytes;
            n = between_bytes / sample_size;
            for (i = 0; i < h-1; i++) {
                fvsample_fill_array(dst, buf->sample_type,
                    &fill_sample->value.u8, fill_sample->type, n);
                dst += pitch;
            }
            /* Bottom */
            fvsample_fill_array(bin->mem + buf->offset, buf->sample_type,
                    &fill_sample->value.u8, fill_sample->type,
                    bottom_bytes / sample_size);
            break;
        case FVPAD_REPEAT:
            ret = FvBuf_pad_2d_mirror(buf, 0);
            break;
        case FVPAD_MIRROR:
            ret = FvBuf_pad_2d_mirror(buf, 1);
            break;
        default:
            break;
    }
    if (ret == FV_SUCCESS) buf->pad_status = 1;
    return ret;
}

static int FvBuf_zero_main_c(FvBuf *buf) {
    FvBinMain *bin = (FvBinMain *) FvBuf_access_main(buf, FVBUF_ACCESS_INIT);
    if (bin == NULL) return FV_INVALID_ARGUMENT;
    memset(bin->mem, 0, bin->size);
    buf->pad_status = 1;
    return FV_SUCCESS;
}

int FvBuf_zero(FvFilterNode *node, FvBuf *buf) {
    (void) node; /* unused */
    /* TODO: add OpenCL versions */
    return FvBuf_zero_main_c(buf);
}


static int FvBuf_process_oi(FvFilterNode *node, FvBuf *out, FvBuf *in,
        fptr_fvsample_process_array_Ppn operation) {
    uint32_t i;
    uint32_t maxd = cm_min(out->dimension, in->dimension);
    FvBinMain *outbin;
    FvBinMain *inbin;

    (void) node; /* unused */

    if (operation == NULL) {
        cm_log_error("No array operation from type %s to %s "
                "available",
                fvsample_name[in->sample_type],
                fvsample_name[out->sample_type]); 
        return FV_INVALID_ARGUMENT;
    }
    outbin = (FvBinMain *) FvBuf_access_main(out, FVBUF_ACCESS_WRITE);
    inbin  = (FvBinMain *) FvBuf_access_main(in,  FVBUF_ACCESS_READ);
    if (maxd == 0) return 0;
    if (maxd == 1) {
        size_t size = cm_min(out->region[0], in->region[0]);
        operation(outbin->mem + out->offset, inbin->mem + in->offset, size);
    } else if (maxd == 2) {
        uint8_t *out_row = outbin->mem + out->offset;
        uint8_t *in_row  =  inbin->mem +  in->offset;
        size_t   w = cm_min(out->region[0], in->region[0]);
        size_t   h = cm_min(out->region[1], in->region[1]);
        for (i = 0; i < h; i++) {
            operation(out_row, in_row, w);
            out_row += out->pitch[0];
            in_row  +=  in->pitch[0];
        }
    } else {
        cm_log_error("Processing %u dimensions is not supported", maxd);
        return FV_INVALID_ARGUMENT;
    }
    out->pad_status = 0;
    return FV_SUCCESS;
}

int FvBuf_convert(FvFilterNode *node, FvBuf *out, FvBuf *in) {
    return FvBuf_process_oi(node, out, in,
            fvsample_convert_array_table[out->sample_type][in->sample_type]);
}

int FvBuf_sub(FvFilterNode *node, FvBuf *out, FvBuf *in) {
    return FvBuf_process_oi(node, out, in,
            fvsample_sub_array_table[out->sample_type][in->sample_type]);
}

int FvBuf_mul(FvFilterNode *node, FvBuf *out, FvBuf *in) {
    return FvBuf_process_oi(node, out, in,
            fvsample_mul_array_table[out->sample_type][in->sample_type]);
}

int FvBuf_sign(FvFilterNode *node, FvBuf *out, FvBuf *in) {
    return FvBuf_process_oi(node, out, in,
            fvsample_sign_array_table[out->sample_type][in->sample_type]);
}


