/* Define to 1 if you have the `posix_memalign' function. */
#define HAVE_POSIX_MEMALIGN 1

/* Define to 1 if you have the `memalign' function in malloc.h. */
#define HAVE_MALLOC_H 0

/* Define to 1 if you have the `strerror_l' function. */
#define HAVE_STRERROR_L 0

#define HAVE_DECL_STRERROR_R 1

#define HAVE_STRERROR_R 1

#define HAVE_FOPEN64 0

#define HAVE_GET_FMODE 0

#define HAVE_FMODE 0

#define HAVE_FSEEKO64 0

#define HAVE_FSEEKI64 0

#define HAVE_FSEEKO 1

#define HAVE_FTELLO64 0

#define HAVE_FTELLI64 0

#define HAVE_FTELLO 1

#define HAVE_GETDELIM 0

#define HAVE_OPENCL 1

#define HAVE_Y4M_FVFRAME 1

/* release build */
#define NDEBUG 1


