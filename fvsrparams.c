/**
 * @file      fvsrparams.c
 * @brief     Forevid Super-resolution filter parameter functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <stdlib.h>
#include <string.h>

#include "cement.h"
#include "fvsr.h"
#include "fvopencl.h"

#define OPT_FLOW_TEMPLATE        'F'
#define OPT_FLOW_OFFSET          'O'
#define OPT_VALIDATION_RADIUS    'V'
#define OPT_VALIDATION_THRESHOLD 'T'
#define OPT_SR_RADIUS            'R'
#define OPT_SR_SCALE             'S'
#define OPT_PSF_SIGMA_H          'X'
#define OPT_PSF_SIGMA_V          'Y'
#define OPT_ITERATIONS           'i'
#define OPT_DESC_START           's'
#define OPT_DESC_FACTOR          'x'
#define OPT_REG_FACTOR           'f'
#define OPT_REG_RADIUS           'r'
#define OPT_REG_DECAY            'd'
#define OPT_OPENCL               'C'
#define OPT_SHOW_USAGE           'h'
#define OPT_OUTPUT               'o'

static int FvSrParams_parse_option(int option_id, const char *arg,
        void *fvsrparams);

static const cm_option cli_options[] = {
    {"flow-template", "string",
     "Path and filename template to optical flow files. Example: "
     "\"~/Videos/clip1/c000.111.flo\". Zeros in the template signify "
     "target frame number. Ones signify reference frame number.",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_FLOW_TEMPLATE},
    {"flow-offset", "integer",
     "Offset of frame numbering in optical flow file names. "
     "Input frames are counted from 0. If optical flow files are "
     "numbered as 001..100, for example, use --flow-offset 1. Default: 0",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_FLOW_OFFSET},
    {"validation-radius", "integer",
     "Radius of SAD blocks used in optical flow validation. Default: 3",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_VALIDATION_RADIUS},
    {"validation-threshold", "integer",
     "Intensity threshold used in optical flow validation. Default: 20",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_VALIDATION_THRESHOLD},
    {"sr-radius", "integer",
     "Superresolution temporal radius in frames. Default: 16",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_SR_RADIUS},
    {"sr-scale", "integer",
     "Superresolution scale factor. Default: 3",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_SR_SCALE},
    {"psf-sigma-x", "float",
     "Point spread function horizontal standard deviation. Default: 1.0",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_PSF_SIGMA_H},
    {"psf-sigma-y", "float",
     "Point spread function vertical standard deviation. Default: 1.0",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_PSF_SIGMA_V},
    {"iterations", "integer",
     "Number of iterations for the reconstruction phase. Default: 20",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_ITERATIONS},
    {"desc-start", "float",
     "Initial descent speed (beta) as factor of 8 bit sampling "
     "resolution: 1.0 equals one 8 bit quantized sampling step or 1/256 "
     "of the whole range, 0.5 = half of the final quantized sampling "
     "resolution. Default: 10.0",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_DESC_START},
    {"desc-factor", "float",
     "Descent slowdown factor at each step. For example, if iterations=20, "
     "desc-start=10.0 and desc-factor=0.85, beta at the final step is "
     "10.0 * 0.85^19 = 0.46. "
     "Default: 0.85 (10.0 * 0.85^20 = 0.39)",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_DESC_FACTOR},
    {"reg-factor", "float",
     "Regularization factor (lambda). Applied the same way as descent "
     "speed (beta). Default: 1.0",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_REG_FACTOR},
    {"reg-radius", "integer",
     "Bilateral regularization radius (P). Default: 2",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_REG_RADIUS},
    {"reg-decay", "float",
     "Bilateral regularization decay factor (alpha). Default: 0.7",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_REG_DECAY},
    {"output", "string",
     "Filename for YUV4MPEG output. Example: \"output.y4m\"",
     FvSrParams_parse_option, CM_ARG_REQUIRED, OPT_OUTPUT},
    {"opencl", "string",
     "Use OpenCL. Values: yes, no, list, <platform>:<device>. "
     "Example: \"--opencl 0:1\" selects OpenCL platform #0, device #1. "
     "Default: no",
     FvSrParams_parse_option, CM_ARG_OPTIONAL, OPT_OPENCL},
    {"help", NULL,
     "Show this list of options",
     FvSrParams_parse_option, CM_ARG_NONE, OPT_SHOW_USAGE},
    {NULL, NULL, NULL, FvSrParams_parse_option, CM_ARG_REQUIRED, -1}
};


static int FvSrParams_parse_opencl(FvSrParams *params, const char *arg) {
    if ((arg == NULL) || (*arg == '\0') ||
            (!strcmp(arg, "yes")) || (!strcmp(arg, "on"))) {
        params->opencl = 1;
    } else if ((!strcmp(arg, "no")) || (!strcmp(arg, "off"))) {
        params->opencl = 0;
    } else if (!strcmp(arg, "list")) {
        params->show_usage = OPT_OPENCL;
    } else if (!strcmp(arg, "help")) {
        params->show_usage = OPT_OPENCL;
   } else {
        char *pos;
        int   err = 0;
        int   platform = (int) strtol(arg, &pos, 10);
        if (pos != arg) {
            params->opencl = 1;
            params->opencl_platform = platform;
        }
        if (*pos != '\0') {
            if (pos[0] == ':') {
                if (pos[1] != '\0') {
                    char *end;
                    int device = (int) strtol(pos+1, &end, 10);
                    if (end != pos) {
                        params->opencl = 1;
                        params->opencl_device = device;
                    } else {
                        err = 1;
                    }
                }
            } else {
                err = 1;
            }
        }
        if (err) {
            cm_log_warning("Argument \"%s\" does not appear to be "
                    "an OpenCL device spec", arg); 
            cm_log_info("OpenCL parameter usage: "
                    "--opencl [yes | no | list | <platform>:<device>]");
            cm_log_info("               example: --opencl 1:2");
            return -2;
        }
    }
    return 0;
}
static int FvSrParams_parse_flow_template(FvSrParams *params,
        const char *arg) {
    int   i = 0;
    int   num_fields = 0;
    int   fieldlen = 0;
    char  curfield = '\0';
    char *temp = (char *) calloc(strlen(arg) + 8, sizeof(char));

    if (temp == NULL) {
        cm_log_error("Failed to allocate memory for a template string");
        return -1;
    }
    params->flow_template_reverse_params = 0;
    do {
        if (num_fields <= 2) {
            if (curfield == *arg) {
                fieldlen++;
            } else {
                if (fieldlen > 0) {
                    /* End of a numeric field. Convert to a printf-style
                     * directive. */
                    if (fieldlen == 1) {
                        i += sprintf(temp + i, "%%u");
                    } else {
                        i += sprintf(temp + i, "%%.%du", fieldlen);
                    }
                }
                if ((*arg == '0') || (*arg == '1')) {
                    if ((num_fields == 0) && (*arg == '1')) {
                        params->flow_template_reverse_params = 1;
                    }
                    num_fields++;
                    fieldlen = 1;
                    curfield = *arg;
                } else {
                    fieldlen = 0;
                    curfield = '\0';
                    temp[i++] = *arg;
                }
            }
        } else {
            temp[i++] = *arg;
        }
    } while (*(arg++) != '\0');
    if (num_fields < 2) {
        cm_log_error("Flow template has less than two numeric fields.");
        return -1;
    }
    if (params->flow_template != NULL) {
        free(params->flow_template);
    }
    params->flow_template = temp;
    /* fprintf(stderr, "Template: \"%s\"\n", temp); */
    return 0;
}

static int FvSrParams_parse_option(int option_id, const char *arg,
        void *fvsrparams) {
    int ret;
    FvSrParams *params = (FvSrParams *) fvsrparams;
    //fprintf(stdout, "%d (-%c): \"%s\"\n", option_id, option_id, arg);
    switch (option_id) {
       case OPT_FLOW_TEMPLATE:
            return FvSrParams_parse_flow_template(params, arg);
        case OPT_FLOW_OFFSET:
            params->flow_offset = atoi(arg);
            break;
        case OPT_VALIDATION_RADIUS:
            params->validation_radius = (unsigned int) cm_max(0, atoi(arg));
            break;
        case OPT_VALIDATION_THRESHOLD:
            params->validation_threshold = (unsigned int) cm_max(0, atoi(arg));
            break;
        case OPT_SR_RADIUS:
            params->sr_radius = (unsigned int) cm_max(0, atoi(arg));
            break;
        case OPT_SR_SCALE:
            params->sr_scale = (unsigned int) cm_max(0, atoi(arg));
            break;
        case OPT_PSF_SIGMA_H:
            params->psf_sigma_h = strtof(arg, NULL);
            params->psf_radius_h = (uint32_t)
                    lrintf(3.0F * params->psf_sigma_h);
            break;
        case OPT_PSF_SIGMA_V:
            params->psf_sigma_v = strtof(arg, NULL);
            params->psf_radius_v = (uint32_t)
                    lrintf(3.0F * params->psf_sigma_v);
            break;
        case OPT_ITERATIONS:
            params->iterations = (unsigned int) cm_max(0, atoi(arg));
            break;
        case OPT_DESC_START:
            params->desc_start = cm_max(0.0F, strtof(arg, NULL)) / 256.0F;
            break;
        case OPT_DESC_FACTOR:
            params->desc_factor = cm_max(0.0F, cm_min(1.0F, strtof(arg, NULL)));
            break;
        case OPT_REG_FACTOR:
            params->reg_factor = cm_max(0.0F, strtof(arg, NULL)) / 256.0F;
            break;
        case OPT_REG_DECAY:
            params->reg_decay = cm_max(0.0F, cm_min(1.0F, strtof(arg, NULL)));
            break;
        case OPT_REG_RADIUS:
            params->reg_radius = (unsigned int) cm_max(0, atoi(arg));
            break;
        case OPT_OUTPUT:
            params->output = arg;
            break;
        case OPT_SHOW_USAGE:
            params->show_usage = OPT_SHOW_USAGE;
            break;
        case OPT_OPENCL:
            ret = FvSrParams_parse_opencl(params, arg);
            if (ret >= -1) return ret;
        case -1:
            if (params->num_files < params->max_files) {
                params->files[params->num_files++] = arg;
            } else {
                /* This shouldn't happen if the parameters struct has
                 * been initialized with init_parameters() */
                cm_log_error("No space left for argument");
                return -1;
            }
            break;
        default:
            cm_log_error("Unknown option 0x%.2x", option_id);
            return -1;
    }
    return 0;
}


void FvSrParams_init(FvSrParams *params, int max_files) {
    memset(params, 0, sizeof(FvSrParams));

    if (max_files < 0) params->max_files = 0;
    else params->max_files = (unsigned int) max_files;
    params->files = (const char **) malloc(params->max_files *
                                           sizeof(const char *));
    params->sr_radius = 16;
    params->sr_scale = 3;
    params->validation_radius = 3;
    params->validation_threshold = 20;
    params->psf_sigma_h = 1.0F;
    params->psf_sigma_v = 1.0F;
    params->psf_radius_h = 3;
    params->psf_radius_v = 3;

    params->iterations = 20;
    params->reg_radius = 2;
    params->reg_decay = 0.7F;
    params->reg_factor = 1.0F/256.0F;
    params->desc_start = 10.0F/256.0F;
    params->desc_factor = 0.85F;

    params->mv_average_radius = 0;
    params->interpolation = FVSR_INTERPOLATE_OUTPUT;
    //params->interpolation = FVSR_INTERPOLATE_INPUT;
    //params->interpolation = FVSR_INTERPOLATE_NONE;

    params->num_channels = 4;

    params->opencl = 0;
    params->opencl_platform = 0;
    params->opencl_device = 0;
}


int FvSrParams_parse(FvSrParams *params, int argc,
        const char * const *argv) {
    cm_parse_argv(argc, argv, cli_options, params);

    if (params->show_usage == OPT_SHOW_USAGE) {
        fprintf(stderr, "\nusage: %s [options] <video.y4m>\n", argv[0]);
        fputs("\nwhere options are:\n\n", stderr);
        cm_print_options(stderr, cli_options, 32, 46);
        fputs("\n", stderr);
        return -1;
    } else if (params->show_usage == OPT_OPENCL) {
        fvclPrintPlatforms();
    }

    if (params->show_usage) {
        return -1;
    }

    if (params->num_files < 1) {
        cm_log_error("No video files given. For usage information, "
                     "run %s without arguments or with option -h", argv[0]);
        return -1;
    }
    if ((params->output == NULL) || (params->output[0] == '\0')) {
        cm_log_error("No output file given. For usage information, "
                     "run %s without arguments or with option -h", argv[0]);
        return -1;
    }
    return 0;
}

void FvSrParams_free(FvSrParams *params) {
    if (params->files != NULL) {
        free(params->files);
    }
    if (params->flow_template != NULL) {
        free(params->flow_template);
    }
    params->files = NULL;
    params->flow_template = NULL;
    params->num_files = 0;
}


