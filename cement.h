/**
 * @file      cement.h
 * @brief     C Environment -- cross-platform C programming utilities
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright 2-clause BSD
 *
 * @section LICENSE
 *
 * Copyright (c) 2013, Niko Mikkilä
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer
 *    in the documentation and/or other materials provided with
 *    the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef CEMENT_H
#define CEMENT_H

#include <stdio.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Set up attribute_printf which can be used in function declarations
 * for compile-time checking of a printf-style format string against
 * associated parameters. */
#ifdef __GNUC__
#  define GCC_VERSION (__GNUC__*10000+__GNUC_MINOR__*100+__GNUC_PATCHLEVEL__)
#  if GCC_VERSION >= 40400
#    define attribute_printf(m,n) __attribute__((format(gnu_printf, (m), (n))))
#  else
#    define attribute_printf(m,n) __attribute__((format(printf, (m), (n))))
#  endif
#else
#  define attribute_printf(m,n)
#endif

/* Thread-local storage */
#ifdef __GNUC__
#  define tls __thread
#endif
#ifdef _MSC_VER
#  define tls __declspec(thread)
#endif


/* These SIZE_T_MAX and OFF_T_MIN/MAX definitions are copied from
 * http://src.chromium.org/native_client/trunk/src/native_client/src/include/portability.h
 */

#ifndef SIZE_T_MAX
#  define SIZE_T_MAX ((size_t) -1)
#endif

/* use uint64_t as largest integral type, assume 8 bit bytes */
#ifndef OFF_T_MIN
#  define OFF_T_MIN ((off_t) (((uint64_t) 1) << (8 * sizeof(off_t) - 1)))
#endif
#ifndef OFF_T_MAX
#  define OFF_T_MAX ((off_t) ~(((uint64_t) 1) << (8 * sizeof(off_t) - 1)))
#endif


/** Log levels */
enum cm_log_level {
    CM_LOG_LEVEL_NONE    = 0,
    CM_LOG_LEVEL_ERROR   = 1,
    CM_LOG_LEVEL_WARNING = 2,
    CM_LOG_LEVEL_INFO    = 3,
    CM_LOG_LEVEL_DEBUG   = 4
};

#define CM_DEFAULT_LOG_TARGET stderr

#define cm_log_error(...) cm_log(CM_LOG_LEVEL_ERROR, __FILE__, \
                                   __func__, __LINE__, __VA_ARGS__)

#define cm_log_warning(...) cm_log(CM_LOG_LEVEL_WARNING, __FILE__, \
                                     __func__, __LINE__, __VA_ARGS__)

#define cm_log_info(...) cm_log(CM_LOG_LEVEL_INFO, \
                                  NULL, NULL, 0, __VA_ARGS__)

#define cm_log_debug(...) cm_log(CM_LOG_LEVEL_DEBUG, __FILE__, \
                                   __func__, __LINE__, __VA_ARGS__)


/** 
 * Returns the smaller of two numerical values.
 * This function is implemented as a preprocessor macro that evaluates
 * each parameter only once.
 */
#define cm_min(a, b) ({ \
    typeof(a) min2_a_ = (a); \
    typeof(b) min2_b_ = (b); \
    (void) (&min2_a_ == &min2_b_); \
    min2_a_ < min2_b_ ? min2_a_ : min2_b_; \
})

/** 
 * Returns the larger of two numerical values.
 * This function is implemented as a preprocessor macro that evaluates
 * each parameter only once.
 */
#define cm_max(a, b) ({ \
    typeof(a) max2_a_ = (a); \
    typeof(b) max2_b_ = (b); \
    (void) (&max2_a_ == &max2_b_); \
    max2_a_ > max2_b_ ? max2_a_ : max2_b_; \
})



/** Minimum memory alignment for cm_malloc */
#define CM_MIN_ALIGNMENT 32

/**
 * Allocate memory aligned to CM_MIN_ALIGNMENT. Returned area must be freed
 * with cm_free.
 */
#define cm_malloc(s) cm_malloc_aligned((s), CM_MIN_ALIGNMENT)

/**
 * Allocate or update a dynamic array. This macro aligns the array to
 * CM_MIN_ALIGNMENT.
 *
 * Allocated array must be freed with cm_free.
 */
#define cm_dynamic_array_update(pptr_, cap_elements_, num_elements_, element_size_) \
cm_dynamic_array_update_aligned((pptr_), (cap_elements_), (num_elements_), (element_size_), CM_MIN_ALIGNMENT)



/** Argument presence values */
enum cm_arg_presence {
    CM_ARG_NONE     = 0,
    CM_ARG_OPTIONAL = 1,
    CM_ARG_REQUIRED = 2
};


/**
 * CM command-line option data structure
 */
typedef struct {
    /* Long option string */
    const char *long_option;
    /* Short argument type description: "int", "float", "string", ... */
    const char *arg_type;
    /* Option description */
    const char *description;
    /* Parser callback function */
    int (*parser)(int option_id, const char *arg, void *parameters);
    /* Argument presence */
    enum cm_arg_presence arg_presence;
    /* Option ID and short option if alphanumeric. Use -1 for the last
     * option in an array of options to pass a parser function for non-option
     * arguments and signal the end of the list. */
    int id;
} cm_option;



/* Function declarations */

FILE *cm_get_log_target(void);

void cm_set_log_target(FILE *target);

enum cm_log_level cm_get_log_level(void);

void cm_set_log_level(enum cm_log_level level);

void cm_log(enum cm_log_level level, const char *file,
             const char *function, int line,
             const char *format, ...) attribute_printf(5, 6);

char *cm_strerror(int errnum);



FILE *cm_fopen_bin(const char *path, const char *mode);

FILE *cm_fopen_text(const char *path, const char *mode);

int cm_fseek(FILE *stream, int64_t offset, int whence);

int64_t cm_ftell(FILE *stream);

ssize_t cm_getdelim(char **lineptr, size_t *n, int delim, FILE *stream);

#define cm_getline(lineptr, n, stream) cm_getdelim((lineptr), (n), '\n', (stream))



const cm_option *cm_get_short_option(const cm_option *options, int optid);

const cm_option *cm_get_long_option(const cm_option *options,
                                      const char *optstring);

int cm_parse_option(const cm_option *opt, const char *argument,
                 void *target_parameters);

int cm_parse_argv(int argc, const char * const argv[],
                   const cm_option *options, void *target_parameters);

void cm_show_error_position(FILE *out, int pos, int argc,
                             const char * const argv[]);

void cm_print_options(FILE *out, const cm_option *options,
                       int col1_width, int col2_width);



void *cm_malloc_aligned(size_t size, size_t alignment);

void *cm_realloc_aligned(void *ptr, size_t size, size_t alignment);

int cm_realloc_safe(void **pptr, size_t size, size_t alignment);

int cm_dynamic_array_update_aligned(void **pptr, size_t *cap_elements,
        size_t num_elements, size_t element_size, size_t alignment);

void cm_free(void *ptr);


int64_t cm_timestamp_nano(void);


#ifdef __cplusplus
}
#endif

#endif /* CEMENT_H */

