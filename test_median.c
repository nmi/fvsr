
#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "median.h"
   
/* #define CLOCKTYPE CLOCK_REALTIME */
#define CLOCKTYPE CLOCK_PROCESS_CPUTIME_ID
#define CLOCK_PRECISION 0.000000001

static double timediff(struct timespec *start, struct timespec *end) {
    double tstart;
    double tend;
    tstart = (double) start->tv_sec +
             (double) start->tv_nsec * CLOCK_PRECISION;
    tend   = (double)   end->tv_sec +
             (double)   end->tv_nsec * CLOCK_PRECISION;
    return tend - tstart;
}

#define BUFSIZE 10*1024*1024
#define NVALUES 3
#define NLOOPS  1



static int run_median(const char *description, uint8_t (*median)(uint8_t *p),
        uint8_t *result, uint8_t *buf, size_t bufsize, size_t nvalues) { 
    size_t i, j, k;
    struct timespec tstart, tend;
    clock_gettime(CLOCKTYPE, &tstart);
    for (j = 0; j < NLOOPS; j++) {
        for (i = 0; i < bufsize; i++) {
            uint8_t m = median(buf + i * nvalues);
            if (m != result[i]) {
                fprintf(stderr, "Error: incorrect median value %d (of", m);
                for (k=0; k < nvalues; k++) {
                    fprintf(stderr, " %d", buf[i*nvalues+k]);
                }
                fprintf(stderr, ")\n");
            } else result[i] = m;
        } 
    }
    clock_gettime(CLOCKTYPE, &tend);
    printf("%s: %9f s\n", description, timediff(&tstart, &tend));
    return 0;
}

static int run_median_heap_uint8_t(const char *description,
        uint8_t *result, uint8_t *buf, size_t bufsize, size_t nvalues) { 
    size_t i, j;
    void *median_context;
    struct timespec tstart, tend;
    median_heap_uint8_t_init(nvalues, &median_context);
    clock_gettime(CLOCKTYPE, &tstart);
    for (j = 0; j < NLOOPS; j++) {
        for (i = 0; i < bufsize; i++) {
            uint8_t m = median_heap_uint8_t(buf + i * nvalues, nvalues, &median_context);
/*            if (m != result[i]) {
                fprintf(stderr, "Error: incorrect median value %d (of", m);
                for (k=0; k < nvalues; k++) {
                    fprintf(stderr, " %d", buf[i*nvalues+k]);
                }
                fprintf(stderr, ")\n");
            } else result[i] = m; */
        } 
    }
    clock_gettime(CLOCKTYPE, &tend);
    median_heap_uint8_t_free(&median_context);
    printf("%s: %9f s\n", description, timediff(&tstart, &tend));
    return 0;
}


//int main(int argc, char **argv) {
int main(void) {
    uint8_t *buf, *result;
    size_t   bufsize = BUFSIZE;
    size_t   i, j;
    struct timespec tstart, tend;

    buf = (uint8_t *) malloc(NVALUES * bufsize);
    result = (uint8_t *) malloc(bufsize);
    if ((buf == NULL) || (result == NULL)) {
        free(buf);
        free(result);
        return 1;
    }

    printf("\nGenerating %zu random bytes... ", NVALUES * bufsize);
    fflush(stdout);
    clock_gettime(CLOCKTYPE, &tstart);
    srandom((unsigned int) time(NULL));
    for (i = 0; i < NVALUES * bufsize; i++) {
        long int r = random();
        buf[i] = (uint8_t) lrintf(256.0F * (float) r / ((float) RAND_MAX + 1.0F));
    }
    clock_gettime(CLOCKTYPE, &tend);
    printf("%9f s\n", timediff(&tstart, &tend));


    clock_gettime(CLOCKTYPE, &tstart);
    for (j = 0; j < NLOOPS; j++) {
        for (i = 0; i < bufsize; i++) {
            result[i] = median3_minmax_uint8_t(buf + i * NVALUES);
        } 
    }
    clock_gettime(CLOCKTYPE, &tend);

    printf("\n\nRandom input\n\n");
    run_median("median3_minmax", median3_minmax_uint8_t,
            result, buf, bufsize, NVALUES);
    run_median("median3_branch", median3_branch_uint8_t,
            result, buf, bufsize, NVALUES);
    run_median_heap_uint8_t("median_heap   ",
            result, buf, bufsize, NVALUES);
    run_median("median3_swap  ", median3_swap_uint8_t,
            result, buf, bufsize, NVALUES);

    /* median3_swap orders the input buffer so we can test performance
     * in that condition too */

    printf("\n\nOrdered input\n\n");
    run_median("median3_minmax", median3_minmax_uint8_t,
            result, buf, bufsize, NVALUES);
    run_median("median3_branch", median3_branch_uint8_t,
            result, buf, bufsize, NVALUES);
    run_median_heap_uint8_t("median_heap   ",
            result, buf, bufsize, NVALUES);
    run_median("median3_swap  ", median3_swap_uint8_t,
            result, buf, bufsize, NVALUES);

    printf("\n");

    free(buf);
    free(result);
    return 0;
}

