/**
 * @file      yuv4mpeg.c
 * @brief     YUV4MPEG demuxer/muxer
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright 2-clause BSD
 *
 * @section LICENSE
 *
 * Copyright (c) 2013, Niko Mikkilä
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer
 *    in the documentation and/or other materials provided with
 *    the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "config.h"
#include "cement.h"
#include "fvbuf.h"
#include "fvframe.h"
#include "fvsample.h"
#include "yuv4mpeg.h"

static const char *y4m_sampling_strings[] = {
    [Y4M_SAMPLING_420_JPEG]  = "420jpeg",
    [Y4M_SAMPLING_420_MPEG2] = "420mpeg2",
    [Y4M_SAMPLING_420_PALDV] = "420paldv",
    [Y4M_SAMPLING_411]       = "411",
    [Y4M_SAMPLING_422]       = "422",
    [Y4M_SAMPLING_444]       = "444",
    [Y4M_SAMPLING_444_ALPHA] = "444alpha",
    [Y4M_SAMPLING_LUMA]      = "mono",
    [Y4M_SAMPLING_LAST]      = NULL
};

/*
static y4m_fraction y4m_sampling_factors[] = {
    [Y4M_SAMPLING_420_JPEG]  = {3, 2},
    [Y4M_SAMPLING_420_MPEG2] = {3, 2},
    [Y4M_SAMPLING_420_PALDV] = {3, 2},
    [Y4M_SAMPLING_411]       = {3, 2},
    [Y4M_SAMPLING_422]       = {2, 1},
    [Y4M_SAMPLING_444]       = {3, 1},
    [Y4M_SAMPLING_444_ALPHA] = {4, 1},
    [Y4M_SAMPLING_LUMA]      = {1, 1}
};
*/

static const char *y4m_interlacing_strings[] = {
    [Y4M_INTERLACING_UNKNOWN]= "?",
    [Y4M_INTERLACING_NONE]   = "p",
    [Y4M_INTERLACING_TFF]    = "t",
    [Y4M_INTERLACING_BFF]    = "b",
    [Y4M_INTERLACING_MIXED]  = "m",
    [Y4M_INTERLACING_LAST]   = NULL
};

static const char *y4m_field_delim = " \n";


static int y4m_parse_tag(const char *str,   size_t *len,
                         const char *delim, const char **alternatives) {
    int i = 0, match = 0;
    int ret = -1;
    const char *a, *b = str;
    /* Compare to all given alternatives */
    if (alternatives != NULL) {
        while ((ret < 0) && ((a = alternatives[i]) != NULL)) {
            b = str;
            match = 1;
            while (*a != '\0') {
                if (*(a++) != *(b++)) {
                    match = 0;
                    break;
                }
            }
            if (match) {
                /* Check that the tag really ends here */
                const char *d = delim;
                if (*b == '\0') {
                    ret = i;
                } else {
                    while (*d != '\0') {
                        if (*b == *d) {
                            ret = i;
                            break;
                        }
                        d++;
                    }
                }
            }
            i++;
        }
    }
    /* Skip over the tag to next delimiting character */
    while (!match && (*b != '\0')) {
        const char *d = delim;
        while (*d != '\0') {
            if (*b == *d) {
                match = 1;
                break;
            }
            d++;
        }
        b++;
    }
    *len = (size_t) (b - str - 1);
    return ret;
}


static int y4m_parse_fraction(const char *str, char **endptr,
                              char separator, y4m_fraction *fraction) {
    int num, den;
    num = (int) strtol(str, endptr, 10);
    if (str == *endptr) return -1;
    str = *endptr;
    if (*str != separator) {
        fraction->num = num;
        fraction->den = 1;
        return 0;
    }
    str++;
    den = (int) strtol(str, endptr, 10);
    if (str == *endptr) return -1;
    fraction->num = num;
    fraction->den = den;
    return 0;
}


static int y4m_calculate_frame_size(y4m *v) {
    int i, chroma_shift_x, chroma_shift_y, num_planes;
    if (v->width  == 0) {
        cm_log_warning("Frame width is unspecified or 0");
        return -1;
    }
    if (v->height == 0) {
        cm_log_warning("Frame height is unspecified or 0");
        return -1;
    }
    switch (v->chroma_sampling) {
        case Y4M_SAMPLING_420_JPEG:
            chroma_shift_x = 1;
            chroma_shift_y = 1;
            num_planes     = 3;
            break;
        case Y4M_SAMPLING_420_MPEG2:
            chroma_shift_x = 1;
            chroma_shift_y = 1;
            num_planes     = 3;
            break;
        case Y4M_SAMPLING_420_PALDV:
            chroma_shift_x = 1;
            chroma_shift_y = 1;
            num_planes     = 3;
            break;
        case Y4M_SAMPLING_411:
            chroma_shift_x = 2;
            chroma_shift_y = 0;
            num_planes     = 3;
            break;
        case Y4M_SAMPLING_422:
            chroma_shift_x = 1;
            chroma_shift_y = 0;
            num_planes     = 3;
            break;
        case Y4M_SAMPLING_444:
            chroma_shift_x = 0;
            chroma_shift_y = 0;
            num_planes     = 3;
            break;
        case Y4M_SAMPLING_444_ALPHA:
            chroma_shift_x = 0;
            chroma_shift_y = 0;
            num_planes     = 4;
            break;
        case Y4M_SAMPLING_LUMA:
            chroma_shift_x = 0;
            chroma_shift_y = 0;
            num_planes     = 1;
            break;
        default:
            chroma_shift_x = 1;
            chroma_shift_y = 1;
            num_planes     = 3;
            break;
    }
    v->plane_width[0] = v->width;
    v->plane_height[0] = v->height;
    v->plane_offset[0] = 0;
    v->plane_size[0] = (size_t) v->width * (size_t) v->height;
    v->frame_size = v->plane_size[0];
    for (i = 1; i < num_planes; i++) {
        v->plane_width[i]  = v->width  >> chroma_shift_x;
        v->plane_height[i] = v->height >> chroma_shift_y;
        v->plane_offset[i] = v->frame_size;
        v->plane_size[i]   = (size_t) v->plane_width[i] *
                             (size_t) v->plane_height[i];
        v->frame_size += v->plane_size[i];
    }
    for (i = num_planes; i < 4; i++) {
        v->plane_width[i]  = 0;
        v->plane_height[i] = 0;
        v->plane_offset[i] = v->frame_size;
        v->plane_size[i] = 0;
    }
    return 0;
}


static int y4m_read_header(y4m *v) {
    char   *line = NULL, *line_end, *p;
    char   *metadata_start = NULL;
    size_t  metadata_size = 0;
    size_t  capacity = 0;
    ssize_t len;

    if (cm_fseek(v->stream, 0, SEEK_SET) == -1) {
        cm_log_error("Unable to seek to the beginning of the stream: %s",
                     cm_strerror(errno));
        return -1;
    }
    len = cm_getline(&line, &capacity, v->stream);
    if (len < 0) {
        cm_log_error("Unable to read stream");
        return -1;
    }

    if ((len < 9) || (strncmp(line, "YUV4MPEG2", 9) != 0)) {
        cm_log_error("YUV4MPEG header not found");
        free(line);
        return -1;
    }
    line_end = line + len;
    p = line + 10;
    while (p < line_end) {
        char *np = p + 1;
        size_t taglen;
        int ival;
        switch (*(p++)) {
            case 'W':
                ival = (int) strtol(p, &np, 10);
                if ((p == np) || (ival < 0))
                    cm_log_warning("Invalid width value "
                                   "(at byte %td)", p - line);
                else v->width = (uint32_t) ival;
                break;
            case 'H':
                ival = (int) strtol(p, &np, 10);
                if ((p == np) || (ival < 0))
                    cm_log_warning("Invalid height value "
                                   "(at byte %td)", p - line);
                else v->height = (uint32_t) ival;
                break;
            case 'C':
                ival = y4m_parse_tag(p, &taglen, y4m_field_delim,
                        y4m_sampling_strings);
                np = p + taglen;
                if (ival < 0)
                    cm_log_warning("Unknown chroma subsampling specification "
                                   "(at byte %td)", p - line);
                else v->chroma_sampling = (enum y4m_sampling) ival;
                break;
            case 'I':
                ival = y4m_parse_tag(p, &taglen, y4m_field_delim,
                        y4m_interlacing_strings);
                np = p + taglen;
                if (ival < 0)
                    cm_log_warning("Unknown interlacing specification "
                                   "(at byte %td)", p - line);
                else v->interlacing = (enum y4m_interlacing) ival;
                break;

            case 'F':
                if (y4m_parse_fraction(p, &np, ':', &v->rate) < 0) {
                    cm_log_warning("Invalid framerate specification "
                                   "(at byte %td)", p - line);
                }
                break;
            case 'A':
                if (y4m_parse_fraction(p, &np, ':', &v->aspect) < 0) {
                    cm_log_warning("Invalid aspect ratio specification "
                                   "(at byte %td)", p - line);
                }
                break;
            case 'X':
                if (metadata_start == NULL) metadata_start = p - 1;
                y4m_parse_tag(p, &taglen, y4m_field_delim, NULL);
                np = p + taglen;
                metadata_size += taglen + 2;
                break;
            case ' ':
            case '\t':
            case '\n':
                continue;
            default:
                cm_log_warning("Unexpected character in header "
                               "(at byte %td)", p - line);
                break;
        }
        p = np;
        /* Skip over the tag value */
        while (p < line_end) {
            int stop;
            switch (*p) {
                case ' ':
                case '\n':
                    stop = 1;
                    break;
                default:
                    stop = 0;
                    break;
            }
            p++;
            if (stop) break;
        }
    }

    y4m_calculate_frame_size(v);

    /* Copy metadata tags */
    if (metadata_size > 0) {
        char *m;
        p = metadata_start;
        m = (char *) malloc(metadata_size);
        v->metadata = m;
        if (m == NULL) {
            cm_log_warning("Failed to allocate memory for metadata");
            free(line);
            return 0;
        }
        while (p < line_end) {
            int skip = 0;
            size_t taglen;
            switch (*(p++)) {
                case 'X':
                    if (skip) break;
                    y4m_parse_tag(p, &taglen, y4m_field_delim, NULL);
                    memcpy(m, p - 1, taglen + 1);
                    m += taglen + 1;
                    *m = ' ';
                    m++;
                    p += taglen;
                    break;
                case ' ':
                case '\t':
                case '\n':
                    skip = 0;
                    break;
                default:
                    skip = 1;
                    break;
            }
        }
        *(metadata_start-1) = '\0';
    }
 
    free(line);
    return 0;
}


void y4m_print_info(y4m *v, FILE *out) {
    fprintf(out, "Number of frames:   %zu\n",
            v->index_frames);
    fprintf(out, "Resolution:         %dx%d\n",
            v->width, v->height);
    fprintf(out, "Rate:               %d:%d\n",
            v->rate.num, v->rate.den);
    fprintf(out, "Aspect ratio:       %d:%d\n",
            v->aspect.num, v->aspect.den);
    fprintf(out, "Interlacing:        %s\n",
            y4m_interlacing_strings[v->interlacing]);
    fprintf(out, "Chroma subsampling: %s\n",
            y4m_sampling_strings[v->chroma_sampling]);
    if (v->metadata != NULL) {
        fprintf(out, "Metadata tags:      %s\n", v->metadata);
    }
}


static int y4m_index_add_frame(y4m *v, int64_t header_pos,
                               int64_t frame_pos) {
    size_t index_capacity = v->index_capacity;
    /* Reallocate memory for the index if capacity is exceeded */
    if (v->index_frames >= index_capacity) {
        size_t newsize;
        y4m_index_rec *newindex;
        if (index_capacity < 512) index_capacity = 1024;
        else index_capacity = index_capacity * 2;
        newsize = index_capacity * sizeof(y4m_index_rec);
        newindex = (y4m_index_rec *) realloc(v->index, newsize);
        if (newindex == NULL) {
            cm_log_error("Failed to reallocate index");
            return -1;
        }
        v->index = newindex;
        v->index_capacity = index_capacity;
    }
    if (frame_pos > v->index_last_frame_pos) {
        v->index[v->index_frames].header_pos = header_pos;
        v->index[v->index_frames].frame_pos = frame_pos;
    } else if (frame_pos == v->index_last_frame_pos) {
        /* Already indexed */
        return 0;
    } else {
        /* Use binary search to find the correct position */
        size_t a = 0;
        size_t b = v->index_frames; 
        while (a < b) {
            size_t c = (a + b) >> 1;
            int64_t  cpos = v->index[c].frame_pos;
            if (frame_pos < cpos) b = c;
            else if (frame_pos >= cpos) a = c;
        }
        if (v->index[a].frame_pos == frame_pos) {
            /* Already indexed */
            return 0;
        }
        /* Move rest of the index forward */
        memmove(&v->index[a+1], &v->index[a],
                (v->index_frames - a) * sizeof(y4m_index_rec));
        v->index[a].header_pos = header_pos;
        v->index[a].frame_pos = frame_pos;
    }
    v->index_last_frame_pos = v->index[v->index_frames].frame_pos;
    v->index_frames++;
    return 0; 
}


static int y4m_seek_to_next_frame(y4m *v, int64_t *frame_header,
                                  int64_t *position) {
    static const char *magic = "FRAME";
    int c = 0;
    int state = 0;
    int64_t hstart = *position;

    while ((state >= 0) && ((c = fgetc(v->stream)) != EOF)) {
        (*position)++;
        switch (state) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                if (c == magic[state]) state++;
                break;
            case 5:
                hstart = *position - 5;
                /* Check that the header is valid and skip over it */
                switch (c) {
                    case '\n':
                        state = -1;
                        break; 
                    case ' ':
                        state = 6;
                        break;
                    case EOF:
                        state = 0;
                        break;
                    case 'F':
                        state = 1;
                        break;
                    default:
                        state = 0;
                        cm_log_warning("Invalid frame header at byte %jd "
                                       "(0x%.2x). Skipping to next frame...",
                                       (intmax_t) hstart, c);
                        break;
                }
                break;
            case 6:
                if (c == '\n') state = -1;
                break;
            default:
                cm_log_error("Internal error: invalid state");
                state = 0;
                break;
        }
    }
    if (state < 0) {
        *frame_header = hstart;
        return 0;
    } else {
        if (!feof(v->stream)) {
            cm_log_error("Unable to read stream: %s",
                         cm_strerror(errno));
            return -2;
        }
        return -1;
    }
}


static int y4m_rebuild_index(y4m *v, int seek_to_beginning) {
    char       *line = NULL;
    size_t      capacity = 0;
    size_t      max_frames = 0;
    int64_t     filepos, frame_header;
    struct stat stream_stat;
    int         fd, ret;

    /* Estimate number of frames based on file size. */
    fd = fileno(v->stream);
    if (fstat(fd, &stream_stat) == -1) {
        cm_log_warning("Unable to measure file size: %s",
                       cm_strerror(errno));
    } else if (v->frame_size > 0) {
        max_frames = (size_t) stream_stat.st_size / v->frame_size;
    }
    if (max_frames < 1024) max_frames = 1024;

    /* Reserve space for index */
    if (v->index != NULL) free(v->index);
    v->index_last_frame_pos = 0;
    v->index_frames = 0;
    v->index = (y4m_index_rec *) malloc(max_frames * sizeof(y4m_index_rec));
    if (v->index == NULL) {
        cm_log_error("Failed to allocate memory for frame index");
        return -1;
    }
    v->index_capacity = max_frames;
  
    if (seek_to_beginning) {
        ssize_t len;
        if (cm_fseek(v->stream, 0, SEEK_SET) == -1) {
            cm_log_error("Unable to seek to the beginning of the file: %s",
                         cm_strerror(errno));
            return -1;
        }
        /* Check for header */
        len = cm_getline(&line, &capacity, v->stream);
        if (len < 0) {
            cm_log_error("Unable to read stream");
            return -1;
        }
        if ((len < 9) || (strncmp(line, "YUV4MPEG2", 9) != 0)) {
            cm_log_warning("YUV4MPEG header not found");
            cm_fseek(v->stream, 0, SEEK_SET);
        }
        free(line);
    }

    filepos = cm_ftell(v->stream);
    if (filepos < 0) {
        cm_log_error("Unable to read file position: %s",
                     cm_strerror(errno));
        filepos = 0;
    }

    while ((ret = y4m_seek_to_next_frame(v, &frame_header, &filepos)) >= 0) {
        y4m_index_add_frame(v, frame_header, filepos);
        if (cm_fseek(v->stream, (int64_t) v->frame_size, SEEK_CUR) == -1) {
            cm_log_error("Unable to seek forward: %s",
                         cm_strerror(errno));
        } else {
            filepos += (int64_t) v->frame_size;
        }
    }
    if (ret < -1) return -1;
    else return 0;
}

#if HAVE_Y4M_FVFRAME

static void y4m_FvFrame_init(y4m *v, FvFrame *frame) {
    memset(frame, 0, sizeof(FvFrame));

    if (v->plane_size[0] == 0) return;
    frame->num_planes++;
    FvBuf_init_2d_padded(&frame->plane[0], FVCOMPONENT_Y, FVSAMPLE_U8,
            v->plane_width[0], v->plane_height[0], 0, FVPAD_NONE);

    if ((v->plane_size[1] == 0) || (frame->num_planes >= FVFRAME_MAX_PLANE)) {
        frame->h_subsampling = 1;
        frame->v_subsampling = 1;
        return;
    }
    frame->num_planes++;
    FvBuf_init_2d_padded(&frame->plane[1], FVCOMPONENT_CB, FVSAMPLE_U8,
            v->plane_width[1], v->plane_height[1], 0, FVPAD_NONE);

    frame->h_subsampling = v->plane_width[0]  / v->plane_width[1];
    frame->v_subsampling = v->plane_height[0] / v->plane_height[1];

    if ((v->plane_size[2] == 0) || (frame->num_planes >= FVFRAME_MAX_PLANE)) {
        return;
    }
    frame->num_planes++;
    FvBuf_init_2d_padded(&frame->plane[2], FVCOMPONENT_CR, FVSAMPLE_U8,
            v->plane_width[2], v->plane_height[2], 0, FVPAD_NONE);

    if ((v->plane_size[3] == 0) || (frame->num_planes >= FVFRAME_MAX_PLANE)) {
        return;
    }
    frame->num_planes++;
    FvBuf_init_2d_padded(&frame->plane[3], FVCOMPONENT_A, FVSAMPLE_U8,
            v->plane_width[3], v->plane_height[3], 0, FVPAD_NONE);
}
            
#endif /* HAVE_Y4M_FVFRAME */

int y4m_from_stream(y4m *v, FILE *stream, int build_index) {
    int    ret;
    memset(v, 0, sizeof(y4m));
    v->stream = stream;
    ret = y4m_read_header(v);
    if (ret < 0) {
        cm_log_error("Stream does not appear to be yuv4mpeg");
        return ret;
    }
    if (build_index) {
        y4m_rebuild_index(v, 1);
    }

#if HAVE_Y4M_FVFRAME
    y4m_FvFrame_init(v, &v->tmpframe);
#endif /* HAVE_Y4M_FVFRAME */

    return ret;
}


void y4m_free(y4m *v) {
    free(v->metadata);
    free(v->index);

#if HAVE_Y4M_FVFRAME
    FvFrame_free(&v->tmpframe);
#endif /* HAVE_Y4M_FVFRAME */

    memset(v, 0, sizeof(y4m));
}


int y4m_get_indexed_frame(y4m *v, uint8_t *buf, size_t bufsize, size_t n) { 
    size_t nbytes;
    if (n >= v->index_frames) {
        cm_log_error("Frame %zu is beyond index range (%d, %zu)",
                     n, 0, v->index_frames);
        return -1;
    }
    if (cm_fseek(v->stream, v->index[n].frame_pos, SEEK_SET) == -1) {
        cm_log_error("Unable to seek in stream: %s",
                     cm_strerror(errno));
    }
    nbytes = fread(buf, 1, bufsize, v->stream);
    if (nbytes < bufsize) {
        cm_log_error("Failed to read a complete frame");
        return -1;
    } else if (bufsize < v->frame_size) {
        cm_fseek(v->stream, (int64_t) (v->frame_size - bufsize), SEEK_CUR);
    }
    return 0;
}


int y4m_get_frame(y4m *v, uint8_t *buf, size_t bufsize,
                       int add_to_index) {
    int64_t frame_header = 0, filepos = 0;
    size_t  nbytes;
    int     ret;

    if (bufsize > v->frame_size) {
        bufsize = v->frame_size;
    }

    if (add_to_index) {
        filepos = cm_ftell(v->stream);
        if (filepos < 0) {
            cm_log_error("Unable to read file position: %s",
                         cm_strerror(errno));
            filepos = 0;
        }
    }
    ret = y4m_seek_to_next_frame(v, &frame_header, &filepos);
    if (ret < 0) return -1;
    if (add_to_index) y4m_index_add_frame(v, frame_header, filepos);
    nbytes = fread(buf, 1, bufsize, v->stream);
    if (nbytes < bufsize) {
        cm_log_error("Failed to read a complete frame");
        return -1;
    } else if (bufsize < v->frame_size) {
        cm_fseek(v->stream, (int64_t) (v->frame_size - bufsize), SEEK_CUR);
    }
    return 0;
}

#if HAVE_Y4M_FVFRAME

int y4m_get_fvframe(y4m *v, FvFrame *out, int add_to_index) {
    int64_t  frame_header = 0, filepos = 0;
    int      ret;
    uint32_t plane;
    static uint8_t default_sample[] = {0, 128, 128, 255};

    if (add_to_index) {
        filepos = cm_ftell(v->stream);
        if (filepos < 0) {
            cm_log_error("Unable to read file position: %s",
                         cm_strerror(errno));
            filepos = 0;
        }
    }
    ret = y4m_seek_to_next_frame(v, &frame_header, &filepos);
    if (ret < 0) return -1;
    if (add_to_index) y4m_index_add_frame(v, frame_header, filepos);

    if (out == NULL) {
        cm_fseek(v->stream, (int64_t) v->frame_size, SEEK_CUR);
        return 0;
    }

    for (plane = 0; plane < 4; plane++) {
        FvBuf     *buf;
        FvBinMain *bin;
        uint8_t   *samples;
        if (plane >= out->num_planes) {
            cm_fseek(v->stream, (int64_t) v->plane_size[plane], SEEK_CUR);
            continue;
        }
        if (out->plane[plane].sample_type != FVSAMPLE_U8) {
            buf = &v->tmpframe.plane[plane];
        } else {
            buf = &out->plane[plane];
        }
        bin = FvBuf_access_main(buf, FVBUF_ACCESS_INIT);
        samples = (uint8_t *) bin->mem;
        if (v->plane_size[plane] > 0) {
            size_t w = cm_min(FvBuf_width(buf),  (size_t) v->plane_width[plane]);
            size_t h = cm_min(FvBuf_height(buf), (size_t) v->plane_height[plane]);
            size_t i;
            size_t row = buf->offset;
            for (i = 0; i < h; i++) {
                size_t j;
                size_t   bytes_read;
                bytes_read = fread(samples + row, 1, w, v->stream);
                if (bytes_read < w) ret = -1; 
                /* Fill extra columns in the output frame */
                for (j = w; j < FvBuf_width(buf); j++) {
                    samples[row + j] = default_sample[plane];
                }
                /* Skip over extra columns in input */
                for (j = w; j < v->plane_width[plane]; j++) {
                    getc(v->stream);
                }
                row += FvBuf_row_pitch(buf);
            }
            /* Fill extra rows in the output frame */
            if (h < FvBuf_height(buf)) {
                memset(samples + row, default_sample[plane],
                       buf->offset + (FvBuf_height(buf) - 1) *
                       FvBuf_row_pitch(buf) + (FvBuf_width(buf) <<
                           fvsample_shift[buf->sample_type]) - row);
            }
            /* Skip over extra rows in input */
            for (i = h; i < v->plane_height[plane]; i++) {
                uint32_t j;
                for (j = 0; j < v->plane_width[plane]; j++) {
                    getc(v->stream);
                }
            }
        } else {
            size_t w = FvBuf_width(buf);
            size_t h = FvBuf_height(buf);
            if (h > 0) {
                memset(samples + buf->offset, default_sample[plane],
                       (h - 1) * FvBuf_row_pitch(buf) +
                       (w << fvsample_shift[buf->sample_type]));
            }
        }
        if (out->plane[plane].sample_type != FVSAMPLE_U8) {
            FvBuf_convert(NULL, &out->plane[plane], buf);
        }
    }
    if (ret < 0) {
        cm_log_error("Failed to read a complete frame");
    }
    return ret;
}

#endif /* HAVE_Y4M_FVFRAME */

static int y4m_write_header(y4m *v) {
    cm_fseek(v->stream, 0, SEEK_SET);
    fputs("YUV4MPEG2", v->stream);
    fprintf(v->stream, " W%d", v->width);
    fprintf(v->stream, " H%d", v->height);
    fprintf(v->stream, " C%s", y4m_sampling_strings[v->chroma_sampling]);
    fprintf(v->stream, " I%s", y4m_interlacing_strings[v->interlacing]);
    fprintf(v->stream, " F%d:%d", v->rate.num, v->rate.den);
    fprintf(v->stream, " A%d:%d", v->aspect.num, v->aspect.den);
    if (v->metadata != NULL) {
        fputs(" X", v->stream);
        fputs(v->metadata, v->stream);
    }
    /* Write a newline that ends the header and check if output worked. */
    if (putc('\n', v->stream) == EOF) {
        return -1;
    } else {
        return 0;
    }
}

int y4m_create(y4m *v, FILE *out, uint32_t width, uint32_t height,
               enum y4m_sampling chroma_sampling,
               enum y4m_interlacing interlacing,
               y4m_fraction rate, y4m_fraction aspect, char *metadata) {
    v->stream = out;
    v->width = width;
    v->height = height;
    v->chroma_sampling = chroma_sampling;
    v->interlacing = interlacing;
    v->rate = rate;
    v->aspect = aspect;
    v->metadata = metadata;
    v->index = NULL;
    v->index_capacity = 0;
    v->index_frames = 0;
    v->index_last_frame_pos = 0;
    y4m_calculate_frame_size(v);
    return y4m_write_header(v);
}

int y4m_put_frame(y4m *v, uint8_t *buf, size_t bufsize, int add_to_index) {
    int64_t header_p, data_p;
    if (v->frame_size != bufsize) {
        cm_log_error("Input buffer size (%zu bytes) does not match frame size (%zu bytes) of the YUV4MPEG stream",
                     bufsize, v->frame_size);
        return -1;
    }
    header_p = cm_ftell(v->stream);
    fputs("FRAME\n", v->stream);
    data_p = cm_ftell(v->stream);
    fwrite(buf, 1, bufsize, v->stream);
    if (add_to_index) {
        y4m_index_add_frame(v, header_p, data_p);
    }
    return 0;
}

#if HAVE_Y4M_FVFRAME

int y4m_put_fvframe(y4m *v, FvFrame *frame, int add_to_index) {
    uint32_t plane;
    int64_t header_p, data_p;
    static uint8_t default_sample[] = {0, 128, 128, 255};
    header_p = cm_ftell(v->stream);
    fputs("FRAME\n", v->stream);
    data_p = cm_ftell(v->stream);
    for (plane = 0; plane < 4; plane++) {
        if (v->plane_size[plane] == 0) break;
        if (plane < frame->num_planes) {
            FvBuf     *buf = &frame->plane[plane];
            FvBinMain *bin;
            uint8_t   *samples;
            size_t     w = cm_min(FvBuf_width(buf),
                                  (size_t) v->plane_width[plane]);
            size_t     h = cm_min(FvBuf_height(buf),
                                  (size_t) v->plane_height[plane]);
            size_t     i = 0;
            size_t     row = 0;
            if (frame->plane[plane].sample_type != FVSAMPLE_U8) {
                buf = &v->tmpframe.plane[plane];
                FvBuf_convert(NULL, buf, &frame->plane[plane]);
            } else {
                buf = &frame->plane[plane];
            }
            bin = FvBuf_access_main(buf, FVBUF_ACCESS_READ);
            if (bin != NULL) {
                samples = (uint8_t *) bin->mem + buf->offset;;
                for (i = 0; i < h; i++) {
                    size_t j;
                    fwrite(samples + row, 1, w, v->stream);
                    row += FvBuf_row_pitch(buf);
                    for (j = w; j < v->plane_width[plane]; j++) {
                        fputc(default_sample[plane], v->stream);
                    }
                }
            }
            for (i = h; i < v->plane_height[plane]; i++) {
                uint32_t j;
                for (j = 0; j < v->plane_width[plane]; j++) {
                    fputc(default_sample[plane], v->stream);
                }
            }
        } else {
            size_t i;
            for (i = 0; i < v->plane_size[plane]; i++) {
                fputc(default_sample[plane], v->stream);
            }
        }
    }
    if (add_to_index) {
        y4m_index_add_frame(v, header_p, data_p);
    }
    return 0;
}

#endif /* HAVE_Y4M_FVFRAME */

