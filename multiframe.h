/**
 * @file      multiframe.h
 * @brief     Stacked frame container for super-resolution
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef MULTIFRAME_H
#define MULTIFRAME_H

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "cement.h"
#include "optflow.h"
#include "fvbuf.h"
#include "median.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct Multiplane {
    uint8_t *data;
    size_t   datasize;
    //size_t   unscaled_size;
    size_t   unscaled_width;
    size_t   unscaled_height;
    size_t   width;
    size_t   height;
    uint32_t scale;
    uint32_t stacksize;
    size_t   pitch;
    float    x_factor;
    float    y_factor;
} Multiplane;

typedef struct Multiframe {
    uint8_t *data;
    size_t   datasize;
    size_t   frame_size;
    size_t   unscaled_frame_size;
    Multiplane plane[3];
} Multiframe;


int Multiframe_init(Multiframe *mf, uint32_t stacksize,
        uint32_t luma_scale, size_t luma_width, size_t luma_height,
        uint32_t chroma_scale, size_t chroma_width, size_t chroma_height);

void Multiframe_free(Multiframe *mf);

void Multiframe_clear(Multiframe *mf);

void Multiframe_add_origin(Multiframe *mf, FvFrame *of);

void Multiframe_add_mc(Multiframe *mf, FvFrame *of,
        FvFrame *frame, const OpticalFlow *flow,
        uint32_t mv_average_radius, uint32_t validation_radius,
        uint32_t validation_threshold);

int Multiframe_norm(FvFrame *norm, Multiframe *mf);

int Multiframe_median(FvFrame *out, Multiframe *mf);

void Multiframe_median_linint(FvFrame *out, Multiframe *mf);

//void Multiframe_median_nnedi(FvFrame *out, Multiframe *mf);


#ifdef __cplusplus
}
#endif

#endif /* MULTIFRAME_H */

