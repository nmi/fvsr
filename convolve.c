/**
 * @file      convolve.c
 * @brief     Convolution filters
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "config.h"

#include "cement.h"
#include "fvbuf.h"
#include "fvframe.h"
#include "fvsample.h"

#include "convolve.h" 

#if HAVE_OPENCL

#include <CL/opencl.h>
#include "fvopencl.h" 

/*
static const char *cl_convolve_src =
#include "convolve.cl"
;
*/

#endif

int FvBuf_convolve_2d_c(FvFilterNode *node);

static FvFilterFunction FvBuf_convolve_2d_functions[] = {
    FvBuf_convolve_2d_c,
    //FvBuf_convolve_2d_cl,
    NULL
};

struct FvBufConvolve2dContext {
    /*FvBuf     tmp;*/
    size_t    width;
    size_t    height;
    size_t    kernel_width;
    size_t    kernel_height;
    enum FvSampleT out_sample_type;
    enum FvSampleT in_sample_type;
};


static int FvBuf_convolve_2d_arg_check(FvFilterNode *node) {
    struct FvBufConvolve2dArg *arg =
            (struct FvBufConvolve2dArg *) node->context.arg;
    struct FvBufConvolve2dContext *priv =
            (struct FvBufConvolve2dContext *) node->context.priv;

    size_t width, height, kernel_width, kernel_height;
    enum FvSampleT in_sample_type, out_sample_type;
    int ret = FV_SUCCESS;

    if (arg->out == NULL) {
        cm_log_error("Pointer to output buffer is NULL");
        return FV_INVALID_ARGUMENT;
    }
    if (arg->in == NULL) {
        cm_log_error("Pointer to input buffer is NULL");
        return FV_INVALID_ARGUMENT;
    }
    if (arg->kernel == NULL) {
        cm_log_error("Pointer to kernel is NULL");
        return FV_INVALID_ARGUMENT;
    }
    if (arg->out == arg->in) {
        cm_log_error("Output to the same buffer is not supported");
        return FV_INVALID_ARGUMENT;
    }
    if (arg->kernel->sample_type != FVSAMPLE_F) {
        cm_log_error("Unsupported kernel sample type %s "
                "(supported types: float)",
                fvsample_name[arg->kernel->sample_type]);
        return FV_INVALID_ARGUMENT;
    }
    width  = cm_min(FvBuf_width(arg->out), FvBuf_width(arg->in));
    height = cm_min(FvBuf_height(arg->out), FvBuf_height(arg->in));
    kernel_width  = FvBuf_width(arg->kernel);
    kernel_height = FvBuf_width(arg->kernel);

    in_sample_type  = arg->in->sample_type;
    out_sample_type = arg->out->sample_type;

    /* Check input padding */
    if (FvBuf_check_pad_2d(arg->in, kernel_width >> 1, kernel_height >> 1) < 0) {
        cm_log_error("Input not padded horizontally by %zu samples or "
                "vertically by %zu samples",
                kernel_width >> 1, kernel_height >> 1);
        return FV_INVALID_ARGUMENT;
    }

    if ((priv->width != width) ||
           (priv->height != height)) {
        ret = FVFILTER_CHANGED_ARG;
    }

    /*
    if ((ret == FVFILTER_CHANGED_ARG) && (priv->tmp.size > 0)) {
        FvBuf_free(&priv->tmp);
        priv->tmp.size = 0;
    }
    if ((priv->tmp.size == 0) &&
            (((kernel_width > 0) && (kernel_height > 0)) ||
             (arg->in == arg->out))) {
        if (FvBuf_init_2d_padded(&priv->tmp, arg->in->component_type,
                FVSAMPLE_F, width, height,
                cm_max(kernel_width, kernel_height), arg->in->pad_type) < 0) {
            cm_log_error("Failed to allocate temporary buffer of size %zux%zu",
                    width, height);
            return FV_OUT_OF_MEMORY;
        }
    }
    */

    if ((priv->kernel_width != kernel_width) ||
           (priv->kernel_height != kernel_height) ||
           (priv->out_sample_type != out_sample_type) ||
           (priv->in_sample_type != in_sample_type)) {
        ret = FVFILTER_CHANGED_ARG;
    }

    priv->width = width;
    priv->height = height;
    priv->kernel_width = kernel_width;
    priv->kernel_height = kernel_height;
    priv->out_sample_type = out_sample_type;
    priv->in_sample_type = in_sample_type;
    return ret;
}

static int FvBuf_convolve_2d_process(FvFilterNode *node) {
    return FvFilter_call(node, FvBuf_convolve_2d_functions);
}
 

#if 0
static int FvBuf_convolve_2d_free(FvFilterNode *node) {
    /*
    struct FvBufConvolve2dContext *priv =
            (struct FvBufConvolve2dContext) node->priv;
    FvBuf_free(&priv->tmp);
    */
    return FV_SUCCESS;
}
#endif

int FvBuf_convolve_2d_init(FvFilterNode *node, FvFilterNode *parent) {
    return FvFilterNode_init(node, parent,
            "FvBuf_convolve_2d",
            /*FvBuf_convolve_2d_functions,*/
            sizeof(struct FvBufConvolve2dArg), NULL,
            sizeof(struct FvBufConvolve2dContext), NULL,
            NULL,
            FvBuf_convolve_2d_process,
            FvBuf_convolve_2d_arg_check);
}

int FvBuf_convolve_2d_arg_set(FvFilterNode *node, FvBuf *out, FvBuf *in,
        FvBuf *kernel, int transpose) {
    struct FvBufConvolve2dArg *arg;
    if (node->context.arg_size != sizeof(struct FvBufConvolve2dArg)) {
        cm_log_error("Argument size mismatch");
        return FV_INVALID_ARGUMENT;
    }
    arg = (struct FvBufConvolve2dArg *) node->context.arg;
    arg->out = out;
    arg->in = in;
    arg->kernel = kernel;
    arg->transpose = transpose;
    return FvBuf_convolve_2d_arg_check(node);
}

int FvBuf_convolve_2d_run(FvFilterNode *node) {
/*        FvBuf *out, FvBuf *in, FvBuf *kernel_h, FvBuf *kernel_v) {*/
    //struct FvBufConvolve2dContext *priv;
    return FvFilter_run(node,
           /*"FvBuf_convolve_2d",*/
           /*FvBuf_convolve_2d_functions,*/
           sizeof(struct FvBufConvolve2dArg), NULL,
           sizeof(struct FvBufConvolve2dContext),
           NULL,
           FvBuf_convolve_2d_process,
           FvBuf_convolve_2d_arg_check);
}

int FvBuf_convolve_2d_c(FvFilterNode *node) {
    struct FvBufConvolve2dArg *arg =
            (struct FvBufConvolve2dArg *) node->context.arg;
    struct FvBufConvolve2dContext *priv =
            (struct FvBufConvolve2dContext *) node->context.priv;
    FvBuf *in = arg->in;
    FvBuf *out = arg->out;
    FvBuf *kernel = arg->kernel;
    FvBinMain *out_bin;
    FvBinMain *in_bin;
    FvBinMain *kernel_bin;
    char *out_row, *in_row;
    size_t kernel_x_offset = FvBuf_width(kernel) >> 1;
    size_t kernel_y_offset = FvBuf_height(kernel) >> 1;
    fptr_fvsample_process_array_Ppctn mac;
    size_t i;

    out_bin = FvBuf_access_main(arg->out, FVBUF_ACCESS_WRITE);
    if (out_bin == NULL) {
        return FV_OUT_OF_MEMORY;
    }
    in_bin = FvBuf_access_main(arg->in, FVBUF_ACCESS_READ);
    if (in_bin == NULL) {
        return FV_OUT_OF_MEMORY;
    }
    kernel_bin = FvBuf_access_main(arg->kernel, FVBUF_ACCESS_READ);
    if (kernel_bin == NULL) {
        return FV_OUT_OF_MEMORY;
    }

    mac = fvsample_mac_array_table[out->sample_type][in->sample_type];
    if (mac == NULL) {
        return FV_INVALID_ARGUMENT;
    }
 
    out_row = out_bin->mem + out->offset;
    if (arg->transpose) {
        in_row  =  in_bin->mem +  in->offset -
            kernel_y_offset * FvBuf_sample_size(in) -
            kernel_x_offset * FvBuf_row_pitch(in);
        if (in_row < in_bin->mem) {
            cm_log_error("Insufficient buffer padding");
            return FV_INVALID_ARGUMENT;
        }
        for (i = 0; i < priv->height; i++) {
            size_t j;
            char *in_p = in_row; 
            memset(out_row, 0, priv->width * FvBuf_sample_size(out));
            for (j = 0; j < FvBuf_width(kernel); j++) {
                char *kernel_p = kernel_bin->mem + kernel->offset;
                size_t k;
                for (k = 0; k < FvBuf_height(kernel); k++) {
                    mac(out_row, in_p + k * FvBuf_sample_size(in),
                            kernel_p + j * FvBuf_sample_size(kernel),
                            kernel->sample_type, priv->width);
                    kernel_p += FvBuf_row_pitch(kernel);
                }
                in_p += FvBuf_row_pitch(in); 
            }
            out_row += FvBuf_row_pitch(out);
            in_row  += FvBuf_row_pitch(in);
        }
    } else {
        in_row  =  in_bin->mem +  in->offset -
            kernel_x_offset * FvBuf_sample_size(in) -
            kernel_y_offset * FvBuf_row_pitch(in);
        if (in_row < in_bin->mem) {
            cm_log_error("Insufficient buffer padding");
            return FV_INVALID_ARGUMENT;
        }
        for (i = 0; i < priv->height; i++) {
            char *in_p = in_row; 
            char *kernel_p = kernel_bin->mem + kernel->offset;
            size_t j;
            memset(out_row, 0, priv->width * FvBuf_sample_size(out));
            for (j = 0; j < FvBuf_height(kernel); j++) {
                size_t k;
                for (k = 0; k < FvBuf_width(kernel); k++) {
                    mac(out_row, in_p + k * FvBuf_sample_size(in),
                            kernel_p + k * FvBuf_sample_size(kernel),
                            kernel->sample_type, priv->width);
                }
                kernel_p += FvBuf_row_pitch(kernel);
                in_p += FvBuf_row_pitch(in); 
            }
            out_row += FvBuf_row_pitch(out);
            in_row  += FvBuf_row_pitch(in);
        }
    }
    out->pad_status = 0;
    return 0;
}


struct FvFrameConvolve2dContext {
    size_t       num_planes;
    FvFilterNode plane_filter[FVFRAME_MAX_PLANE];
};

static int FvFrame_convolve_2d_arg_check(FvFilterNode *node) {
    struct FvFrameConvolve2dArg *arg =
            (struct FvFrameConvolve2dArg *) node->context.arg;
    struct FvFrameConvolve2dContext *priv =
            (struct FvFrameConvolve2dContext *) node->context.priv;
    size_t i;
    int err = FV_SUCCESS;
    uint32_t num_planes;
    if ((arg->in == NULL) || (arg->out == NULL)) {
        num_planes = 0;
    } else {
        num_planes = cm_min(arg->in->num_planes, arg->out->num_planes);
        num_planes = cm_min(num_planes, arg->kernel->num_planes); 
        num_planes = cm_min(num_planes, (uint32_t) FVFRAME_MAX_PLANE);
    }
    priv->num_planes = num_planes;
    for (i = 0; i < num_planes; i++) {
        int ret = FvBuf_convolve_2d_arg_set(&priv->plane_filter[i],
                &arg->out->plane[i], &arg->in->plane[i],
                &arg->kernel->plane[i], arg->transpose);
        if ((err == FV_SUCCESS) || (ret < 0)) err = ret;
    }
    return err;
}

static int FvFrame_convolve_2d_process(FvFilterNode *node) {
    struct FvFrameConvolve2dContext *priv =
            (struct FvFrameConvolve2dContext *) node->context.priv;
    size_t i;
    int err = FV_SUCCESS;
    for (i = 0; i < priv->num_planes; i++) {
        int ret = FvBuf_convolve_2d_run(&priv->plane_filter[i]);
        if ((err == FV_SUCCESS) || (ret < 0)) err = ret;
    }
    return err;
}
 
int FvFrame_convolve_2d_init(FvFilterNode *node, FvFilterNode *parent) {
    struct FvFrameConvolve2dContext *priv;
    size_t i;
    int ret;
    ret = FvFilterNode_init(node, parent,
            "FvFrame_convolve_2d",
            /*FvBuf_convolve_2d_functions,*/
            sizeof(struct FvFrameConvolve2dArg), NULL,
            sizeof(struct FvFrameConvolve2dContext), NULL,
            NULL,
            FvFrame_convolve_2d_process,
            FvFrame_convolve_2d_arg_check);
    if (ret < 0) return ret;

    priv = (struct FvFrameConvolve2dContext *) node->context.priv;
    for (i = 0; i < FVFRAME_MAX_PLANE; i++) {
        ret = FvBuf_convolve_2d_init(&priv->plane_filter[i], node);
        if (ret < 0) {
            priv->num_planes = i;
            return ret;
        }
    }
    return FV_SUCCESS;
}

int FvFrame_convolve_2d_arg_set(FvFilterNode *node,
        FvFrame *out, FvFrame *in, FvFrame *kernel, int transpose) {
    struct FvFrameConvolve2dArg *arg;
    if (node->context.arg_size != sizeof(struct FvFrameConvolve2dArg)) {
        cm_log_error("Argument size mismatch");
        return FV_INVALID_ARGUMENT;
    }
    arg = (struct FvFrameConvolve2dArg *) node->context.arg;
    arg->out = out;
    arg->in = in;
    arg->kernel = kernel;
    arg->transpose = transpose;
    return FvFrame_convolve_2d_arg_check(node);
}

int FvFrame_convolve_2d_run(FvFilterNode *node) {
/*        FvBuf *out, FvBuf *in, FvBuf *kernel_h, FvBuf *kernel_v) {*/
    //struct FvBufConvolve2dContext *priv;
    return FvFilter_run(node,
           /*"FvBuf_convolve_2d",*/
           /*FvBuf_convolve_2d_functions,*/
           sizeof(struct FvFrameConvolve2dArg), NULL,
           sizeof(struct FvFrameConvolve2dContext),
           NULL,
           FvFrame_convolve_2d_process,
           FvFrame_convolve_2d_arg_check);
}


#define GAUSSIAN_SAMPLES 10
#define PI 3.14159265358979323846

struct FvBufGaussianKernel1dContext {
    FvBuf *buf;
    float sigma;
};

static int FvBuf_gaussian_kernel_1d_c(FvFilterNode *parent, void *context) {
    struct FvBufGaussianKernel1dContext *ctx =
            (struct FvBufGaussianKernel1dContext *) context;
    FvBuf      *kernel = ctx->buf;
    FvBinMain  *kernel_bin;
    float      *samples;
    float      *center;
    float       norm;
    float       sigma = ctx->sigma;
    float       sample_width = 1.0F / ((float) GAUSSIAN_SAMPLES);
    float       a = 1.0F / (sigma * sqrtf(2.0F * (float) PI));
    size_t      i, radius, size;

    (void) parent;

    if (kernel->sample_type != FVSAMPLE_F) {
        return FV_INVALID_ARGUMENT;
    }
    kernel_bin = FvBuf_access_main(kernel, FVBUF_ACCESS_WRITE);
    if (kernel_bin == NULL) {
        return FV_OUT_OF_MEMORY;
    }
    size = FvBuf_width(kernel);
    radius = ((size + 1) >> 1);
    if (radius == 0) {
        return FV_SUCCESS;
    }
    samples = (float *) (kernel_bin->mem + kernel->offset);
    center = samples + radius - 1;

    norm = 0.0F;
    samples[size-1] = 0.0F;
    for (i = 0; i < radius; i++) {
        int j;
        float x = -0.5F + (float) i;
        float sum = 0.0F;
        for (j = 0; j < GAUSSIAN_SAMPLES; j++) {
            float gaussian = a * expf(-x * x / (2.0F * sigma * sigma));
            sum += gaussian;
            x += sample_width;
        }
        sum *= sample_width;
        *(center + i) = sum;
        *(center - i) = sum;
        norm += sum + sum;
    }
    norm -= *center;
    norm = 1.0F / norm;
    for (i = 0; i < size; i++) {
        samples[i] *= norm;
    }
    return FV_SUCCESS;
}

int FvBuf_gaussian_kernel_1d(FvFilterNode *parent, FvBuf *buf, float sigma) {
    struct FvBufGaussianKernel1dContext ctx;
    ctx.buf = buf;
    ctx.sigma = sigma;
    return FvBuf_gaussian_kernel_1d_c(parent, &ctx);
}

int FvFrame_gaussian_kernel_1d(FvFilterNode *parent, FvFrame *frame,
        float sigma) {
    int ret = FV_SUCCESS;
    int p;
    for (p = 0; p < frame->num_planes; p++) {
        struct FvBufGaussianKernel1dContext ctx;
        int err;
        ctx.buf = &frame->plane[p];
        if (frame->h_subsampling > 1) {
            ctx.sigma = sigma / (float) frame->h_subsampling;
        }
        ctx.sigma = sigma;
        err = FvBuf_gaussian_kernel_1d_c(parent, &ctx);
        if ((ret == FV_SUCCESS) || (err < 0)) ret = err;
    }
    return ret;
}


