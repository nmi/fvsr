#!/bin/bash

CC=gcc

FVSR_SOURCES="cement.c convolve.c fvfilter.c fvsample.c fvbuf.c fvframe.c fvsr.c fvopencl.c fvsrparams.c median.c multiframe.c optflow.c reconst.c yuv4mpeg.c"
FVSR_OBJECTS="cement.o convolve.o fvfilter.o fvsample.o fvbuf.o fvframe.o fvsr.o fvopencl.o fvsrparams.o median.o multiframe.o optflow.o reconst.o yuv4mpeg.o" 

FVBENCH_SOURCES="cement.c convolve.c fvbench.c fvbuf.c fvopencl.c yuv4mpeg.c"
FVBENCH_OBJECTS="cement.o convolve.o fvbench.o fvbuf.o fvopencl.o yuv4mpeg.o"


FVFILTER_INCLUDES="-Ifvfilter/src -Ifvfilter/util"
FVFILTER_LIBS="-Lfvfilter/src/.libs -lfvfilter"

NNEDI_INCLUDES="-Innedi"
NNEDI_LIBS="-Lnnedi -lnnedi"

CWARN="
-Waggregate-return
-Wbad-function-cast
-Wc++-compat
-Wcast-align
-Wcast-qual
-Wchar-subscripts
-Wconversion
-Wdeclaration-after-statement
-Wdisabled-optimization
-Wfloat-equal
-Wformat=2
-Winit-self
-Winline
-Wmissing-declarations
-Wmissing-format-attribute
-Wmissing-include-dirs
-Wmissing-prototypes
-Wnested-externs
-Wno-parentheses
-Wno-switch
-Wno-format-zero-length
-Wno-pointer-sign
-Wold-style-definition
-Wpacked
-Wpointer-arith
-Wredundant-decls
-Wshadow
-Wstack-protector
-Wstrict-aliasing
-Wstrict-overflow=5
-Wstrict-prototypes
-Wswitch-default
-Wtype-limits
-Wundef
-Wunknown-pragmas
-Wunreachable-code
-Wunused-parameter
-Wwrite-strings"

#-Wlogical-op
#-Wunsafe-loop-optimizations

# -Wpadded

# CFLAGS="-Wall $CWARN -O3 -g -std=c99 -D_POSIX_C_SOURCE=200111L"
#CFLAGS="-Wall $CWARN -O3 -g -std=c99 -D_LARGEFILE64_SOURCE"
CFLAGS="-ggdb -fopenmp -Wall $CWARN -O3 -march=native -mtune=native  -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"

rm -f $FVSR_OBJECTS $FVBENCH_OBJECTS fvsr fvbench median
sed -f cl.sed convolve.cl.c > convolve.cl
sed -f cl.sed reconst_1.cl.c > reconst_1.cl
sed -f cl.sed reconst_4.cl.c > reconst_4.cl
#$CC $CFLAGS $FVFILTER_INCLUDES $NNEDI_INCLUDES -c $FVSR_SOURCES
#$CC $CFLAGS $FVSR_OBJECTS $FVFILTER_LIBS $NNEDI_LIBS -lm -lOpenCL -o fvsr
$CC $CFLAGS $NNEDI_INCLUDES -c $FVSR_SOURCES
$CC $CFLAGS $FVSR_OBJECTS $NNEDI_LIBS -lm -lOpenCL -o fvsr

#$CC $CFLAGS $FVFILTER_INCLUDES $NNEDI_INCLUDES -c $FVBENCH_SOURCES
#$CC $CFLAGS $FVBENCH_OBJECTS $FVFILTER_LIBS $NNEDI_LIBS -lm -lOpenCL -o fvbench

#gcc $CFLAGS -march=native -mtune=native -lrt -lm median.c -o median

