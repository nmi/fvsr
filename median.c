
#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "median.h"

#define DEFINE_MEDIAN3_BRANCH(T) \
T median3_branch_##T(T *p) { \
    if (p[0] < p[1]) { \
        if (p[1] < p[2]) return p[1]; \
        else if (p[0] < p[2]) return p[2]; \
        else return p[0]; \
    } else { \
        if (p[0] < p[2]) return p[0]; \
        else if (p[1] < p[2]) return p[2]; \
        else return p[1]; \
    } \
}

#define DEFINE_MEDIAN3_SWAP(T) \
T median3_swap_##T(T *p) { \
    sort2(p[0], p[1]); \
    sort2(p[1], p[2]); \
    sort2(p[0], p[1]); \
    return p[1]; \
}

#define DEFINE_MEDIAN3_MINMAX(T) \
T median3_minmax_##T(T *p) { \
    T max_01 = (T) max2(p[0], p[1]); \
    T min_01 = (T) min2(p[0], p[1]); \
    T candidate = (T) min2(p[2], max_01); \
    return (T) max2(min_01, candidate); \
}

DEFINE_MEDIAN3_BRANCH(uint8_t)
DEFINE_MEDIAN3_SWAP(uint8_t)
DEFINE_MEDIAN3_MINMAX(uint8_t)


#define DEFINE_MAXHEAP(T) \
\
typedef struct maxheap_##T { \
    T      *elements; \
    size_t  capacity; \
    size_t  size; \
} maxheap_##T; \
static int maxheap_##T##_init(maxheap_##T *h, size_t capacity) { \
    T *p = (T *) calloc((capacity+1), sizeof(T)); \
    if (p == NULL) { \
        fprintf(stderr, "Error: failed to allocate memory for a binary heap structure of %zu elements (%zu bytes)", capacity, capacity * sizeof(T)); \
        return -1; \
    } \
    h->elements = p; \
    h->capacity = capacity; \
    h->size = 0; \
    h->elements[1] = 0; \
    return 0; \
} \
static void maxheap_##T##_free(maxheap_##T *h) { \
    free(h->elements); \
    h->elements = NULL; \
    h->capacity = 0; \
    h->size = 0; \
} \
static void maxheap_##T##_clear(maxheap_##T *h) { \
    h->size = 0; \
    h->elements[1] = 0; \
} \
static inline T maxheap_##T##_peek(maxheap_##T *h) { \
    return h->elements[1]; \
} \
static inline T maxheap_##T##_peek_second(maxheap_##T *h) { \
    if (h->size > 2) return (T) max2(h->elements[2], h->elements[3]); \
    else return h->elements[2]; \
} \
static inline int maxheap_##T##_append(maxheap_##T *h, T value) { \
    size_t i; \
    if (h->size >= h->capacity) return -1; \
    h->size++; \
    i = h->size; \
    while (i > 1) { \
        size_t j = i >> 1; \
        if (h->elements[j] < value) { \
            h->elements[i] = h->elements[j]; \
            i = j; \
        } else { \
            break; \
        } \
    } \
    h->elements[i] = value; \
    return 0; \
} \
static inline T maxheap_##T##_insert(maxheap_##T *h, T value) { \
    size_t i; \
    T ret; \
    if ((h->size == 0) || (value >= h->elements[1])) return value; \
    ret = h->elements[1]; \
    i = 1; \
    while (i <= h->size) { \
        size_t j = i << 1; \
        if (j > h->size) { \
            h->elements[i] = value; \
            break; \
        } else if (j < h->size) { \
            if (h->elements[j] < h->elements[j+1]) j++; \
        } \
        if (value < h->elements[j]) { \
            h->elements[i] = h->elements[j]; \
            i = j; \
        } else { \
            h->elements[i] = value; \
            break; \
        } \
    } \
    return ret; \
}

DEFINE_MAXHEAP(uint8_t)


#define DEFINE_MEDIAN_HEAP(T,T2) \
T median_heap_##T(T *p, size_t n, void **priv) { \
    maxheap_##T *heap = NULL; \
    size_t i, half = (n+1) >> 1; \
    T median; \
    switch (n) { \
        case 0: \
            return 0; \
        case 1: \
            return p[0]; \
        case 2: \
            return (uint8_t) (((uint32_t) p[0] + (uint32_t) p[1]) >> 1); \
        default: \
            break; \
    } \
    if (priv != NULL) heap = (maxheap_##T *) *priv; \
    if (heap == NULL) { \
        heap = (maxheap_##T *) calloc(1, sizeof(maxheap_##T)); \
        if (heap == NULL) return 0; \
        maxheap_##T##_init(heap, half); \
    } else if (heap->capacity < half) { \
        size_t cap = max2(half, 2 * heap->capacity); \
        maxheap_##T##_free(heap); \
        maxheap_##T##_init(heap, cap); \
    } else { \
        maxheap_##T##_clear(heap); \
    } \
    if (priv != NULL) *priv = heap; \
    for (i = 0; i < half; i++) { \
        maxheap_##T##_append(heap, p[i]); \
    } \
    for(; i < n; i++) { \
        maxheap_##T##_insert(heap, p[i]); \
    } \
    median = maxheap_##T##_peek(heap); \
    if ((n & 1) == 0) { \
        median = (T) (((T2) median + (T2) maxheap_##T##_peek_second(heap)) / 2); \
    } \
    if (priv == NULL) free(heap); \
    return median; \
} \
int median_heap_##T##_init(size_t n, void **priv) { \
    size_t half = (n+1) >> 1; \
    maxheap_##T *heap; \
    if (priv == NULL) return -1; \
    heap = (maxheap_##T *) calloc(1, sizeof(maxheap_##T)); \
    if (heap == NULL) return -1; \
    maxheap_##T##_init(heap, half); \
    *priv = heap; \
    return 0; \
} \
void median_heap_##T##_free(void **priv) { \
    maxheap_##T *heap; \
    if (priv == NULL) return; \
    heap = (maxheap_##T *) *priv; \
    maxheap_##T##_free(heap); \
    free(heap); \
    *priv = NULL; \
}
 
DEFINE_MEDIAN_HEAP(uint8_t, uint32_t)


/*
int quickselect(T *p, int l, int r, int pivot) {
    
}
*/

