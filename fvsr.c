/**
 * @file      fvsr.c
 * @brief     Forevid Super-resolution filter
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <omp.h>

#include "fvsr.h"

#include "fvbuf.h"
#include "fvframe.h"
#include "fvopencl.h"
#include "reconst.h"
#include "yuv4mpeg.h"

#include "nnedi.h"



static void FvFrame_upscale_nnedi(nnedi_t *cfg, FvFrame *out, FvFrame *in,
        uint32_t scale) {
    uint32_t num_planes = cm_min(out->num_planes, in->num_planes);
    uint32_t p = 0;
    int width[4];
    int height[4];
    for (p = 0; p < num_planes; p++) {
        FvBuf     *in_plane  = &in->plane[p];
        FvBuf     *out_plane = &out->plane[p];
        FvBinMain *in_bin    = FvBuf_access_main(in_plane,  FVBUF_ACCESS_READ);
        FvBinMain *out_bin   = FvBuf_access_main(out_plane, FVBUF_ACCESS_WRITE);
        int in_pitch  = (int)  in_plane->pitch[FVBUF_ROW_PITCH];
        int out_pitch = (int) out_plane->pitch[FVBUF_ROW_PITCH];
        if (in_bin == NULL) {
            cm_log_error("Failed to access input image data of plane %d", p);
            continue;
        }
        if (out_bin == NULL) {
            cm_log_error("Failed to access output image data of plane %d", p);
            continue;
        }
        width[p]  = (int) in_plane->region[0];
        height[p] = (int) in_plane->region[1];
        nnedi_upscale_2x(cfg, out_bin->mem + out_plane->offset,
                in_bin->mem + in_plane->offset, width[p], height[p],
                out_pitch, in_pitch);
    }
    scale /= 2;
    while (scale > 1) {
        for (p = 0; p < num_planes; p++) {
            FvBuf     *plane = &out->plane[p];
            FvBinMain *bin   = FvBuf_access_main(plane, FVBUF_ACCESS_WRITE);
            int pitch = (int) plane->pitch[FVBUF_ROW_PITCH];
            width[p]  *= 2;
            height[p] *= 2;
            nnedi_upscale_2x(cfg, bin->mem + plane->offset,
                bin->mem + plane->offset, width[p], height[p], pitch, pitch);
        }
        scale /= 2;
    }
}

static void FvFrame_upscale_chroma_nnedi(nnedi_t *cfg, FvFrame *frame) {
    uint32_t p = 0;
    for (p = 1; p < frame->num_planes; p++) {
        FvBuf   *plane = &frame->plane[p];
        FvBinMain *bin = FvBuf_access_main(plane, FVBUF_ACCESS_WRITE);
        int pitch = (int) plane->pitch[FVBUF_ROW_PITCH];
        nnedi_upscale_2x(cfg, bin->mem + plane->offset,
                bin->mem + plane->offset,
                (int) plane->region[0] >> 1, (int) plane->region[1] >> 1,
                pitch, pitch);
    }
}



int FvFrameSeq_init(FvFrameSeq *fs, size_t size, FvFrame *parameters) {
    size_t i;
    fs->frames = (FvFrame *) calloc(size, sizeof(FvFrame));
    if (fs->frames == NULL) {
        cm_log_error("Failed to allocate memory for a frame sequence");
        return -1;
    }
    if (parameters != NULL) {
        for (i = 0; i < size; i++) {
            if (FvFrame_init_duplicate(&fs->frames[i], parameters, 0) < 0) {
                size_t j;
                cm_log_error("Failed to allocate sequence frames");
                for (j = 0; j < i; j++) {
                    FvFrame_free(&fs->frames[j]);
                }
                return -1;
            }
        }
    }
    fs->size = size;
    return 0;
}

void FvFrameSeq_free(FvFrameSeq *fs) {
    size_t i;
    for (i = 0; i < fs->size; i++) {
        FvFrame_free(&fs->frames[i]);
    }
    free(fs->frames);
    memset(fs, 0, sizeof(FvFrameSeq));
}

void FvFrameSeq_roll(FvFrameSeq *fs) {
    size_t i;
    FvFrame tmp = fs->frames[0]; 
    for (i = 0; i < fs->size - 1; i++) {
        fs->frames[i] = fs->frames[i + 1];
    }
    fs->frames[i] = tmp;
}

#define FrameSeq_last(fs) (fs)->frames[(fs)->size - 1]


int FvSrThread_init(FvSrThread *tc, FvSrContext *sc) {
    memset(tc, 0, sizeof(FvSrThread));
    tc->shared_context = sc;

    /* Reserve memory for a Multiframe */
    if (Multiframe_init(&tc->multiframe,
                2 * (sc->params->sr_radius * 2 + 1),
                sc->params->sr_scale,
                sc->y4m_input.width, sc->y4m_input.height,
                sc->y4m_output.plane_width[1] /
                        cm_max((uint32_t) 1, sc->y4m_input.plane_width[1]),
                sc->y4m_input.plane_width[1],
                sc->y4m_input.plane_height[1]) < 0) {
        goto multiframe_error_exit;
    }

    if (FvFrame_init_padded(&tc->hrframe, FVSAMPLE_U8,
            sc->y4m_output.width, sc->y4m_output.height, 3, 1, 1,
            sc->pad, sc->pad, FVPAD_MIRROR) < 0) {
        cm_log_error("Failed to allocate a %dx%d output frame ",
                sc->y4m_output.width, sc->y4m_output.height);
        goto hrframe_error_exit;
    }

    if (FvFrame_init_padded(&tc->norm, FVSAMPLE_F,
            sc->y4m_output.width, sc->y4m_output.height, 3, 1, 1,
            0, 0, FVPAD_NONE) < 0) {
        cm_log_error("Failed to allocate normalization frame (%ux%u)",
                sc->y4m_output.width, sc->y4m_output.height);
        goto norm_error_exit;
    }
    /*
    if (FvFrame_init_with_padding(&tmpframe, FV_TYPE_UINT8,
            (uint32_t) output.width, (uint32_t) output.height, 3,
            2, 2, 0, 0, FV_PADDING_NONE) < 0) {
        cm_log_error("Failed to allocate a %dx%d temporary frame ",
                output.width, output.height);
        FvFrame_free(&hrframe);
        goto tmpframe_error_exit;
    }
    */
    if (ReconstState_init(&tc->rcstate, &tc->hrframe,
            &sc->psf_h, &sc->psf_v, sc->params->reg_radius,
            sc->params->reg_decay, sc->params->reg_factor,
            sc->params->desc_start, sc->params->desc_factor,
            sc->cl_context, sc->num_channels) < 0) {
        cm_log_error("Failed to initialize reconstruction algorithm");
        goto rcstate_error_exit;
    }

    return 0;

rcstate_error_exit:

    FvFrame_free(&tc->norm);
norm_error_exit:

    FvFrame_free(&tc->hrframe);
hrframe_error_exit:
 
    Multiframe_free(&tc->multiframe);
multiframe_error_exit:

    return -1;
}

void FvSrThread_free(FvSrThread *tc) {
    Multiframe_free(&tc->multiframe);
    OpticalFlow_free(&tc->flow);
    ReconstState_free(&tc->rcstate);
    FvFrame_free(&tc->norm);
    FvFrame_free(&tc->hrframe);
    /*FvFrame_free(&tc->tmpframe);*/
    /*FvBuf_free(&tc->tmpbuf);*/
}


int FvSr_init(FvSrContext *sc, FvSrParams *params) {
    int i;
    uint32_t seq_w, seq_h;
    FvFrame input_params;

    memset(sc, 0, sizeof(FvSrContext));

    sc->params = params;
    sc->num_threads = omp_get_max_threads();

    sc->pad = cm_max(params->psf_radius_h, params->psf_radius_v);

    /* Initialize OpenCL */
   
    if (params->opencl) { 
        sc->cl_context = (FvClContext *) malloc(sizeof(FvClContext));
        if (sc->cl_context == NULL) {
            cm_log_error("Failed to allocate memory for FvClContext");
            return -1;
        }
        FvClContext_init(sc->cl_context, params->opencl_platform,
                params->opencl_device);
    } else {
        sc->cl_context = NULL;
    }
    if (sc->cl_context != NULL) {
        sc->num_channels = params->num_channels;
    } else {
        sc->num_channels = 1;
    }
    sc->work_unit.size = (size_t) sc->num_channels * (size_t) sc->num_threads * 24;
 
    /* Open input file */
    sc->input_file = cm_fopen_bin(params->files[0], "r");
    if (sc->input_file == NULL) {
        cm_log_error("Unable to open input file \"%s\" (%s)",
                     params->files[0], cm_strerror(errno));
        goto error_exit;
    }
    if (y4m_from_stream(&sc->y4m_input, sc->input_file, 0) < 0) {
        goto y4m_input_error_exit;
    }

    /* Create an output file */
    if ((params->output[0] == '-') && (params->output[1] == '\0')) {
        sc->output_file = stdout;
    } else {
        sc->output_file = cm_fopen_bin(params->output, "w+");
    }
    if (sc->output_file == NULL) {
        cm_log_error("Unable to open output file \"%s\" (%s)",
                     params->output, cm_strerror(errno));
        goto output_error_exit;
    }
    if (y4m_create(&sc->y4m_output, sc->output_file,
                   sc->y4m_input.width  * params->sr_scale,
                   sc->y4m_input.height * params->sr_scale,
                   Y4M_SAMPLING_444, Y4M_INTERLACING_NONE,
                   sc->y4m_input.rate, sc->y4m_input.aspect,
                   sc->y4m_input.metadata) < 0) {
        cm_log_error("Failed to write Y4M header to output file \"%s\"",
                params->output);
        goto y4m_output_error_exit;
    }

    /* Initialize nnedi upscaling */
    sc->nnedi_cfg = nnedi_config(0, 0);


    /* Create point spread functions */
    if (FvFrame_init_padded(&sc->psf_h, FVSAMPLE_F,
            sc->params->psf_radius_h * 2 + 1, 1,
            3, 1, 1, 0, 0, FVPAD_NONE) < 0) {
        cm_log_error("Failed to initialize horizontal PSF");
        goto psf_h_init_error_exit;
    }
    if (FvFrame_gaussian_kernel_1d(NULL, &sc->psf_h, sc->params->psf_sigma_h) < 0) {
        cm_log_error("Failed to create horizontal PSF");
        goto psf_h_error_exit;
    }
    if (FvFrame_init_padded(&sc->psf_v, FVSAMPLE_F,
            sc->params->psf_radius_v * 2 + 1, 1,
            3, 1, 1, 0, 0, FVPAD_NONE) < 0) {
        cm_log_error("Failed to initialize horizontal PSF");
        goto psf_v_init_error_exit;
    }
    if (FvFrame_gaussian_kernel_1d(NULL, &sc->psf_v, sc->params->psf_sigma_v) < 0) {
        cm_log_error("Failed to create vertical PSF");
        goto psf_v_error_exit;
    }

    /* Create a rolling buffer for frames */

    if (sc->params->interpolation == FVSR_INTERPOLATE_INPUT) {
        seq_w = sc->y4m_output.width;
        seq_h = sc->y4m_output.height;
    } else {
        seq_w = sc->y4m_input.width;
        seq_h = sc->y4m_input.height;
    }
    if (FvFrame_init_padded(&input_params, FVSAMPLE_U8, seq_w, seq_h,
            3, 1, 1, sc->pad, sc->pad, FVPAD_MIRROR) < 0) {
        cm_log_error("Failed to initialize input frame buffer");
        goto input_seq_error_exit;
    }
    if (FvFrameSeq_init(&sc->input_seq,
            2 * (size_t) params->sr_radius + (size_t) sc->work_unit.size,
            &input_params) < 0) {
        goto input_seq_error_exit;
    }


    /* Allocate work unit output frames */
    sc->work_unit.frames = (FvFrame *)
            malloc(sc->work_unit.size * sizeof(FvFrame));
    if (sc->work_unit.frames == NULL) {
        cm_log_error("Failed to allocate work unit");
        goto work_unit_error_exit;
    }
    for (i = 0; i < sc->work_unit.size; i++) {
       if (FvFrame_init_padded(&sc->work_unit.frames[i], FVSAMPLE_U8,
                sc->y4m_output.width, sc->y4m_output.height, 3, 1, 1,
                sc->pad, sc->pad, FVPAD_MIRROR) < 0) {
            cm_log_error("Failed to allocate work unit output frames");
            for (i--; i >= 0; i--) {
                FvFrame_free(&sc->work_unit.frames[i]);
            }
            goto work_unit_frames_error_exit;
        }
    }

    /* Initialize thread contexts */
    sc->thread_context = (FvSrThread *) calloc((size_t) sc->num_threads,
            sizeof(FvSrThread));
    if (sc->thread_context == NULL) {
        cm_log_error("Failed to allocate memory for thread context");
        goto threads_error_exit;
    }
    for (i = 0; i < sc->num_threads; i++) {
        fprintf(stderr, "Initializing thread %d\n", i); 
        if (FvSrThread_init(&sc->thread_context[i], sc) < 0) {
            cm_log_error("Failed to initialize thread %d/%d", i,
                    sc->num_threads);
            for (i--; i >= 0; i--) {
                FvSrThread_free(&sc->thread_context[i]);
            }
            goto thread_init_error_exit;
        }
    }

    return 0;

thread_init_error_exit:

    free(sc->thread_context);
threads_error_exit:

    for (i=0; i < sc->work_unit.size; i++) {
        FvFrame_free(&sc->work_unit.frames[i]);
    }
work_unit_frames_error_exit:

    free(sc->work_unit.frames);
work_unit_error_exit:

    FvFrameSeq_free(&sc->input_seq);
input_seq_error_exit:

psf_v_error_exit:
    FvFrame_free(&sc->psf_v);
psf_v_init_error_exit:

psf_h_error_exit:
    FvFrame_free(&sc->psf_h);
psf_h_init_error_exit:

    y4m_free(&sc->y4m_output);
y4m_output_error_exit:

    fclose(sc->output_file);
output_error_exit:

    y4m_free(&sc->y4m_input);
y4m_input_error_exit:

    fclose(sc->input_file);
error_exit:

    free(sc->cl_context);

    return -1;
}

void FvSr_free(FvSrContext *sc) {
    int i;
    for (i = 0; i < sc->num_threads; i++) {
        FvSrThread_free(&sc->thread_context[i]);
    }
    free(sc->thread_context);

    for (i=0; i < sc->work_unit.size; i++) {
        FvFrame_free(&sc->work_unit.frames[i]);
    }
    free(sc->work_unit.frames);

    FvFrameSeq_free(&sc->input_seq);
    FvFrame_free(&sc->psf_h);
    FvFrame_free(&sc->psf_v);

    if (sc->nnedi_cfg != NULL) free(sc->nnedi_cfg);

    y4m_free(&sc->y4m_input);
    y4m_free(&sc->y4m_output);
    fclose(sc->input_file);
    fclose(sc->output_file);
    free(sc->cl_context);
}

static void FvFrame_pack_channel_f_to_f(FvFrame *dstframe, FvFrame *srcchannel,
        size_t num_channels, size_t offset) {
    uint32_t p;
    for (p = 0; p < srcchannel->num_planes; p++) {
        FvBuf *srcbuf = &srcchannel->plane[p];
        FvBuf *dstbuf = &dstframe->plane[p];
        FvBinMain *srcbin = FvBuf_access_main(srcbuf, FVBUF_ACCESS_READ);
        FvBinMain *dstbin = FvBuf_access_main(dstbuf, FVBUF_ACCESS_WRITE);
        float *src = (float *) (srcbin->mem + srcbuf->offset);
        float *dst = (float *) (dstbin->mem + dstbuf->offset);
        size_t row;
        for (row = 0; row < FvBuf_height(srcbuf); row++) {
            size_t col;
            for (col = 0; col < FvBuf_width(srcbuf); col++) {
                dst[col*num_channels+offset] = src[col];
            }
            src += FvBuf_row_pitch_samples(srcbuf);
            dst += FvBuf_row_pitch_samples(dstbuf);
        }
    }
}

#define U8_TO_F (1.0F / 256.0F)
static void FvFrame_pack_channel_u8_to_f(FvFrame *dstframe, FvFrame *srcchannel,
        size_t num_channels, size_t offset) {
    uint32_t p;
    for (p = 0; p < srcchannel->num_planes; p++) {
        FvBuf *srcbuf = &srcchannel->plane[p];
        FvBuf *dstbuf = &dstframe->plane[p];
        FvBinMain *srcbin = FvBuf_access_main(srcbuf, FVBUF_ACCESS_READ);
        FvBinMain *dstbin = FvBuf_access_main(dstbuf, FVBUF_ACCESS_WRITE);
        uint8_t *src = (uint8_t *) (srcbin->mem + srcbuf->offset);
        float *dst = (float *) (dstbin->mem + dstbuf->offset);
        size_t row;
        for (row = 0; row < FvBuf_height(srcbuf); row++) {
            size_t col;
            for (col = 0; col < FvBuf_width(srcbuf); col++) {
                dst[col*num_channels+offset] = ((float) src[col]) * U8_TO_F;
            }
            src += FvBuf_row_pitch_samples(srcbuf);
            dst += FvBuf_row_pitch_samples(dstbuf);
        }
    }
}


static void FvFrame_unpack_channel_f_to_u8(FvFrame *dstchannel, FvFrame *srcframe,
        size_t num_channels, size_t offset) {
    uint32_t p;
    for (p = 0; p < srcframe->num_planes; p++) {
        FvBuf *srcbuf = &srcframe->plane[p];
        FvBuf *dstbuf = &dstchannel->plane[p];
        FvBinMain *srcbin = FvBuf_access_main(srcbuf, FVBUF_ACCESS_READ);
        FvBinMain *dstbin = FvBuf_access_main(dstbuf, FVBUF_ACCESS_WRITE);
        float *src = (float *) (srcbin->mem + srcbuf->offset);
        uint8_t *dst = (uint8_t *) (dstbin->mem + dstbuf->offset);
        size_t row;
        for (row = 0; row < FvBuf_height(dstbuf); row++) {
            size_t col;
            for (col = 0; col < FvBuf_width(dstbuf); col++) {
                float val = src[col*num_channels+offset];
                val = 256.0F * cm_min(1.0F, cm_max(0.0F, val));
                dst[col] = (uint8_t) cm_min((uint32_t) 255, (uint32_t) val);
            }
            src += FvBuf_row_pitch_samples(srcbuf);
            dst += FvBuf_row_pitch_samples(dstbuf);
        }
    }
}



void FvSrThread_process(FvSrThread *tc, uint32_t wn, int fn, int seq_center, uint32_t channels) {
    int verbose_logs = (cm_get_log_level() >= CM_LOG_LEVEL_DEBUG) ? 1 : 0;
    int r;
    uint32_t i;
    FvSrContext *sc = tc->shared_context;
    ReconstState *rc = &tc->rcstate;
    for (i = 0; i < channels; i++, fn++, seq_center++) {
        FvFrame *origin = &sc->input_seq.frames[seq_center];
        FvFrame *output = &sc->work_unit.frames[wn+i];
        int fn_ref, seq_pos;

        FvFrame_zero(NULL, output);
        Multiframe_clear(&tc->multiframe);

        if (sc->params->interpolation == FVSR_INTERPOLATE_OUTPUT) {
                FvFrame_upscale_nnedi(sc->nnedi_cfg, output,
                        origin, sc->params->sr_scale);
        }
        /*
        if (sc->params->interpolation == FVSR_INTERPOLATE_OUTPUT) {
                //FvFrame_upscale_nnedi(sc->nnedi_cfg, output,
                //        origin, sc->params->sr_scale);
        } else if (sc->params->interpolation == FVSR_INTERPOLATE_INPUT) {
            //FvFrame_convert(output, origin);
        }
        */

        Multiframe_add_origin(&tc->multiframe, origin);
        
        fn_ref = fn - (int) sc->params->sr_radius;
        seq_pos = seq_center - (int) sc->params->sr_radius;

        for (r = 0; r < sc->params->sr_radius; r++, fn_ref++, seq_pos++) {
            if (verbose_logs) {
                fprintf(stderr, "Frame %4d,   ref %3d,   ", fn, r+1);
            }
            if ((fn_ref >= 0) &&
                    (OpticalFlow_from_template(&tc->flow,
                            sc->params->flow_template,
                            (size_t) fn, (size_t) fn_ref,
                            sc->params->flow_template_reverse_params,
                            verbose_logs) >= 0)) {
                Multiframe_add_mc(&tc->multiframe, origin,
                        &sc->input_seq.frames[seq_pos],
                        &tc->flow,
                        sc->params->mv_average_radius,
                        sc->params->validation_radius,
                        sc->params->validation_threshold);
            } else if (verbose_logs) {
                fprintf(stderr, "not available\n");
            }
        }

        fn_ref = fn + 1;
        seq_pos = seq_center + 1;
        for (r = 0; r < sc->params->sr_radius; r++, fn_ref++, seq_pos++) {
            if (verbose_logs) {
                fprintf(stderr, "Frame %4d,   ref %3d,   ",
                        fn, (int) sc->params->sr_radius + r + 1);
            }
            if (OpticalFlow_from_template(&tc->flow,
                        sc->params->flow_template,
                        (size_t) fn, (size_t) fn_ref,
                        sc->params->flow_template_reverse_params,
                        verbose_logs) >= 0) {
                Multiframe_add_mc(&tc->multiframe, origin,
                        &sc->input_seq.frames[seq_pos],
                        &tc->flow,
                        sc->params->mv_average_radius,
                        sc->params->validation_radius,
                        sc->params->validation_threshold);
            } else {
                if (verbose_logs) {
                    fprintf(stderr, "not available\n");
                }
            }
        }

        Multiframe_median(output, &tc->multiframe);
        /*
        if (sc->params->interpolation == FVSR_INTERPOLATE_NONE) {
            Multiframe_median(output, &tc->multiframe);
            //Multiframe_median_linint(output, &tc->multiframe);
        } else {
            Multiframe_median(output, &tc->multiframe);
            //Multiframe_median_linint(output, &tc->multiframe);
            //Multiframe_median_nnedi(output, &tc->multiframe);
        }
        */
        if (sc->num_channels > 1) {
            Multiframe_norm(&tc->norm, &tc->multiframe);
            FvFrame_pack_channel_u8_to_f(&rc->hres_median, output, sc->num_channels, i);
            FvFrame_pack_channel_f_to_f(&rc->hres_norm, &tc->norm, sc->num_channels, i);
        } else {
            Multiframe_norm(&rc->hres_norm, &tc->multiframe);
            FvFrame_convert(NULL, &rc->hres_median, output);
        }
    }

        ReconstState_start(&tc->rcstate);
        for (r = 0; r < sc->params->iterations; r++) {
            if (verbose_logs) {
                fprintf(stderr, "Frame %4d, iteration %3d/%-3d\r",
                        fn, r+1, sc->params->iterations);
            }
            ReconstState_step(&tc->rcstate);
        }
       
        if (verbose_logs) {
            fprintf(stderr, "\n");
        }
        if (sc->num_channels > 1) {
            for (i = 0; i < channels; i++) {
                FvFrame *output = &sc->work_unit.frames[wn+i];
                FvFrame_unpack_channel_f_to_u8(output, &rc->hres_deblurred, sc->num_channels, i);
            }
        } else {
            FvFrame *output = &sc->work_unit.frames[wn];
            if (FvFrame_convert(NULL, output, &tc->rcstate.hres_deblurred)
                    != FV_SUCCESS) {
                cm_log_error("Failed to convert output frame");
            }
        }
}

void FvSr_process(FvSrContext *sc) {
    FvFrame input_lowres;
    int      n = sc->params->flow_offset;
    uint32_t frames_left;
    uint32_t i;
    while (n < 0) {
        if (y4m_get_fvframe(&sc->y4m_input, NULL, 1) < 0) {
            cm_log_warning("Failed to skip over a frame in yuv4mpeg input");
        }
        n++;
    }

    frames_left = 1;
    for (i = 0; i < sc->params->sr_radius; i++) {
        FvFrame_zero(NULL, &sc->input_seq.frames[i]);
    }
    for (; i < sc->params->sr_radius * 2 + (uint32_t) sc->work_unit.size; i++) {
        FvFrame *frame = &sc->input_seq.frames[i];
        //FvFrame_access_main(frame, 1);
        if (y4m_get_fvframe(&sc->y4m_input, frame, 1) >= 0) {
            if ((sc->y4m_input.plane_width[1] < sc->y4m_input.plane_width[0]) &&
                (sc->y4m_input.plane_height[1] < sc->y4m_input.plane_height[0])) {
                FvFrame_upscale_chroma_nnedi(sc->nnedi_cfg, frame);
            }

            if (sc->params->interpolation == FVSR_INTERPOLATE_INPUT) {
                FvFrame_view(&input_lowres, frame, 0, 0,
                        sc->y4m_input.width, sc->y4m_input.height);
                FvFrame_upscale_nnedi(sc->nnedi_cfg, frame,
                        &input_lowres, sc->params->sr_scale);
            }
            frames_left++;

        } else {
            FvFrame_zero(NULL, frame);
        }
    }
    while ((frames_left > 0) /*&& (n < 48)*/) {
        uint32_t num_frames = cm_min((uint32_t) sc->work_unit.size, frames_left);
        uint32_t num_groups = num_frames / sc->num_channels;
        if (num_frames % sc->num_channels > 0) num_groups++;
        //if (200 - n < num_frames) num_frames = 200 - n;
        #pragma omp parallel for private(i) shared(n)
        for (i = 0; i < num_groups; i++) {
            uint32_t channels = cm_min(sc->num_channels, num_frames - (i * sc->num_channels));
            int tid = omp_get_thread_num();
            FvSrThread *tc = &sc->thread_context[tid];
        //fprintf(stderr, "processing frame: %d\n", n+fn);
            uint32_t fn = i * sc->num_channels;
            FvSrThread_process(tc, fn, n + (int) fn,
                    (int) (sc->params->sr_radius + fn), channels);
        //fprintf(stderr, "done frame: %d\n", n+fn);
        }
        for (i = 0; i < num_frames; i++) {
            //FvSrThread *tc = &sc->thread_context[i];
            FvFrame *nextFrame;
            y4m_put_fvframe(&sc->y4m_output, &sc->work_unit.frames[i], 0);
            FvFrameSeq_roll(&sc->input_seq);
            nextFrame = FvFrameSeq_last(&sc->input_seq);
            if (y4m_get_fvframe(&sc->y4m_input, nextFrame, 1) >= 0) {
                if ((sc->y4m_input.plane_width[1] < sc->y4m_input.plane_width[0]) &&
                    (sc->y4m_input.plane_height[1] < sc->y4m_input.plane_height[0])) {
                    FvFrame_upscale_chroma_nnedi(sc->nnedi_cfg, nextFrame);
                }

                if (sc->params->interpolation == FVSR_INTERPOLATE_INPUT) {
                    FvFrame_view(&input_lowres, nextFrame, 0, 0,
                            sc->y4m_input.width, sc->y4m_input.height);
                    FvFrame_upscale_nnedi(sc->nnedi_cfg, nextFrame,
                            &input_lowres, sc->params->sr_scale);
                }
            } else {
                frames_left--;
                FvFrame_zero(NULL, nextFrame);
            }
        }
        n += (int) num_frames;
    }
}


int main(int argc, const char * const *argv) {
    FvSrContext context;
    FvSrParams  params;
    int ret;

    FvSrParams_init(&params, argc);

    ret = FvSrParams_parse(&params, argc, argv);
    if (ret < 0) {
        if (params.show_usage) {
            return 0;
        } else {
            cm_log_error("Failed to parse command line parameters");
            return 1;
        }
    }

    if (FvSr_init(&context, &params) < 0) {
        return 1;
    }

    FvSr_process(&context);
 
    fprintf(stderr, "Input file %s:\n", params.files[0]);
    y4m_print_info(&context.y4m_input, stderr);

    fprintf(stderr, "\nOutput file %s:\n", params.output);
    y4m_print_info(&context.y4m_output, stderr);

    FvSr_free(&context);
    FvSrParams_free(&params);

    return 0;
}


