/**
 * @file      filter.c
 * @brief     Filtering functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <math.h>
#include <stdio.h>

#include "cement.h"
#include "convolve.h"
#include "filter.h"
#include "fvbuf.h"
#include "yuv4mpeg.h"
/*#include "multiframe.h"*/

/*
typedef struct FvBenchCx {
    FILE *file_in;
    FILE *file_out;
    y4m   y4m_in;
    y4m   y4m_out;
    FvFrame2d frame
    FvBuf 
} FvBenchCx;
*/

int main(int argc, char **argv) {
    FILE *file_in, *file_out;
    y4m y4m_in, y4m_out;
    FvFrame2d frame;
    int ret = 1;


    if (argc < 2) {
        printf("\nUsage: %s in.y4m out.y4m\n", argv[0]);
        return 0;
    }

    /* Open input file */
    file_in = cm_fopen_bin(argv[1], "r");
    if (file_in == NULL) {
        cm_log_error("Unable to open input file \"%s\" (%s)",
                     argv[1], cm_strerror(errno));
        goto input_error_exit;
    }
    if (y4m_from_stream(&y4m_in, file_in, 0) < 0) {
        goto y4m_input_error_exit;
    }

    /* Create an output file */
    if ((argv[2][0] == '-') && (argv[2][1] == '\0')) {
        file_out = stdout;
    } else {
        file_out = cm_fopen_bin(argv[2], "w+");
    }
    if (file_out == NULL) {
        cm_log_error("Unable to open output file \"%s\" (%s)",
                     argv[2], cm_strerror(errno));
        goto output_error_exit;
    }
    if (y4m_create(&y4m_out, file_out, y4m_in.width, y4m_in.height,
                   Y4M_SAMPLING_420_JPEG, Y4M_INTERLACING_NONE,
                   y4m_in.rate, y4m_in.aspect, y4m_in.metadata) < 0) {
        cm_log_error("Failed to write Y4M header to output file \"%s\"",
                argv[2]);
        goto y4m_output_error_exit;
    }

    ret = 0;

    /* Initialize a frame buffer */
    if (FvFrame2d_init_with_padding(&frame, FV_TYPE_UINT8,
            y4m_in.width, y4m_in.height, 3, 2, 2, 0, 0, FV_PADDING_NONE) < 0) {
        goto init_frame_error_exit;
    } 

    /* Process frames */
    while (y4m_get_fvframe(&y4m_in, &frame, 0) == 0) {
        y4m_put_fvframe2d(&y4m_out, &frame, 0);
    }

    FvFrame2d_free(&frame);

init_frame_error_exit:
    y4m_free(&y4m_out);
y4m_output_error_exit:
    fclose(file_out);
output_error_exit:
    y4m_free(&y4m_in);
y4m_input_error_exit:
    fclose(file_in);
input_error_exit:
    return ret;
}


#endif


