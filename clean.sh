#!/bin/bash

CC=gcc

FVSR_SOURCES="cement.c convolve.c fvfilter.c fvsample.c fvbuf.c fvframe.c fvsr.c fvopencl.c fvsrparams.c median.c multiframe.c optflow.c reconst.c yuv4mpeg.c"
FVSR_OBJECTS="cement.o convolve.o fvfilter.o fvsample.o fvbuf.o fvframe.o fvsr.o fvopencl.o fvsrparams.o median.o multiframe.o optflow.o reconst.o yuv4mpeg.o" 

FVBENCH_SOURCES="cement.c convolve.c fvbench.c fvbuf.c fvopencl.c yuv4mpeg.c"
FVBENCH_OBJECTS="cement.o convolve.o fvbench.o fvbuf.o fvopencl.o yuv4mpeg.o"

rm -f $FVSR_OBJECTS $FVBENCH_OBJECTS fvsr fvbench median

