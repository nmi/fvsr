/**
 * @file      fvframe.c
 * @brief     Generic frame buffers
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "config.h"
#include "cement.h"
//#include "fvsample.h"
#include "fvbuf.h"
#include "fvframe.h"
#include "fvfilter.h"

#if HAVE_OPENCL
#include "fvopencl.h"
#endif

const size_t fvframe_component_index[FVCOMPONENT_T_SIZE] = {
    [FVCOMPONENT_VOID] = 0,
    [FVCOMPONENT_Y]  = 0,
    [FVCOMPONENT_CB] = 1,
    [FVCOMPONENT_CR] = 2,
    [FVCOMPONENT_R]  = 0,
    [FVCOMPONENT_G]  = 1,
    [FVCOMPONENT_B]  = 2,
    [FVCOMPONENT_A]  = 3
};

int FvFrame_init_padded(FvFrame *frame,
        enum FvSampleT sample_type, size_t width, size_t height,
        uint32_t num_planes, uint32_t vertical_subsampling,
        uint32_t horizontal_subsampling, size_t luma_pad,
        size_t chroma_pad, enum FvPadT pad_type) {
    num_planes = cm_max((uint32_t) 1, num_planes);
    memset(frame, 0, sizeof(FvFrame));

    if (FvBuf_init_2d_padded(&frame->plane[0], FVCOMPONENT_Y,
                sample_type, width, height, luma_pad,
                pad_type) != FV_SUCCESS) {
        cm_log_error("Failed to set plane 0 parameters");
        memset(frame, 0, sizeof(FvFrame));
        return -1;
    }
    if ((num_planes >= 2) &&
        (FvBuf_init_2d_padded(&frame->plane[1], FVCOMPONENT_CB,
                sample_type, width / horizontal_subsampling,
                height / vertical_subsampling, chroma_pad,
                pad_type) != FV_SUCCESS)) {
        cm_log_error("Failed to set plane 1 parameters");
        FvBuf_free(&frame->plane[0]);
        memset(frame, 0, sizeof(FvFrame));
        return -1;
    }
    if ((num_planes >= 3) &&
        (FvBuf_init_2d_padded(&frame->plane[2], FVCOMPONENT_CR,
                sample_type, width / horizontal_subsampling,
                height / vertical_subsampling, chroma_pad,
                pad_type) != FV_SUCCESS)) {
        cm_log_error("Failed to set plane 2 parameters");
        FvBuf_free(&frame->plane[0]);
        FvBuf_free(&frame->plane[1]);
        memset(frame, 0, sizeof(FvFrame));
        return -1;
    }
    if (num_planes >= 4) {
        if (FvBuf_init_2d_padded(&frame->plane[3], FVCOMPONENT_A,
                sample_type, width, height, luma_pad,
                pad_type) != FV_SUCCESS) {
            cm_log_error("Failed to set plane 3 (alpha) parameters");
            FvBuf_free(&frame->plane[0]);
            FvBuf_free(&frame->plane[1]);
            FvBuf_free(&frame->plane[2]);
            memset(frame, 0, sizeof(FvFrame));
            return -1;
        }
        num_planes = 4;
    }
    frame->num_planes = num_planes;
    frame->v_subsampling = vertical_subsampling;
    frame->h_subsampling = horizontal_subsampling;
    return 0;
}

/*
int FvFrame_init_padded(FvFrame *f2d,
        enum FvType sample_type, uint32_t width, uint32_t height,
        uint32_t num_planes, uint32_t vertical_subsampling,
        uint32_t horizontal_subsampling, uint32_t luma_pad,
        uint32_t chroma_pad, enum FvPadT pad_type) {
    num_planes = cm_max((uint32_t) 1, num_planes);
    memset(f2d, 0, sizeof(FvFrame));
    if (FvBuf2d_init_with_pad(&f2d->plane[0], FVCOMPONENT_Y,
                sample_type, width, height, luma_pad,
                pad_type) < 0) {
        cm_log_error("Failed to initialize plane 0");
        return -1;
    }
    if ((num_planes >= 2) &&
        (FvBuf2d_init_with_pad(&f2d->plane[1], FVCOMPONENT_CB,
                sample_type, width / horizontal_subsampling,
                height / vertical_subsampling, chroma_pad,
                pad_type) < 0)) {
        cm_log_error("Failed to initialize plane 1");
        FvBuf2d_free(&f2d->plane[0]);
        return -1;
    }
    if ((num_planes >= 3) &&
        (FvBuf2d_init_with_pad(&f2d->plane[2], FVCOMPONENT_CR,
                sample_type, width / horizontal_subsampling,
                height / vertical_subsampling, chroma_pad,
                pad_type) < 0)) {
        cm_log_error("Failed to initialize plane 2");
        FvBuf2d_free(&f2d->plane[1]);
        FvBuf2d_free(&f2d->plane[0]);
        return -1;
    }
    if (num_planes >= 4) {
        if (FvBuf2d_init_with_pad(&f2d->plane[3], FVCOMPONENT_A,
                sample_type, width, height, luma_pad,
                pad_type) < 0) {
            cm_log_error("Failed to initialize plane 3 (alpha)");
            FvBuf2d_free(&f2d->plane[2]);
            FvBuf2d_free(&f2d->plane[1]);
            FvBuf2d_free(&f2d->plane[0]);
            return -1;
        }
        num_planes = 4;
    }
    f2d->num_planes = num_planes;
    f2d->v_subsampling = vertical_subsampling;
    f2d->h_subsampling = horizontal_subsampling;
    return 0;
}
*/

int FvFrame_init_duplicate(FvFrame *dst, FvFrame *src,
        int copy_content) {
    int i;
    memset(dst, 0, sizeof(FvFrame));
    for (i = 0; i < src->num_planes; i++) {
        if (FvBuf_init_duplicate(&dst->plane[i], &src->plane[i],
                    copy_content) < 0) {
            cm_log_error("Failed to duplicate plane %d", i);
            for (i--; i >= 0; i--) {
                FvBuf_free(&dst->plane[i]);
            }
            return -1;
        }
    }
    dst->num_planes = src->num_planes;
    dst->v_subsampling = src->v_subsampling;
    dst->h_subsampling = src->h_subsampling;
    return 0;
}

void FvFrame_view(FvFrame *view, FvFrame *src,
        size_t tl_x, size_t tl_y, size_t br_x, size_t br_y) {
    uint32_t i;
    size_t w = 0;
    size_t h = 0;
    memcpy(view, src, sizeof(FvFrame));

    for (i = 0; i < src->num_planes; i++) {
        FvBuf     *buf     = &view->plane[i];
        FvBinMain *mainbin = (FvBinMain *)
                             FvBuf_access_main(buf, FVBUF_ACCESS_READ);
        uint32_t   j;
        for (j = 0; j < FVBUF_MAX_BIN; j++) {
            buf->bin[j] = NULL;
        }
        buf->bin[FVBIN_MAIN] = mainbin;
        mainbin->refcount++;
        w = cm_max(w, buf->region[0]);
        h = cm_max(h, buf->region[1]);
    }
    for (i = 0; i < src->num_planes; i++) {
        FvBuf *buf = &view->plane[i];
        size_t sx1 = (size_t) lrintf((float) tl_x *
                                     (float) buf->region[0] / (float) w);
        size_t sy1 = (size_t) lrintf((float) tl_y *
                                     (float) buf->region[1] / (float) h);
        size_t sx2 = (size_t) lrintf((float) br_x *
                                     (float) buf->region[0] / (float) w);
        size_t sy2 = (size_t) lrintf((float) br_y *
                                     (float) buf->region[1] / (float) h);
        buf->offset   += sy1 * buf->pitch[FVBUF_ROW_PITCH] +
                         sx1 * fvsample_size[buf->sample_type];
        buf->region[0] = sx2 - sx1;
        buf->region[1] = sy2 - sy1;
    }
}

void FvFrame_free(FvFrame *frame) {
    int i;
    for (i = 0; i < frame->num_planes; i++) {
        FvBuf_free(&frame->plane[i]);
    }
    memset(frame, 0, sizeof(FvFrame));
}

void FvFrame_zero(FvFilterNode *node, FvFrame *frame) {
    int i;
    for (i = 0; i < frame->num_planes; i++) {
        FvBuf_zero(node, &frame->plane[i]);
    }
}

int FvFrame_convert(FvFilterNode *node, FvFrame *out, FvFrame *in) {
    int      ret = 0;
    uint32_t i   = 0;
    uint32_t num_planes = cm_min(out->num_planes, in->num_planes);
    for (i = 0; i < num_planes; i++) {
        if (FvBuf_convert(node, &out->plane[i], &in->plane[i]) < 0) {
            ret = -1;
        }
    }
    for (; i < out->num_planes; i++) {
        FvBuf_zero(node, &out->plane[i]);
    }
    return ret;
}

int FvFrame_sub(FvFilterNode *node, FvFrame *out, FvFrame *in) {
    int ret = 0;
    uint32_t p = 0;
    uint32_t num_planes = cm_min(out->num_planes, in->num_planes);
    for (p = 0; p < num_planes; p++) {
        if (FvBuf_sub(node, &out->plane[p], &in->plane[p]) < 0) {
            ret = -1;
        }
    }
    return ret;
}

int FvFrame_mul(FvFilterNode *node, FvFrame *out, FvFrame *in) {
    int ret = 0;
    uint32_t p = 0;
    uint32_t num_planes = cm_min(out->num_planes, in->num_planes);
    for (p = 0; p < num_planes; p++) {
        if (FvBuf_mul(node, &out->plane[p], &in->plane[p]) < 0) {
            ret = -1;
        }
    }
    return ret;
}

int FvFrame_sign(FvFilterNode *node, FvFrame *out, FvFrame *in) {
    int ret = 0;
    uint32_t p = 0;
    uint32_t num_planes = cm_min(out->num_planes, in->num_planes);
    for (p = 0; p < num_planes; p++) {
        if (FvBuf_sign(node, &out->plane[p], &in->plane[p]) < 0) {
            ret = -1;
        }
    }
    return ret;
}

void FvFrame_update_pad(FvFilterNode *node, FvFrame *frame) {
    uint32_t p = 0;
    for (p = 0; p < frame->num_planes; p++) {
        FvBuf_update_pad_2d(node, &frame->plane[p]);
    }
}


