/**
 * @file      fvfilter.h
 * @brief     Filter interface
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVFILTER_H
#define FVFILTER_H

#include "fvopencl.h"
#include "stdint.h"
#include "stdio.h"

#ifdef __cplusplus
extern "C" {
#endif

#define FVFILTER_CHANGED_ARG 2
#define FV_SUCCESS 0
#define FV_UNKNOWN_ERROR    -1
#define FV_INVALID_ARGUMENT -2 
#define FV_OUT_OF_MEMORY    -3

#define FVFILTER_DEFAULT_TIMINGS 10

#define FVFILTERNODE_INITIAL_CAPACITY 4

#define FVFILTER_FLAG_INITIALIZED 1
#define FVFILTER_FLAG_CHECK_ARG 2
#define FVFILTER_FLAG_PROFILE 4
#define FVFILTER_FLAG_PROFILE_READY 8

struct FvFilterGraph;
struct FvFilterNode;
struct FvFilterContext;
struct FvFilterProfile;

typedef int (*FvFilterFunction)(struct FvFilterNode *fnode);

typedef struct FvFilterTiming {
    FvFilterFunction function;
    int64_t         *timing;
    size_t           timing_pass;
} FvFilterTiming;

typedef struct FvFilterContext {
    size_t           arg_size;
    void            *arg;
    size_t           priv_size;
    void            *priv;
    FvFilterFunction priv_free;
} FvFilterContext;

typedef struct FvFilterProfile {
    size_t                 timings_limit;
    size_t                 functions_num;
    struct FvFilterTiming *functions;
    size_t                 selected;
    /*int64_t          elapsed;
    size_t           lastf;
    int64_t          lasttime;
    int              lastres;*/
} FvFilterProfile;

typedef struct FvFilterNode {
    size_t                  id;
    char                   *name;
    struct FvFilterGraph   *graph;
    struct FvFilterNode    *parent;
    struct FvFilterNode    *firstchild;
    struct FvFilterNode    *next;
    struct FvFilterContext  context;
    struct FvFilterProfile  profile;
    FvFilterFunction        process;
    FvFilterFunction        arg_check;
    int64_t                 time;
    uint32_t                flags;
} FvFilterNode;

typedef struct FvFilterGraph {
    const char         *name;
    struct FvClContext *clcontext;
    size_t              id_counter;
    struct FvFilterNode root;
} FvFilterGraph;


void FvFilterGraph_init(FvFilterGraph *graph, const char *name);
void FvFilterGraph_set_cl_context(FvFilterGraph *graph, FvClContext *clc);
void FvFilterGraph_free(FvFilterGraph *graph);
void FvFilterGraph_free_recursive(FvFilterGraph *graph);

int FvFilterNode_init(FvFilterNode *node, FvFilterNode *parent,
        const char *name, /*FvFilterFunction *functions,*/
        size_t arg_size, void *arg, size_t priv_size, void *priv,
        FvFilterFunction priv_free, FvFilterFunction process,
        FvFilterFunction arg_check);
void FvFilterNode_free(FvFilterNode *node);
void FvFilterNode_free_recursive(FvFilterNode *node);
int FvFilterNode_add_child(FvFilterNode *parent, FvFilterNode *child);
int FvFilter_run(FvFilterNode *node,
	/* FvFilterFptr *functions, */
        size_t arg_size, void *arg, size_t priv_size,
        FvFilterFunction priv_free, FvFilterFunction process,
        FvFilterFunction arg_check);

int FvFilter_call(FvFilterNode *node, FvFilterFunction *functions);



#ifdef __cplusplus
}
#endif

#endif /* FVFILTER_H */

