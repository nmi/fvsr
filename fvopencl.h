/**
 * @file      fvopencl.h
 * @brief     OpenCL utility function declarations and macros
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVOPENCL_H
#define FVOPENCL_H

#include <CL/opencl.h>

#define CL_BUILD_LOG_SIZE (1024 * 1024)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct FvClPlatformInfo {
    char *profile_string;
    char *version_string;
    char *name;
    char *vendor;
    char *extensions;
    
    /*
    enum FvClProfile profile;
    int version_major;
    int version_minor;
    */
} FvClPlatformInfo;

#if 0
typedef struct FvClDeviceInfo {
    cl_uint  address_bits;
    cl_bool  available;
    char    *built_in_kernels;            /* 1.2 */
    cl_bool  compiler_available;
    cl_device_fp_config           double_fp_config;
    cl_bool  endian_little;
    cl_bool  error_correction_support;
    cl_device_exec_capabilities   execution_capabilities;
    char    *extensions;
    cl_ulong global_mem_cache_size;
    cl_device_mem_cache_type      global_mem_cache_type;
    cl_uint  global_mem_cacheline_size;
    cl_ulong global_mem_size;
    cl_device_fp_config           half_fp_config;
    cl_bool  host_unified_memory;         /* 1.1 */
    cl_bool  image_support;
    size_t   image2d_max_height;
    size_t   image2d_max_width;
    size_t   image3d_max_depth;
    size_t   image3d_max_height;
    size_t   image3d_max_width;
    size_t   image_max_buffer_size;       /* 1.2 */
    size_t   image_max_array_size;        /* 1.2 */
    cl_bool  linker_available;            /* 1.2 */
    cl_ulong local_mem_size;
    cl_device_local_mem_type      local_mem_type;
    cl_uint  max_clock_frequency;
    cl_uint  max_compute_units;
    cl_uint  max_constant_args;
    cl_ulong max_constant_buffer_size;
    cl_ulong max_mem_alloc_size;
    size_t   max_parameter_size;
    cl_uint  max_read_image_args;
    cl_uint  max_samplers;
    size_t   max_work_group_size;
    cl_uint  max_work_item_dimensions;
    size_t  *max_work_item_sizes;
    cl_uint  max_write_image_args;
    cl_uint  mem_base_addr_align;
    cl_uint  min_data_type_align_size;
    char    *name;
    cl_uint  native_vector_width_char;    /* 1.1 */
    cl_uint  native_vector_width_short;   /* 1.1 */
    cl_uint  native_vector_width_int;     /* 1.1 */
    cl_uint  native_vector_width_long;    /* 1.1 */
    cl_uint  native_vector_width_float;   /* 1.1 */
    cl_uint  native_vector_width_double;  /* 1.1 */
    cl_uint  native_vector_width_half;    /* 1.1 */
    char    *opencl_c_version;            /* 1.1 */
    cl_device_id parent_device;           /* 1.2 */
    cl_device_partition_property *partition_properties;      /* 1.2 */
    cl_device_affinity_domain     partition_affinity_domain; /* 1.2 */
    cl_device_partition_property *partition_type;            /* 1.2 */
    cl_platform_id                platform;
    cl_uint  preferred_vector_width_char;
    cl_uint  preferred_vector_width_short;
    cl_uint  preferred_vector_width_int;
    cl_uint  preferred_vector_width_long;
    cl_uint  preferred_vector_width_float;
    cl_uint  preferred_vector_width_double;
    cl_uint  preferred_vector_width_half;
    size_t   printf_buffer_size;          /* 1.2 */
    cl_bool  preferred_interop_user_sync; /* 1.2 */
    char    *profile_string;
    enum FvClProfile profile;
    size_t   profiling_timer_resolution;
    cl_command_queue_properties   queue_properties;
    cl_uint  reference_count;             /* 1.2 */
    cl_device_fp_config           single_fp_config;
    cl_device_type                type;
    char    *vendor;
    cl_uint  vendor_id;
    char    *version_string;
    int      version_major;
    int      version_minor;
    char    *driver_version;
} FvClDeviceInfo;
#endif

typedef struct FvClContext {
    cl_platform_id   platform_id;
    cl_device_id     device_id;
    cl_context       context;
    cl_command_queue commandq;
    int              use_gpu;
    int              status;
    /*
    FvClPlatformInfo platform_info;
    FvClDeviceInfo   device_info;
    */
} FvClContext;


/**
 * Return a string for an OpenCL error value.
 * From https://github.com/enjalot/adventures_in_opencl/blob/master/part1/util.cpp
 * @param error OpenCL error value
 * @return pointer to a const string
 */
const char* fvclErrorString(cl_int error);

cl_int fvclGetPlatformIDs(cl_uint num_entries, void *platforms,
                          cl_uint *num_platforms);

cl_int fvclGetDeviceIDs(cl_platform_id platform, cl_device_type device_type,
        cl_uint num_entries, void *devices, cl_uint *num_devices);

cl_int fvclGetPlatformInfo(cl_platform_id platform, cl_platform_info param_name,
        size_t param_value_size, void *param_value);

cl_int fvclGetDeviceInfo(cl_device_id device, cl_device_info param_name,
        size_t param_value_size, void *param_value);

const char *fvclGetDeviceTypeStr(cl_device_type type);

cl_int fvclPrintPlatformInfo(cl_platform_id platform, cl_bool print_devices,
        const char *prefix);

cl_int fvclPrintDeviceInfo(cl_device_id device, const char *prefix);

cl_int fvclPrintPlatforms(void);

cl_program FvClContext_create_program(FvClContext *context, const char *src, const char *options);

cl_kernel FvClContext_create_kernel(cl_program prg, const char *name);

int FvClContext_init(FvClContext *context,
        cl_int platform_num, cl_int device_num);


#ifdef __cplusplus
}
#endif

#endif /* FVOPENCL_H */

