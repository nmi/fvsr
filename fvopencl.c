/**
 * @file      fvopencl.c
 * @brief     OpenCL utility functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <CL/opencl.h>

#include "config.h"
#include "cement.h"
#include "fvopencl.h"

#include <string.h>


const char* fvclErrorString(cl_int error) {
    static const char* strings[] = {
        "CL_SUCCESS",
        "CL_DEVICE_NOT_FOUND",
        "CL_DEVICE_NOT_AVAILABLE",
        "CL_COMPILER_NOT_AVAILABLE",
        "CL_MEM_OBJECT_ALLOCATION_FAILURE",
        "CL_OUT_OF_RESOURCES",
        "CL_OUT_OF_HOST_MEMORY",
        "CL_PROFILING_INFO_NOT_AVAILABLE",
        "CL_MEM_COPY_OVERLAP",
        "CL_IMAGE_FORMAT_MISMATCH",
        "CL_IMAGE_FORMAT_NOT_SUPPORTED",
        "CL_BUILD_PROGRAM_FAILURE",
        "CL_MAP_FAILURE",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "CL_INVALID_VALUE",
        "CL_INVALID_DEVICE_TYPE",
        "CL_INVALID_PLATFORM",
        "CL_INVALID_DEVICE",
        "CL_INVALID_CONTEXT",
        "CL_INVALID_QUEUE_PROPERTIES",
        "CL_INVALID_COMMAND_QUEUE",
        "CL_INVALID_HOST_PTR",
        "CL_INVALID_MEM_OBJECT",
        "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
        "CL_INVALID_IMAGE_SIZE",
        "CL_INVALID_SAMPLER",
        "CL_INVALID_BINARY",
        "CL_INVALID_BUILD_OPTIONS",
        "CL_INVALID_PROGRAM",
        "CL_INVALID_PROGRAM_EXECUTABLE",
        "CL_INVALID_KERNEL_NAME",
        "CL_INVALID_KERNEL_DEFINITION",
        "CL_INVALID_KERNEL",
        "CL_INVALID_ARG_INDEX",
        "CL_INVALID_ARG_VALUE",
        "CL_INVALID_ARG_SIZE",
        "CL_INVALID_KERNEL_ARGS",
        "CL_INVALID_WORK_DIMENSION",
        "CL_INVALID_WORK_GROUP_SIZE",
        "CL_INVALID_WORK_ITEM_SIZE",
        "CL_INVALID_GLOBAL_OFFSET",
        "CL_INVALID_EVENT_WAIT_LIST",
        "CL_INVALID_EVENT",
        "CL_INVALID_OPERATION",
        "CL_INVALID_GL_OBJECT",
        "CL_INVALID_BUFFER_SIZE",
        "CL_INVALID_MIP_LEVEL",
        "CL_INVALID_GLOBAL_WORK_SIZE"
    };

    const int count = sizeof(strings) / sizeof(strings[0]);
    const int i = -error;
    return ((i >= 0) && (i < count)) ? strings[i] : "(unknown)";
}

cl_int fvclGetPlatformIDs(cl_uint num_entries, void *platforms,
        cl_uint *num_platforms) {
    cl_int ret;
    if (num_entries == 0) {
        ret = clGetPlatformIDs(0, NULL, &num_entries);
        if (platforms != NULL) {
            void **ptr = (void **) platforms;
            if (ret == CL_SUCCESS) {
                *ptr = malloc(num_entries * sizeof(cl_platform_id));
                if (*ptr == NULL) return CL_OUT_OF_RESOURCES;
                ret = clGetPlatformIDs(num_entries, (cl_platform_id *) *ptr,
                        num_platforms);
            }
        }
    } else {
        ret = clGetPlatformIDs(num_entries, (cl_platform_id *) platforms,
                num_platforms);
    }
    return ret;
}

cl_int fvclGetDeviceIDs(cl_platform_id platform, cl_device_type device_type,
        cl_uint num_entries, void *devices, cl_uint *num_devices) {
    cl_int ret;
    if (num_entries == 0) {
        ret = clGetDeviceIDs(platform, device_type, 0, NULL, &num_entries);
        if (devices != NULL) {
            void **ptr = (void **) devices;
            if (ret == CL_SUCCESS) {
                *ptr = malloc(num_entries * sizeof(cl_device_id));
                if (*ptr == NULL) return CL_OUT_OF_RESOURCES;
                ret = clGetDeviceIDs(platform, device_type, num_entries,
                        (cl_device_id *) *ptr, num_devices);
            }
        }
    } else {
        ret = clGetDeviceIDs(platform, device_type, num_entries,
                (cl_device_id *) devices, num_devices);
    }
    return ret;
}


cl_int fvclGetPlatformInfo(cl_platform_id platform, cl_platform_info param_name,
        size_t param_value_size, void *param_value) {
    cl_int ret;
    if (param_value_size == 0) {
        void **ptr = (void **) param_value;
        size_t size;
        ret = clGetPlatformInfo(platform, param_name, 0, NULL, &size);
        if (ret == CL_SUCCESS) {
            *ptr = malloc(size);
            if (*ptr == NULL) return CL_OUT_OF_RESOURCES;
            ret = clGetPlatformInfo(platform, param_name, size, *ptr, NULL);
        }
    } else {
        ret = clGetPlatformInfo(platform, param_name, param_value_size,
                                param_value, NULL);
    }
    return ret;
}

cl_int fvclGetDeviceInfo(cl_device_id device, cl_device_info param_name,
        size_t param_value_size, void *param_value) {
    cl_int ret;

    if (param_value_size == 0) {
        void **ptr = (void **) param_value;
        size_t size;
        ret = clGetDeviceInfo(device, param_name, 0, NULL, &size);
        if (ret == CL_SUCCESS) {
            *ptr = malloc(size);
            if (*ptr == NULL) return CL_OUT_OF_RESOURCES;
            ret = clGetDeviceInfo(device, param_name, size, *ptr, NULL);
        }
    } else {
        ret = clGetDeviceInfo(device, param_name,
                              param_value_size, param_value, NULL);
    }
    return ret;
}

struct FvClDeviceType {
    cl_device_type type;
    const char    *desc;
};

const char *fvclGetDeviceTypeStr(cl_device_type type) {
    cl_ulong i = 1;
    static const struct FvClDeviceType device_types[] = {
        {0,                          "UNDEFINED"},
        {CL_DEVICE_TYPE_CPU,         "CPU"},
        {CL_DEVICE_TYPE_GPU,         "GPU"},
        {CL_DEVICE_TYPE_ACCELERATOR, "ACCELERATOR"},
        {CL_DEVICE_TYPE_DEFAULT,     "DEFAULT"},
        {CL_DEVICE_TYPE_ALL,         "ALL"},
        {0,                          NULL},
    };

    while (device_types[i].desc != NULL) {
        if (type == device_types[i].type) {
            return device_types[i].desc;
        }
        i++;
    }
    return device_types[0].desc;
}

cl_int fvclPrintDeviceInfo(cl_device_id device, const char *prefix) {
    cl_int  ret = CL_SUCCESS;
    static const char *bool_str[] = {"no", "yes"};

    char          *profile = NULL;
    char          *version = NULL;
    char          *name    = NULL;
    char          *vendor  = NULL;
    cl_device_type type = CL_DEVICE_TYPE_DEFAULT;
    cl_bool        available = 0;
    cl_ulong       global_mem_size = 0;
    cl_bool        image_support = 0;
    size_t         image2d_max_width = 0;
    size_t         image2d_max_height = 0;
    cl_uint        max_frequency = 0;
    cl_uint        max_compute_units = 0;

    fvclGetDeviceInfo(device, CL_DEVICE_NAME,      0, &name);
    fvclGetDeviceInfo(device, CL_DEVICE_VENDOR,    0, &vendor);
    fvclGetDeviceInfo(device, CL_DEVICE_VERSION,   0, &version);
    fvclGetDeviceInfo(device, CL_DEVICE_PROFILE,   0, &profile);
    fvclGetDeviceInfo(device, CL_DEVICE_TYPE,
            sizeof(cl_device_type), &type);
    fvclGetDeviceInfo(device, CL_DEVICE_AVAILABLE,
            sizeof(cl_bool), &available);
    fvclGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE,
            sizeof(cl_ulong), &global_mem_size);
    fvclGetDeviceInfo(device, CL_DEVICE_IMAGE_SUPPORT,
            sizeof(cl_bool), &image_support);
    fvclGetDeviceInfo(device, CL_DEVICE_IMAGE2D_MAX_WIDTH,
            sizeof(size_t), &image2d_max_width);
    fvclGetDeviceInfo(device, CL_DEVICE_IMAGE2D_MAX_HEIGHT,
            sizeof(size_t), &image2d_max_height);
    fvclGetDeviceInfo(device, CL_DEVICE_MAX_CLOCK_FREQUENCY,
            sizeof(cl_uint), &max_frequency);
    fvclGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS,
            sizeof(cl_uint), &max_compute_units);

    cm_log_info("%sname:              %s", prefix, name);
    cm_log_info("%svendor:            %s", prefix, vendor);
    cm_log_info("%sversion:           %s", prefix, version);
    cm_log_info("%sprofile:           %s", prefix, profile);
    cm_log_info("%stype:              %s", prefix, fvclGetDeviceTypeStr(type));
    cm_log_info("%savailable:         %s", prefix, bool_str[available]);
    cm_log_info("%smax frequency:     %u", prefix, max_frequency);
    cm_log_info("%smax compute units: %u", prefix, max_compute_units);
    cm_log_info("%simage support:     %s", prefix, bool_str[image_support]);
    cm_log_info("%simage2d max size:  %zux%zu", prefix, image2d_max_width,
            image2d_max_height);

    free(profile);
    free(version);
    free(name);
    free(vendor);
    return ret;
}


cl_int fvclPrintPlatformInfo(cl_platform_id platform, cl_bool print_devices,
        const char *prefix) {
    cl_int  ret = CL_SUCCESS;
    cl_uint i;

    cl_device_id *devices;
    cl_uint       num_devices = 0;

    char *name       = NULL;
    char *vendor     = NULL;
    char *profile    = NULL;
    char *version    = NULL;
    char *extensions = NULL;

    ret |= fvclGetPlatformInfo(platform, CL_PLATFORM_PROFILE,
            0, &profile);
    ret |= fvclGetPlatformInfo(platform, CL_PLATFORM_VERSION,
            0, &version);
    ret |= fvclGetPlatformInfo(platform, CL_PLATFORM_NAME,
            0, &name);
    ret |= fvclGetPlatformInfo(platform, CL_PLATFORM_VENDOR,
            0, &vendor);
    ret |= fvclGetPlatformInfo(platform, CL_PLATFORM_EXTENSIONS,
            0, &extensions);
    cm_log_info("%sname:       %s", prefix, name);
    cm_log_info("%svendor:     %s", prefix, vendor);
    cm_log_info("%sversion:    %s", prefix, version);
    cm_log_info("%sprofile:    %s", prefix, profile);
    cm_log_info("%sextensions: %s", prefix, extensions);
    free(profile);
    free(version);
    free(name);
    free(vendor);
    free(extensions);
    if (ret != CL_SUCCESS) {
        cm_log_error("Failed to get OpenCL platform information: %s",
                fvclErrorString(ret));
        return ret;
    }

    if (print_devices) {
        char  *device_prefix = NULL;
        size_t device_prefix_size = strlen(prefix) + 6;
        ret = fvclGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, &devices,
                &num_devices);
        if (ret != CL_SUCCESS) {
            cm_log_error("Failed to get OpenCL device IDs: %s",
                    fvclErrorString(ret));
            return ret;
        }
        cm_log_info("%sdevices:    %d", prefix, num_devices);
        device_prefix = (char *) malloc(device_prefix_size);

        snprintf(device_prefix, device_prefix_size, "%s|    ", prefix); 
        for (i = 0; i < num_devices; i++) {
            cm_log_info("%s|", prefix);
            cm_log_info("%s|- OpenCL device %d", prefix, i);
            fvclPrintDeviceInfo(devices[i], device_prefix);
        }
        free(device_prefix);
    }
    return ret;
}

cl_int fvclPrintPlatforms(void) {
    cl_platform_id *platforms;
    cl_uint         i, num_platforms;
    cl_int          ret = CL_SUCCESS;

    ret = fvclGetPlatformIDs(0, &platforms, &num_platforms);
    if (ret != CL_SUCCESS) {
        cm_log_error("Failed to get OpenCL platform IDs: %s",
                     fvclErrorString(ret));
        return ret;
    }
    cm_log_info("Number of detected OpenCL platforms: %d", num_platforms);
    for (i = 0; i < num_platforms; i++) {
        cm_log_info("OpenCL platform %d", i);
        ret |= fvclPrintPlatformInfo(platforms[i], CL_TRUE, "  ");
        cm_log_info("");
    }
    free(platforms);
    return ret;
}
 

int FvClContext_init(FvClContext *context,
        cl_int platform_num, cl_int device_num) {
    char   *str = NULL;
    char   *str2 = NULL;
    cl_bool feature;
    cl_int  ret;

    cl_platform_id *platforms;
    cl_uint         num_platforms;

    cl_device_id   *devices;
    cl_uint         num_devices;

    context->status = 0;

    fvclPrintPlatforms();

    /* Get requested platform ID */
    ret = fvclGetPlatformIDs(0, &platforms, &num_platforms);
    if (ret != CL_SUCCESS) {
        cm_log_error("Failed to get OpenCL platform IDs: %s",
                     fvclErrorString(ret));
        return -1;
    }
    if (platform_num < 0) {
        platform_num = 0;
    }
    if (platform_num < num_platforms) {
        context->platform_id = platforms[platform_num];
    } else {
        cm_log_error("Platform number %d is not available", platform_num);
        free(platforms);
        return -1;
    }
    free(platforms);

    ret = fvclGetPlatformInfo(context->platform_id, CL_PLATFORM_NAME, 0, &str);
    if (ret == CL_SUCCESS) {
        cm_log_info("Selected OpenCL platform number %d: %s",
                platform_num, str);
        free(str);
    } else {
        cm_log_info("Selected OpenCL platform number %d",
                platform_num);
    }

    /* Get requested device ID */
    ret = fvclGetDeviceIDs(context->platform_id,
            CL_DEVICE_TYPE_ALL, 0, &devices, &num_devices);
    if (ret != CL_SUCCESS) {
        cm_log_error("Failed to get OpenCL device IDs: %s",
                     fvclErrorString(ret));
        return -1;
    }
    if (device_num < 0) {
        device_num = 0;
    }
    if (device_num < num_devices) {
        context->device_id = devices[device_num];
        //memcpy(&context->device_id, &devices[device_num], sizeof(cl_device_id));
    } else {
        cm_log_error("Device number %d is not available", device_num);
        free(devices);
        return -1;
    }

    ret = fvclGetDeviceInfo(context->device_id, CL_DEVICE_NAME, 0, &str2);
    if (ret == CL_SUCCESS) {
        cm_log_info("Selected OpenCL device number %d: %s", 
                device_num, str2);
        free(str2);
    } else {
        cm_log_info("Selected OpenCL device number %d",
                device_num);
        cm_log_error("Failed to read OpenCL device name");
    }
    free(devices);

    ret = fvclGetDeviceInfo(context->device_id, CL_DEVICE_IMAGE_SUPPORT,
            sizeof(feature), &feature);
    if (ret == CL_SUCCESS) {
        if (feature == CL_FALSE) {
            cm_log_error("OpenCL device does not support images");
            return -1;
        }
    } else {
        cm_log_error("Failed to determine whether OpenCL device supports "
                     "images: %s", fvclErrorString(ret));
        return -1;
    }

    /* Create a context */
    context->context = clCreateContext(0, 1, &context->device_id,
                                       NULL, NULL, &ret);
    if (ret != CL_SUCCESS) {
        cm_log_error("Failed to create OpenCL context: %s",
                     fvclErrorString(ret));
        return -1;
    }

    /* Create a queue */
    context->commandq = clCreateCommandQueue(context->context,
                                             context->device_id, 0, &ret);
    if (ret != CL_SUCCESS) {
        cm_log_error("Failed to create OpenCL command queue: %s",
                     fvclErrorString(ret));
        return -1;
    }
    context->status = 1;

    return 0;
}

cl_program FvClContext_create_program(FvClContext *context,
        const char *src, const char *options) {
    cl_int     ret;
    cl_program prg;
    prg = clCreateProgramWithSource(context->context, 1,
            &src, NULL, &ret);
    if (ret != CL_SUCCESS) {
        cm_log_error("Failed to create OpenCL program object: %s",
                     fvclErrorString(ret));
        return NULL;
    }

    if (options == NULL) {
        ret = clBuildProgram(prg, 1, &context->device_id, "",
                NULL, NULL);
    } else {
        ret = clBuildProgram(prg, 1, &context->device_id, options,
                NULL, NULL);
    }
    if (ret != CL_SUCCESS) {
        char *log;
        cm_log_error("Failed to build OpenCL program: %s",
                     fvclErrorString(ret));
        log = (char *) malloc(CL_BUILD_LOG_SIZE * sizeof(char));
        if (clGetProgramBuildInfo(prg, context->device_id,
                CL_PROGRAM_BUILD_LOG, CL_BUILD_LOG_SIZE, log, NULL)
                    != CL_SUCCESS) {
            cm_log_error("Failed to retrieve build log from OpenCL");
        } else {
            cm_log_error("OpenCL build log:\n%s", log);
        }
        free(log);
        return NULL;
    }
    return prg;
}

cl_kernel FvClContext_create_kernel(cl_program prg, const char *name) {
    cl_int    ret;
    cl_kernel kernel;
    kernel = clCreateKernel(prg, name, &ret);
    if (ret != CL_SUCCESS) {
        cm_log_error("Failed to create OpenCL kernel: %s",
                fvclErrorString(ret));
        return NULL;
    }
    return kernel;
}

