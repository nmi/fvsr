/**
 * @file      optflow.c
 * @brief     Optical flow input functions for Middlebury
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cement.h"
#include "fvbuf.h"
#include "fvframe.h"
#include "optflow.h"


static const char *flo_magic   = "PIEH"; 
static const float flo_magic_f = 202021.25F;

union float32 {
    float    f;
    uint32_t i;
};

static inline int get_uint32_little_endian(FILE *stream, uint32_t *var) {
    int c;
    uint32_t tmp;
    *var = 0;

    c = getc(stream);
    if (c == EOF) return -1;
    tmp = (uint32_t) c;

    c = getc(stream);
    if (c == EOF) return -1;
    tmp += ((uint32_t) c) << 8;

    c = getc(stream);
    if (c == EOF) return -1;
    tmp += ((uint32_t) c) << 16;

    c = getc(stream);
    if (c == EOF) return -1;
    tmp += ((uint32_t) c) << 24;

    *var = tmp;
    return 0;
}

void OpticalFlow_clear(OpticalFlow *flow) {
    memset(flow->data, 0, flow->datasize);
}

void OpticalFlow_free(OpticalFlow *flow) {
    cm_free(flow->data);
    memset(flow, 0, sizeof(OpticalFlow));
}

int OpticalFlow_read(FILE *stream, OpticalFlow *flow) {
    size_t        frame_size, num_vectors;
    size_t        i;
    int           width, height;
    union float32 f32;

    if (flow == NULL) return -1;

    for (i = 0; i < 4; i++) {
        int c = getc(stream);
        if (c == EOF) {
            cm_log_error("Unexpected end of file");
            return -1;
        } else if (c != flo_magic[i]) {
            cm_log_error("File does not begin with .flo magic number \"PIEH\"");
            return -1;
        }
    }
    /* Check that the compiler handles floats according to
     * the IEEE 754 standard */
    f32.i = (uint32_t) flo_magic[0]        + ((uint32_t) flo_magic[1] << 8) +
           ((uint32_t) flo_magic[2] << 16) + ((uint32_t) flo_magic[3] << 24);
    if (fabs(f32.f - flo_magic_f) > 0.01) {
        cm_log_error("Representation of single-precision floating point "
                     "numbers used by this system does not follow IEEE 754 "
                     "standard. Make sure that the C compiler used to "
                     "build this program is C99-compatible.");
        return -1;
    }

    if ((get_uint32_little_endian(stream, &width) < 0) ||
        (get_uint32_little_endian(stream, &height) < 0)) {
        cm_log_error("Unexpected end of file");
        return -1;
    }
    if (width <= 0) {
        cm_log_error("Invalid frame width: %d", width);
        return -1;
    }
    if (height <= 0) {
        cm_log_error("Invalid frame height: %d", height);
        return -1;
    }
    num_vectors = 2 * (size_t) width * (size_t) height; 
    frame_size = num_vectors * sizeof(float);
    if (flow->datasize < frame_size) {
        flow->width = 0;
        flow->height = 0;
        free(flow->data);
        flow->datasize = 0;
        flow->data = (float *) cm_malloc(frame_size);
        if (flow->data == NULL) {
            cm_log_error("Failed to allocate memory for a frame of "
                         "%dx%dx%zu bytes", width, height,
                         sizeof(float) * 2);
            return -1;
        }
        flow->datasize = frame_size;
    }
    for (i = 0; i < num_vectors; ++i) {
        if (get_uint32_little_endian(stream, &f32.i) < 0) {
            cm_log_error("Unexpected end of file");
            return -1;
        }
        flow->data[i] = -f32.f;
    }
    flow->width = (uint32_t) width;
    flow->height = (uint32_t) height;
    return 0;
}

int OpticalFlow_from_template(OpticalFlow *flow,
        const char *filename_template, size_t framenum, size_t ref,
        int reverse_order, int verbose) {
    size_t filename_len = strlen(filename_template) + 16;
    char  *filename = (char *) malloc(filename_len);
    FILE  *flowfile;
    int    ret;

    if (filename == NULL) {
        cm_log_error("Failed to allocate %zu bytes of memory for "
                     "a filename string", filename_len);
        return -1;
    }
    if (reverse_order) {
        size_t tmp = ref;
        ref = framenum;
        framenum = tmp;
    }
    snprintf(filename, filename_len, filename_template, framenum, ref);

    if (verbose) {
        fprintf(stderr, "optflow from %s\n", filename);
    }

    flowfile = cm_fopen_bin(filename, "r");
    if (flowfile == NULL) {
        cm_log_warning("Failed to open optical flow file %s", filename);
        free(filename);
        return -1;
    }

    ret = OpticalFlow_read(flowfile, flow);

    fclose(flowfile);
    free(filename);
    return ret;
}

int OpticalFlow_validate_vector(const OpticalFlow *flow,
        FvFrame *origo, FvFrame *frame,
        uint32_t x, uint32_t y, uint32_t max_components,
        uint32_t validation_radius, uint32_t validation_threshold) {
    float xscale = (float) origo->plane[0].region[0] / (float) flow->width;
    float yscale = (float) origo->plane[0].region[1] / (float) flow->height;
    const float *flowdata = flow->data;
    size_t p = (size_t) y * (size_t) flow->width + (size_t) x;
    int sad = 0;
    uint32_t n = 0;

    /*if (vx < 0.1F) && (vy < 0.1F)) {
        return -1;
    } else */
    if ((flowdata[2*p] > 1e9F) || (flowdata[2*p+1] > 1e9F)) {
        cm_log_warning("Out of range optical flow vector at (x:%u, y:%u)",
                       x, y);
        return -1;
    }

    x = (uint32_t) lrintf(xscale * (float) x);
    y = (uint32_t) lrintf(yscale * (float) y);

    if (validation_radius > 0) {
        uint32_t pnum;
        uint32_t components = cm_min(max_components, frame->num_planes);
        const FvBuf *rplane0 = &frame->plane[0];
        for (pnum = 0; pnum < components; pnum++) {
            FvBuf *rplane = &frame->plane[pnum];
            FvBuf *oplane = &origo->plane[pnum];
            const FvBinMain *rbin = FvBuf_access_main(rplane, FVBUF_ACCESS_READ);
            const FvBinMain *obin = FvBuf_access_main(oplane, FVBUF_ACCESS_READ);
            float h_factor = (float) rplane->region[0] /
                             (float) rplane0->region[0];
            float v_factor = (float) rplane->region[1] /
                             (float) rplane0->region[1];
            int   h_radius = (int) lrintf((float) validation_radius *
                                          h_factor);
            int   v_radius = (int) lrintf((float) validation_radius *
                                          v_factor);
            float vx = flowdata[2*p]   * xscale * h_factor;
            float vy = flowdata[2*p+1] * yscale * v_factor;
            int   rx = (int) lrintf((float) x * h_factor);
            int   ry = (int) lrintf((float) y * v_factor);
            int   ox = rx + (int) lrintf(vx);
            int   oy = ry + (int) lrintf(vy);
            int i;

            for (i = -v_radius+1; i < v_radius; i++) {
                int j;
                const uint8_t *rrow, *orow;
                if ((oy+i < 0) || (oy+i >= oplane->region[1])) continue;
                if ((ry+i < 0) || (ry+i >= rplane->region[1])) continue;
                rrow = rbin->mem + rplane->offset +
                        (size_t) (ry+i) * (size_t) rplane->pitch[0];
                orow = obin->mem + oplane->offset +
                        (size_t) (ry+i) * (size_t) oplane->pitch[0];

                for (j = -h_radius+1; j < h_radius; j++) {
                    int diff;
                    if ((ox+j < 0) || (ox+j >= oplane->region[0])) continue;
                    if ((rx+j < 0) || (rx+j >= rplane->region[0])) continue;
                    diff = rrow[rx+j] - orow[ox+j];
                    if (diff < 0) diff = -diff;
                    sad += diff;
                    n++;
                }
            }
        }
        if (sad > n * validation_threshold) {
            //cm_log_info("INVALID: %d %d", sad, n);
            return -1;
        }
    }

    //cm_log_info("VALID: %d %d", sad, n);
    return 0;
}



