/**
 * @file      optflow.h
 * @brief     Optical flow input definitions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef OPTFLOW_H
#define OPTFLOW_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cement.h"
#include "fvframe.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct OpticalFlow {
    float *data;
    size_t datasize;
    uint32_t width;
    uint32_t height;
} OpticalFlow;


void OpticalFlow_clear(OpticalFlow *flow);
void OpticalFlow_free(OpticalFlow *flow);

/**
 * Reads optical flow vectors from a Middlebury FLO file.
 * FLO format specification is available at
 * http://vision.middlebury.edu/flow/code/flow-code/README.txt
 */
int OpticalFlow_read(FILE *stream, OpticalFlow *flow);

int OpticalFlow_from_template(OpticalFlow *flow,
        const char *filename_template, size_t framenum, size_t ref,
        int reverse_order, int verbose);

int OpticalFlow_validate_vector(const OpticalFlow *flow,
        FvFrame *origo, FvFrame *frame,
        uint32_t x, uint32_t y, uint32_t max_components,
        uint32_t validation_radius, uint32_t validation_threshold);
 

#ifdef __cplusplus
}
#endif

#endif /* OPTFLOW_H */
 
