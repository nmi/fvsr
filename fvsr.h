/**
 * @file      fvsr.h
 * @brief     Forevid Super-resolution filter definitions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVSR_H
#define FVSR_H


#include <stdint.h>
#include <stdio.h>

#include "fvbuf.h"
#include "fvopencl.h"

#include "multiframe.h"
#include "optflow.h"
#include "reconst.h"
#include "yuv4mpeg.h"

#include "nnedi.h"


#ifdef __cplusplus
extern "C" {
#endif

enum FvSrInterpolation {
    FVSR_INTERPOLATE_NONE   = 0,
    FVSR_INTERPOLATE_INPUT  = 1,
    FVSR_INTERPOLATE_OUTPUT = 2
};

typedef struct FvSrParams {
    const char **files;
    const char  *output;

    char    *flow_template;
    int      flow_template_reverse_params;
    int      flow_offset;
    uint32_t validation_radius;
    uint32_t validation_threshold;
    uint32_t sr_radius;
    uint32_t sr_scale;
    float    psf_sigma_h;
    float    psf_sigma_v;
    uint32_t psf_radius_v;
    uint32_t psf_radius_h;

    uint32_t iterations;
    uint32_t reg_radius;
    float    reg_decay;
    float    reg_factor;
    float    desc_start;
    float    desc_factor;

    uint32_t num_files;
    uint32_t max_files;
    int      show_usage;

    uint32_t mv_average_radius;
    enum FvSrInterpolation interpolation;

    int      opencl;
    int      opencl_platform;
    int      opencl_device;

    uint32_t num_channels;
} FvSrParams;


void FvSrParams_init(FvSrParams *params, int max_files);

int FvSrParams_parse(FvSrParams *params, int argc, const char * const *argv);

void FvSrParams_free(FvSrParams *params);


typedef struct FvFrameSeq {
    FvFrame *frames;
    size_t   size;
} FvFrameSeq;

int FvFrameSeq_init(FvFrameSeq *fs, size_t size, FvFrame *parameters);

void FvFrameSeq_free(FvFrameSeq *fs);

void FvFrameSeq_roll(FvFrameSeq *fs);

#define FvFrameSeq_last(fs) &(fs)->frames[(fs)->size - 1]


struct FvSrContext;
typedef struct FvSrContext FvSrContext;

typedef struct FvSrThread {
    Multiframe   multiframe;
    OpticalFlow  flow;
    ReconstState rcstate;
    FvFrame      hrframe;
    FvFrame      norm;
    /*FvFrame      tmpframe;
    FvBuf        tmpbuf;*/
    FvSrContext *shared_context;
} FvSrThread;

struct FvSrContext {
    FvSrParams  *params;

    y4m          y4m_input,   y4m_output;
    FILE        *input_file, *output_file;
    nnedi_t     *nnedi_cfg;

    FvFrame      input_frame_params;
    FvFrameSeq   work_unit;
    FvFrameSeq   input_seq;
    FvFrame      psf_h;
    FvFrame      psf_v;
    size_t       pad;
    /*
    FvBuf        psf_luma_x,          psf_luma_y;
    FvBuf        psf_chroma_x,        psf_chroma_y;
    uint32_t     psf_radius_luma_x,   psf_radius_luma_y;
    uint32_t     psf_radius_chroma_x, psf_radius_chroma_y;
    */

    FvSrThread  *thread_context;
    int          num_threads;
    uint32_t     num_channels;

    FvClContext *cl_context;
};


int FvSrThread_init(FvSrThread *tc, FvSrContext *sc);

void FvSrThread_free(FvSrThread *tc);

void FvSrThread_process(FvSrThread *tc, uint32_t wn, int fn, int seq_center, uint32_t channels);


int FvSr_init(FvSrContext *sc, FvSrParams *params);

void FvSr_free(FvSrContext *sc);

void FvSr_process(FvSrContext *sc);

#ifdef __cplusplus
}
#endif


#endif /* FVSR_H */

