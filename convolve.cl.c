__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;


__kernel void h_convolve(__write_only image2d_t out,
        __read_only image2d_t in,
        __global float *weights,
        __private int num_weights) {

    const int2 pos = {get_global_id(0), get_global_id(1)};
    float sum = 0.0F;
    //weights[0] * read_imagef(in, sampler, pos).x;
   
    int2 p = pos;
    p.x -= (num_weights ) / 2;
    for (int i = 0; i < num_weights; i++) {
        sum += weights[i] * read_imagef(in, sampler, p).x;
        p.x+=1.0F;
        //sum += weights[i] * read_imagef(in, sampler, pos).x;
    }
    
    write_imagef(out, pos, sum);
    //write_imagef(out, pos, read_imagef(in, sampler, pos));
}

__kernel void v_convolve(__write_only image2d_t out,
        __read_only image2d_t in,
        __global float *weights,
        __private int num_weights) {

    const int2 pos = {get_global_id(0), get_global_id(1)};
    float sum = 0.0F;
    int2 p = pos;
    p.y -= (num_weights ) / 2;
    for (int i = 0; i < num_weights; i++) {
        sum += weights[i] * read_imagef(in, sampler, p).x;
        p.y+=1.0F;
        //sum += weights[i] * read_imagef(in, sampler, pos).x;
    }
    /* 
    float sum = weights[0] * read_imagef(in, sampler, pos).x;
    for (int i = 1; i < num_weights; i++) {
        sum += weights[i] * read_imagef(in, sampler, pos).x;
        sum += weights[i] * read_imagef(in, sampler, pos).x;
    }
    */
    write_imagef(out, pos, sum);
    //write_imagef(out, pos, read_imagef(in, sampler, pos));
}
