__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;
//__constant float4 SGN_ZERO = {0.5F / 256.0F, 0.5F / 256.0F, 0.5F / 256.0F, 0.5F / 256.0F};
#define SGN_ZERO (0.5F / 256.0F)


__kernel void reconst_fgb(
        __write_only image2d_t backproject,
        __read_only image2d_t in,
        __read_only image2d_t median,
        __read_only image2d_t norm,
        __constant float *psf_h,
        __constant float *psf_v) {
    //float vsum[PSF_V_SIZE];
    const int2 pos = {get_global_id(0), get_global_id(1)};
    int2 row = {pos.x - PSF_H_RADIUS, pos.y - PSF_V_RADIUS};
    float sum = 0.0F;
    float median_pix = read_imagef(median, sampler, pos).x;
    for (int i = 0; i < PSF_V_SIZE; i++) {
        float hsum = 0.0F;
        int2 col = row;
        for (int j = 0; j < PSF_H_SIZE; j++) {
            hsum += read_imagef(in, sampler, col).x * psf_h[j];
            col.x++;
        }
        sum += hsum * psf_v[i];
        row.y++;
    }
    float norm_pix = read_imagef(norm, sampler, pos).x;
    sum -= median_pix;
    //sum = (float) sign(sum);
    sum = (SGN_ZERO < sum) - (sum < -SGN_ZERO);
    write_imagef(backproject, pos, sum * norm_pix);
}

__kernel void reconst_reg(
        __write_only image2d_t out,
        __read_only image2d_t in,
        __read_only image2d_t backproject,
        __constant float *psf_h,
        __constant float *psf_v,
        float desc) {
    const int2 pos = {get_global_id(0), get_global_id(1)};
    int2 row = {pos.x - PSF_H_RADIUS, pos.y - PSF_V_RADIUS};
    float in_pix = read_imagef(in, sampler, pos).x;
    float reg = 0.0F;
    float sum = 0.0F;
    for (int i = 0; i < PSF_V_SIZE; i++) {
        float hsum = 0.0F;
        int2 col = row;
        for (int j = 0; j < PSF_H_SIZE; j++) {
            hsum += read_imagef(backproject, sampler, col).x * psf_h[j];
            col.x++;
        }
        sum += hsum * psf_v[i];
        row.y++;
    }
 
    for (int l = -REG_RADIUS; l <= REG_RADIUS; l++) {
        int2 pos_pshift;
        int2 pos_nshift;
        int m;
        if (l < 1) m = 1;
        else m = 0;
        pos_pshift.x = pos.x + l;
        pos_nshift.x = pos.x - l;
        for (; m <= REG_RADIUS; m++) {
            const float scale = powr(REG_DECAY, (float) (abs(l) + abs(m)));
            float d, val_pshift, val_nshift;
            float s1, s2;
            pos_pshift.y = pos.y + m;
            pos_nshift.y = pos.y - m;
            val_pshift = read_imagef(in, sampler, pos_pshift).x;
            val_nshift = read_imagef(in, sampler, pos_nshift).x;
            d = in_pix - val_pshift;
            s1 = (SGN_ZERO < d) - (d < -SGN_ZERO);
            //s1 = sign(d);
            d = val_nshift - in_pix;
            s2 = (SGN_ZERO < d) - (d < -SGN_ZERO);
            //s2 = sign(d);
            reg += scale * (s1 - s2);
        }
    }
    write_imagef(out, pos, in_pix + desc * (sum + REG_FACTOR * reg));
}

#if 0
    const int2 pos = {get_global_id(0), get_global_id(1)};
    float4 deblurred = read_imagef(in,    sampler, pos);
    float decay = 0.01F;
    float reg_factor = 0.04F;
    int i;
    for (i = 0; i < iterations; i++) {
        int2 p = pos;
        float4 blurred = {0.0F, 0.0F, 0.0F, 0.0F};
        p.y -= (psf_size) / 2;
        for (int j = 0; j < psf_size; j++) {
            p.x = pos.x - (psf_size >> 1);
            float4 sum = 0.0F;
            for (int k = 0; k < psf_size; k++) {
                sum += read_imagef(in, sampler, p) * psf[k];
                p.x += 1;
                //sum += weights[i] * read_imagef(in, sampler, pos).x;
            }
            blurred += sum * psf[j];
            p.y += 1;
        }

        float4 difference = blurred - read_imagef(in, sampler, pos);

        //float4 sgn = (float4)(SGN_ZERO < difference) - (float4)(difference < -SGN_ZERO);
        float4 sgn = sign(difference);
        //sgn *= read_imagef(norm, sampler, pos);
        sgn = 0.0F;
        //write_imagef(out, pos, sgn);

    float4 outval = {0.0F, 0.0F, 0.0F, 0.0F};
    float4 val = read_imagef(in, sampler, pos);
    int l;
    int radius = 2;
    for (l = -radius; l <= radius; l++) {
        int2 pos_pshift;
        int2 pos_nshift;
        int m;
        if (l < 1) m = 1;
        else m = 0;
        pos_pshift.x = pos.x + l;
        pos_nshift.x = pos.x - l;
        for (; m <= radius; m++) {
            const float scale = powr(decay, (float) (abs(l) + abs(m)));
            float4 d, val_pshift, val_nshift;
            float4 s1, s2;
            pos_pshift.y = pos.y + m;
            pos_nshift.y = pos.y - m;
            val_pshift = read_imagef(in, sampler, pos_pshift);
            val_nshift = read_imagef(in, sampler, pos_nshift);
            d = val - val_pshift;
            //s1 = (float4)(SGN_ZERO < d) - (float4)(d < -SGN_ZERO);
            s1 = sign(d);
            d = val_nshift - val;
            //s2 = (float4)(SGN_ZERO < d) - (float4)(d < -SGN_ZERO);
            s2 = sign(d);
            outval += scale * (s1 - s2);
        }
    }
    val = reg_factor * deblurred;
    val += read_imagef(in, sampler, pos);
        p = pos;
        p.y -= (psf_size) / 2;
        for (int j = 0; j < psf_size; j++) {
            p.x = pos.x - (psf_size >> 1);
            float4 sum = 0.0F;
            for (int k = 0; k < psf_size; k++) {
                sum += psf[k] * val;
                p.x += 1;
                //sum += weights[i] * read_imagef(in, sampler, pos).x;
            }
            blurred += sum * psf[j];
            p.y += 1;
        }

    write_imagef(out, pos, blurred +
            val * 1.5);

    }
#endif

#if 0
/*
__kernel void reconstx(__write_only image2d_t out,
        __read_only image2d_t in,
        __global float *weights,
        __private int num_weights) {

    const int2 pos = {get_global_id(0), get_global_id(1)};
    float sum = 0.0F;
    //weights[0] * read_imagef(in, sampler, pos).x;
   
    int2 p = pos;
    p.x -= (num_weights ) / 2;
    for (int i = 0; i < num_weights; i++) {
        sum += weights[i] * read_imagef(in, sampler, p).x;
        p.x+=1.0F;
        //sum += weights[i] * read_imagef(in, sampler, pos).x;
    }
    
    write_imagef(out, pos, sum);
    //write_imagef(out, pos, read_imagef(in, sampler, pos));
}
*/

__kernel void fgb(
        __write_only image2d_t out,
        __read_only  image2d_t blurred,
        __read_only  image2d_t orig,
        __read_only  image2d_t norm) {
    const int2 pos = {get_global_id(0), get_global_id(1)};
    
    float4 difference = read_imagef(blurred, sampler, pos) -
                       read_imagef(orig,    sampler, pos);
    //float4 sgn = (float4)(SGN_ZERO < difference) - (float4)(difference < -SGN_ZERO);
    float4 sgn = sign(difference);
    sgn *= read_imagef(norm, sampler, pos);
    //sgn = 0.0F;
    write_imagef(out, pos, sgn);
}

__kernel void bgr(
        __write_only image2d_t out,
        __read_only image2d_t in,
        __private int radius,
        __private float decay) {
    float4 outval = {0.0F, 0.0F, 0.0F, 0.0F};
    const int2 pos = {get_global_id(0), get_global_id(1)};
    float4 val = read_imagef(in, sampler, pos);
    int l;
    for (l = -radius; l <= radius; l++) {
        int2 pos_pshift;
        int2 pos_nshift;
        int m;
        if (l < 1) m = 1;
        else m = 0;
        pos_pshift.x = pos.x + l;
        pos_nshift.x = pos.x - l;
        for (; m <= radius; m++) {
            const float scale = powr(decay, (float) (abs(l) + abs(m)));
            float4 d, val_pshift, val_nshift;
            float4 s1, s2;
            pos_pshift.y = pos.y + m;
            pos_nshift.y = pos.y - m;
            val_pshift = read_imagef(in, sampler, pos_pshift);
            val_nshift = read_imagef(in, sampler, pos_nshift);
            d = val - val_pshift;
            //s1 = (float4)(SGN_ZERO < d) - (float4)(d < -SGN_ZERO);
            s1 = sign(d);
            d = val_nshift - val;
            //s2 = (float4)(SGN_ZERO < d) - (float4)(d < -SGN_ZERO);
            s2 = sign(d);
            outval += scale * (s1 - s2);
        }
    }
    write_imagef(out, pos, outval);
}

__kernel void combine(
        __write_only image2d_t out,
        __read_only image2d_t hres_deblurred,
        __read_only image2d_t bp,
        __read_only image2d_t reg,
        float reg_factor,
        float descent_speed) {
    const int2 pos = {get_global_id(0), get_global_id(1)};
    float4 val = reg_factor * read_imagef(reg, sampler, pos);
    val += read_imagef(bp, sampler, pos);
    write_imagef(out, pos, read_imagef(hres_deblurred, sampler, pos) +
            val * descent_speed);
}
#endif

