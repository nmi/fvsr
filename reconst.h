/**
 * @file      reconst.h
 * @brief     Multiframe super-resolution reconstruction
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef RECONST_H
#define RECONST_H

#include <stdint.h>

#include "fvframe.h"
#include "fvsample.h"
#include "multiframe.h"
#include "convolve.h"

#include "fvopencl.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ReconstState {
    FvFilterGraph filtergraph;
    FvFilterNode convolve_h;
    FvFilterNode convolve_v;
    FvFrame      hres_deblurred;
    FvFrame      hres_median;
    FvFrame      hres_norm;
    FvFrame      backprojection;
    FvFrame      regularization;
    FvFrame      tmp;
    FvFrame     *psf_h, *psf_v;
    float        reg_factor;
    float        desc_start;
    float        desc_factor;
    float        desc;
    uint32_t     reg_radius_luma, reg_radius_chroma;
    float        reg_decay_luma, reg_decay_chroma;

    FvClContext *clc;
    cl_program   clprg;
    cl_kernel    clreconst_fgb;
    cl_kernel    clreconst_reg;
    cl_event     clwait;
    cl_channel_order clchannels;
    uint32_t     psf_h_radius;
    uint32_t     psf_v_radius;
} ReconstState;

int ReconstState_init(ReconstState *state,
        FvFrame *hres, FvFrame *psf_h, FvFrame *psf_v,
        uint32_t reg_radius, float reg_decay, float reg_factor,
        float desc_start, float desc_factor, FvClContext *clc,
        uint32_t num_channels);

void ReconstState_free(ReconstState *state);

void ReconstState_start(ReconstState *state);

void ReconstState_step(ReconstState *state);


#ifdef __cplusplus
}
#endif

#endif /* RECONST_H */

